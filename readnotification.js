'use strict'; 
var _ = require('lodash');
var async = require('async'); 
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')


exports.readnotification = function(req, res) { 
    var user_id =  req.body.user_id;
    var notification_id = req.body.notification_id;

    if(user_id === "" || user_id == undefined){
      res.send({status:0,error:"missing mandatory element deviceid"});
      return true;
    }

    if(!notification_id || notification_id == ""){
      var input_data = {
        isnew : false
      }
      var where = {
        receiver_id : ObjectId(user_id)
      }
    }else{
      var input_data = {
        seen : true,
        isnew: false
      }
      var where = {
        receiver_id : ObjectId(user_id),
        _id : ObjectId(notification_id)
      }
    }
    db.get().collection('sn_notifications').update(where, { $set: input_data },{upsert:false,multi:true}, function(err, result) { 
      assert.equal(err, null); 

      if(result.result['nModified']!=0){
        res.send({status:1})
      }else{
        res.send({status:0, error: "Update failed"})
      }

    }) 
};
