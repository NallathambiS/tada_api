'use strict';
const assert = require('assert')
var db = require("../../db")

exports.getreport = function(req, res) {

    let where = {}
    db.get().collection("user_report_type").find(where).toArray(function(err, docs) {
          assert.equal(err, null);
          if (docs.length>0) {
            res.send({status:1,data:docs})
          }
          else {
            res.send({status:0,data:"type not found"});
          }
    })
}
