'use strict';
var async = require('async')
const assert = require('assert')
var db = require("../../db")

exports.avgetoccasiontype = function(req, res) {
db.get().collection("av_occasion_type").find({}).sort({"sorder_no":1}).toArray(function(err, docs) { 
assert.equal(err, null);
    if(docs){
      res.send({
      status: 1,
      occasion_type: docs     
    });
    }
    else
    {
      res.send({
      status: 0,
      error: "occasion type not found"      
    });
    }
  });          
}
