'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')

const gcm = require("node-gcm");
var sender = new gcm.Sender(init.gcm_key);
const apn = require("apn");
var apnProvider = new apn.Provider(init.apnoptions);


exports.favouritevideo = function(req, res) { 
	  var video_id =  req.body.video_id;
    var user_id =  req.body.user_id;
    var fav =  req.body.favourite;

    if(!user_id || user_id == "" || !fav || fav == ""){
      res.send({status:0,error:"Elements not found"});
      return true;
    }
    
    let where={_id:ObjectId(video_id)}
    let input_data={} 
     if(fav==='favourite'){ 
      input_data={$addToSet: {favourite:{user_id:ObjectId(user_id)}}}
      db.get().collection('av_video').update(where,input_data, function(err, result) {
      assert.equal(err, null);
      res.send({status:1,message:"favourite added"});
      return true;
    }) 
	}
	else if(fav==='unfavourite'){
      input_data={ $pull: { favourite:{user_id:ObjectId(user_id) }}}
		  db.get().collection('av_video').update(where,input_data, function(err, result) {
      assert.equal(err, null);
      res.send({status:1,message:"favourite removed"});
      return true;
    })
	}
   else{
    res.send({status:0, message:"favourite type not found"});
  } 
}
