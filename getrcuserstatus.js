'use strict';
var _ = require('lodash')
var async = require('async')
var mongo = require('mongodb')
const assert = require('assert')
var db = require("../../db")
var ObjectId = mongo.ObjectID
var request = require('request')

exports.getrcuserstatus = function(req, res) {

    let user_id     =  req.body.user_id

    if(user_id == undefined || user_id == ""){
      res.send({status:0,error:"missing mandatory element user_id"})
      return true
    }

     try{
      db.get().collection("aug_users").find({chat_userId : user_id}).limit(1).skip(0).toArray(function(err, docs){
      assert.equal(err, null);

        if(docs[0] && docs[0]['chat_authToken'] && docs[0]['chat_userId']){
          request({
              url: "https://chat.augray.com/api/v1/users.getStatus?userId=" + user_id + "",
              method: "GET",
              headers: {
                  "content-type": "application/json",
                  "X-Auth-Token": docs[0]['chat_authToken'],
                  "X-User-Id": docs[0]['chat_userId'],
              },
              json: true,
          },
           function (error, response, body){
            console.log(body)
              if(body.success === true){
                res.send({status:1,status_details:body})
              }else{
                res.send({status:0})
              }
          });
        }else{
          res.send({status:0})
        }

    })
    }
    catch(errorcatch){
      console.log(errorcatch);
    }
}
