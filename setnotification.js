'use strict';
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')

exports.setnotification = function(req, res) {
  var user_id =  req.body.user_id;
  var push_notification=req.body.push_notification
    let where = {_id:ObjectId(user_id)}
    db.get().collection("aug_users").updateOne(where,{$set:{push_notification:push_notification}},{upsert:false},function(err, docs) {  
          assert.equal(err, null);
          if (docs.result['nModified']!=0) {
            res.send({status:1,push_notification:push_notification})
          }
          else {
            res.send({status:0,message:"push_notification not changed"});
          }
    })
}
