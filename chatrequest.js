'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var request = require('request')
var db = require("../../db")

exports.chatrequest = function(req, res) {
  let follower = req.body.from_user_id
  let following = req.body.to_user_id
  let action = req.body.action

  if(following === undefined || follower === undefined || action === undefined ||
    following === '' || follower === '' || action === ''){
    return res.send({"status":0,message:"Missing mandatory elements"})
  }

  if(follower.length === 0){
    return res.send({"status":0,message:"from_user_id should consist of alteast one element"});
  }

  if(following.length === 0){
    return res.send({"status":0,message:"to_userid should consist of alteast one element"});
  }

  if(following.includes(follower)){
    return res.send({"status":0,message:"to_userid and from_userid cant be same"});
  }

    if(following.length !== 0){
      if(action === 'INVITE'){
          let input_data = {
              following:ObjectId(following),
              follower:ObjectId(follower),
              status: parseInt(0),
              created_at: new Date(),
              last_updated_at: new Date()
            }

            db.get().collection("user_friends").insert(input_data,function(err, docsInserted) {
              assert.equal(err, null);
              if(docsInserted!=0){
                let where_data={
                  following:ObjectId(following),
                  follower:ObjectId(follower)
                }
                 db.get().collection("aug_users").find({_id: {$in:[ObjectId(following),ObjectId(follower)]}}).project({_id:1,user_name:1,user_gender:1,user_dob:1,profile_picture:1}).toArray(function(err, docs){
                 assert.equal(err, null);
               // res.send({status:1,userlist:docs}); //invite accepte
               res.send({status:1,message:"Invite success"}); //invite accepte
              });
              }
              else{
                res.send({status:0,userlist:"invite failed"});
              }
            })
      }else if (action === 'DENY') {

            db.get().collection("user_friends").remove({
              following:ObjectId(following),
              follower: ObjectId(follower)
              },function(err,docs){
              assert.equal(err, null);
              if(docs!=""){
                res.send({status:3,message:"Deny success"});
              }
              else{
                res.send({status:3,message:"Deny failed"});
              }
            })

      }else if (action === 'ACCEPT') {

            let update_data = {
              $set : {
                status : parseInt(1),
                last_updated_at: new Date()
              }
            }
            console.log(following)
            console.log(follower)
            db.get().collection("user_friends").update({
              following:ObjectId(following),
              follower: ObjectId(follower)
              },update_data,function(err, docs) {
              assert.equal(err, null);
              console.log(docs)
            });


          async.parallel([
          function(callback) {
            db.get().collection("aug_users").find({_id : ObjectId(follower)}).limit(1).skip(0).toArray(function(err, docs){
              assert.equal(err, null);
              callback(null, docs);
            })
          },
          function(callback) {
            db.get().collection("aug_users").find({_id : ObjectId(following)}).limit(1).skip(0).toArray(function(err, docs){
              assert.equal(err, null);
              callback(null, docs);
            })
          }],
       function(err, results) {
        if(results.length !== 0){
          var data = {
            from: results[0] && results[0][0],
            to: results[1] && results[1][0],
          };
          if(data){
            var myJSONObject = {
              "username": data['to']['user_name']
            };
            request({
                url: "https://chat.augray.com/api/v1/im.create",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    "X-Auth-Token": data['from']['chat_authToken'],
                    "X-User-Id": data['from']['chat_userId'],
                },
                json: true,
                body: myJSONObject,
            }, function (error, response, body){
              console.log(body)
                if(body.success === true){
                  res.send({status:2,room_details:body.room})
                }else{
                  res.send({status:0,message:"chat initiation failed"})
                }
            });
          }else{
            res.send({status:0,message:"no users found"})
          }
        }
      })
      }
    }
}
