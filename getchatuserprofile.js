'use strict';
var _ = require('lodash');
var async = require('async');
const assert = require('assert');
var mongo_lib = require('mongodb');
var ObjectId = mongo_lib.ObjectID;
var db = require("../../db")

exports.getchatuserprofile = function(req, res) {

      var chat_user_id = req.body.chat_user_id;

      if(chat_user_id === "" || chat_user_id === undefined){
        res.send({status:0,error:"missing input field, chat_user_id is mandatory."});
        return true;
      }

      var whereuserid = {};
      whereuserid['chat_userId'] =chat_user_id;

      db.get().collection("aug_users").find(whereuserid).toArray(function(err, userdocs) {
        assert.equal(err, null);
        if(userdocs.length !== 0){
          var user_id= ObjectId(userdocs[0]._id)
          var where = {};
          where['_id'] =user_id;

      db.get().collection("aug_users").find(where).toArray(function(err, docs) {
        assert.equal(err, null);
        if(docs.length !== 0){
          async.parallel([
            function(callback0) {
              //groups
                db.getChat().collection("rocketchat_room")
                  .find({t:'p',usernames:docs[0]['user_name']})
                    .project({ _id: 1, name: 1, usernames: 1 })
                      .toArray(function(err, docs) {
                        assert.equal(err, null);
                        callback0(null, docs);
                      })
            },
            function(callback1) {
              //user_following
              let where = {
                follower : ObjectId(user_id),
                status : 1,
              }
              db.get().collection("user_friends").find(where).count(function(err1, docs) {
                assert.equal(err1, null);
                callback1(null, docs);
              });
            },
            function(callback2) {
              //user_followers
              let where = {
                following : ObjectId(user_id),
                status : 1
              }
              db.get().collection("user_friends").find(where).count(function(err2, docs) {
                assert.equal(err2, null);
                callback2(null, docs);
              });
            }
          ],
          function(err, results) {
            docs[0]['groups'] = results && results[0];
            docs[0]['following_count'] = results && results[1];
            docs[0]['followers_count'] = results && results[2];
            // docs[0]['thumb_image'] = docs[0]['thumb_image'] ? docs[0]['thumb_image'][0]['file_path'] : null;
            res.send({
              status: 1,
              data: docs ? docs[0] : {}
            });
          })
        }
      })
    }
    else {
      res.send({status:0,message:"user not found"})
    }
  })
};
