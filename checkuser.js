'use strict';
var _ = require('lodash');
var async = require('async');
var mongo_lib = require('mongodb');
const assert = require('assert');
var ObjectId = mongo_lib.ObjectID;
var db = require("../../db")


exports.checkuser = function(req, res) {

    var phone     =  req.body.phone;
    var email     =  req.body.email;
    var is_social =  req.body.is_social;

    if(is_social == undefined ){
      res.send({status:0,error:"missing mandatory element is_social"});
      return true;
    }

    if(!phone && !email){
      res.send({status:0,error:"Atleast one element required, email/phone no."});
      return true;
    }

     if(phone){
      var where ={
        "is_social_login": is_social,
        "user_phone": phone
      };
    } else if(email){
      var where ={
        "is_social_login": is_social,
        "user_email": email
      };
    }

    db.get().collection("aug_users").find(where).limit(1).skip(0).toArray(function(err, docs) {
      assert.equal(err, null);
      if(docs.length > 0){
        docs[0]['status'] = 1
      }
      res.send((docs.length > 0) ? {status: 1,data: docs[0]} : { "status":0,error:"User not found" });
    });


};
