'use strict'; 
var _ = require('lodash');
var async = require('async'); 
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')


exports.setnotificationdata = function(req, res) {
    var input_data = {};
    input_data['user_id']     =  req.body.user_id;
    input_data['notification_name']     =  req.body.notification_name;
    input_data['notification_set']     =  req.body.notification_set;

    for (var item in input_data) {
      if(input_data[item] === "" || input_data[item] == undefined){
          res.send({status:0,error: "Missing mandatory elements "+item});
          return true;
      }
    }
    switch(input_data['notification_name']) {
      case 1:
          var noti_name = "common_notification";
          break;
      case 2:
          var noti_name = "friend_req_notification";
          break;  
      default:
      res.send({status:0,error: "Notification Id not matching with anyone"});
      return true;
    }
    var update_data = {};
    update_data[noti_name] = input_data['notification_set'];

    db.get().collection('aug_users').updateOne({_id: ObjectId(input_data['user_id'])}, {$set: update_data}, function(err, result) {
      assert.equal(err, null); 

      if(result){
        res.send({status:1})
      }else{
        res.send({status:0, error: "Update failed"})
      }

    })

};
