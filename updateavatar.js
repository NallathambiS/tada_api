'use strict';
var Settings = require("../../lib/Settings")
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var AWS = require('aws-sdk')
var path = require('path')
var db = require("../../db")

exports.updateavatar = function(req, res) {

    var assetArray = [];
    var deleteFileNames = [];

    var avatar_name =  req.body.avatar_name; //string
    var gender =  req.body.gender; //string   *
    var user_id =  req.body.user_id; //string *
    var assets =  req.body.assets; //array of objects with id, color, type *
    var facetexture =  req.body.face_texture; //s3path *face_texure
    var orgimage =  req.body.org_image; //s3path *
    var face_shape = req.body.face_shape; //string *
    var skin_color =  req.body.skin_color; //string *
    var thumbimage =  req.body.thumb_image; //array of objects with name, filepath *
    var gifanimation = req.body.gif_animation; //array of objects with gifcode,s3path *

    if(
      !gender || gender == "" ||  gender === undefined ||
      !user_id || user_id == "" ||  user_id === undefined ||
      !assets || assets == "" ||  assets === undefined ||
      !facetexture || facetexture == "" ||  facetexture === undefined ||
      !orgimage || orgimage == "" ||  orgimage === undefined ||
      !face_shape || face_shape == "" ||  face_shape === undefined ||
      !skin_color || skin_color == "" ||  skin_color === undefined ||
      !thumbimage || thumbimage == "" ||  thumbimage === undefined ||
      !gifanimation || gifanimation == "" ||  gifanimation === undefined
    ){
      res.send({status:0,error:"missing mandatory elements"});
      return true;
    }

    //checkuser before deleting in s3
    var where = {
      user_id: ObjectId(user_id)
    };

    db.get().collection('av_avatar').find(where).toArray(function(err, result) {
      assert.equal(err, null);
      if(result.length === 0){
          res.send({status:0,error:"User not found"});
          return true;
      }
    })

    const mapObjectID = assets.map(async a => {
        await assetArray.push({
        name: a.name,
        asset_id: ObjectId(a.asset_id.toString()),
        color: a.color
      })
    });

  var s3 = new AWS.S3({
             accessKeyId: Settings.s3.key,
             secretAccessKey: Settings.s3.secret
           });
           // var mynewBucket = Settings.s3.bucketnew;

    //s3 paths to delete = *face_texture, *org_image, *thumb_image, *gif_animation

    deleteFileNames.push(path.basename(facetexture))
    deleteFileNames.push(path.basename(orgimage))

    thumbimage.map(a => {
      deleteFileNames.push(path.basename(a.file_path))
    })

    gifanimation.map(a => {
      deleteFileNames.push(path.basename(a.file_path))
    })

    console.log(deleteFileNames);
   // res.send({data:deleteFileNames});

    const deleteSThreePath = deleteFileNames.map(async a => {

      var params = {
          Bucket: Settings.s3.bucketnew,
          Key: Settings.s3['path-avatar'] + a
      };
      await s3.headObject(params, function (err, metadata) {
        if (!err || err.code != 'NotFound') {
          s3.deleteObject(params, function(err, data) {
           if (err) console.log(a,err, err.stack); // an error occurred
           else     console.log(data);           // successful response
          });
        }
      });

    })
//res.send({data:"succes"});
    var update_data = {
      avatar_name   :   avatar_name,
      gender        :   gender,
      user_id       :   ObjectId(user_id),
      assets        :   assetArray,
      face_texture  :   facetexture,
      org_image     :   orgimage,
      face_shape    :   face_shape,
      skin_color    :   skin_color,
      thumb_image   :   thumbimage,
      gif_animation  :   gifanimation,
      create_date   :   new Date(),
      last_update   :   new Date()
    };

    db.get().collection('av_avatar').findOneAndUpdate(where, {$set: update_data}, function(err, result) {
      assert.equal(err, null);

      console.log(result);

      res.send({
        status: 1,
        message: 'Updated Successfully'
      })
      return true;
    })

};
