'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")

exports.getdefassetanim = function(req, res) {

  let gender = req.body.gender

  if(gender == "" || gender == undefined){
    res.send({status:0,error:"missing input fields"});
    return true;
  }

  let where = {
    asset_name: "Default",
    gender: gender
  }

  let animation_name = {
    animation_name: "Default"
  }

  let asset = (callback0) => {
    db.get().collection("av_asset").aggregate([
      {
        "$match" : where
      },
      {
        $lookup: {
          "from": "av_asset_type",
          "localField": "asset_type_id",
          "foreignField": "_id",
          "as": "ob_asset"
        }
      },
      {
        $unwind: "$ob_asset"
      },
      {
        $project: {
          asset_type_name: "$ob_asset.asset_type_name",
          asset_type_id: 1,
          char_type_id: 1,
          gender: 1,
          user_id: 1,
          asset_name: 1,
          thumb_image: 1,
          file_path_ios: 1,
          file_path_android: 1,
          metadata: 1,
          asset_enable: 1,
          country_code: 1,
          point: 1,
          last_update: 1
        }
      }
    ]).toArray((err, docs) => {
      assert.equal(err, null);
      callback0(null, docs);
    })
  }

  let animation = (callback1) => {
    db.get().collection("av_animation").aggregate([
      {
        "$match" : animation_name
      },
      {
        $lookup: {
          "from": "av_animation_type",
          "localField": "animation_type_id",
          "foreignField": "_id",
          "as": "default_animations"
        }
      },
      {
        $unwind: "$default_animations"
      },
      {
        $project: {
          "user_id": 1,
          "char_type_id": 1,
          "animation_type_id": 1,
          "animation_type_name": "$default_animations.animation_type_name",
          "animation_name": 1,
          "file_path_ios": 1,
          "file_path_android": 1,
          "thumb_image": 1,
          "metadata": 1,
          "animation_enable": 1,
          "country_code": 1,
          "point": 1,
          "last_update": 1
        }
      }

    ]).toArray((err, docs) => {
      assert.equal(err, null);
      callback1(null, docs);
    })
  }

  async.parallel({
    default_asset: asset,
    default_animation: animation
  },
  (err, results) => {

      if (err) {
          res.send({
            status: 0
          })
          return
      }

      res.send({
        status:1,
        data: [
          {
            c_type: "asset",
            count: _.size(results.default_asset),
            gender: gender,
            resource_list: results.default_asset
          },
          {
            c_type: "animation",
            count: _.size(results.default_animation),
            resource_list: results.default_animation
          }
        ]
      })
    }
  )

}
