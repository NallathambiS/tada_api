'use strict';
var _ = require('lodash');
var mongo = require('mongodb');
const assert = require('assert');
var ObjectId = mongo.ObjectID;
var Const = require("../../const");
var request = require('request');
const msgclient = require('twilio')(Const.twilioaccountSid, Const.twilioauthToken);
const sgMail = require('@sendgrid/mail');
var db = require("../../db")

exports.sendemailmessage = function(req, res) {

    sgMail.setApiKey(Const.sendgrid_api);

    var where_data = {};
    var user_id  =  req.body.user_id;
    var user_phone=req.body.user_phone;
    var phone_code=req.body.phone_code;
    var user_email=req.body.user_email;
    var code = req.body.code;

    if(!ObjectId.isValid(user_id)){
      res.send({status:3,message: "user_id is not mongo object ID"});
      return true;
    }

    if(user_id === "" || user_id == undefined || code === "" || code == undefined){
        res.send({status:2,message: "Missing mandatory element user_id/code"});
        return true;
    }

    if(user_id != "" || user_id != undefined){
      where_data['_id']     =  ObjectId(user_id);
    }

    if(user_phone != "" || user_phone != undefined){
      where_data['user_phone']     =  user_phone;
    }

    if(phone_code != "" || phone_code != undefined){
      where_data['phone_code']     =  phone_code;
    }

    if(user_email != "" || user_email != undefined){
      where_data['user_email']     =  user_email;
    }

    if (user_phone){
      if (phone_code == "" || phone_code == undefined ){
        res.send({status:4,message:"Missing input fields phone_code"});
        return true;
      }
    }

    if (user_phone) {
      db.get().collection('aug_users').find({_id:where_data['_id'],user_phone:where_data['user_phone'],phone_code:where_data['phone_code']}).toArray(function(err, docs) { 
        assert.equal(err, null);

            if(docs !=""){
              var tophone='';
              if(user_phone.includes(phone_code)){
                tophone=user_phone;
              }
              else
              {
                tophone=phone_code+user_phone;
              }

             msgclient.messages.create({
               body: 'TaDa - Your verification number : '+code,
               from: Const.twiliphoneno,
               to: tophone
             }).then(function(message){
                console.log(message);
                if (message.status == "sent" || message.status == "queued"){
                  res.send({status:1})
                } else{
                  res.send({status:0,error:"Unable to send message, check country code with mobile number"})
                }
             }).done();
            }
        })
    }


   if (user_email)
    {
        db.get().collection('aug_users').find({_id:where_data['_id'],user_email:where_data['user_email']}).toArray(function(err, result) {
          assert.equal(err, null);

          if(result !=""){
              var msgTxt =  '<html><p>Hi,</p><p>Please use the below authenticate code for your TaDa Time App.</p><p>Your verification code is - <b>'+ code +'</b></p><p>Regards,<br/>Admin - TaDa</p><marquee>Thanks for using TaDa Time</marquee></html>';
              const msg = {
                to: user_email,
                from: {
                  email: 'tada@augray.com',
                  name: 'TaDa Admin'
                },
                subject: 'TaDa - Verification Code',
                html: msgTxt,
              };
              sgMail.send(msg,function(success,message) {
                if (!success) {
                  res.send({status:1,data:message});
                }else{
                  res.send({status:0});
                }
              });
            }
        })
    }
  }
