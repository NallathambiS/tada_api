'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')
var request = require('request')

exports.reguser = function(req, res) {

    var full_name = req.body.full_name;
    var user_name = req.body.user_name;
    var device_id = req.body.device_id;
    var profile_picture = req.body.profile_picture || "";
    var profile_picture_org = req.body.profile_picture_org || "";
    var phone_code = req.body.phone_code;
    var user_phone = req.body.user_phone;
    var user_gender = req.body.user_gender;
    var user_dob = req.body.user_dob;
    var user_email = req.body.user_email;
    var social_user_id = req.body.social_user_id;
    var is_social_login = req.body.is_social_login;
    var social_type = req.body.social_type || "";
    var user_about = req.body.user_about || "";

    if(is_social_login === "" || is_social_login === undefined){
      res.send({status:0,error:"missing input fields, is_social_login is mandatory."});
      return true;
    }

    if(is_social_login === false || is_social_login === 'false'){
      if(
         user_dob == "" ||
         user_gender == "" ||
         user_name == "" ||
         device_id == "" ||
         user_name == undefined || device_id == undefined || user_gender == undefined ||
          (
           ((phone_code === "" || phone_code === undefined) &&
           (user_phone === "" || user_phone === undefined)) &&
           (user_email === "" || user_email === undefined)
         )){
         res.send({status:0,error:"missing input fields"});
         return true;
       }
    }

    if(is_social_login === true || is_social_login === 'true'){
      if(
         user_dob == "" ||
         user_gender == "" ||
         user_name == "" ||
         device_id == "" ||
         social_user_id == "" ||
         // user_email == "" ||
         social_type == "" ||
         user_name == undefined || device_id == undefined ||
         social_type == undefined ||
         user_gender == undefined || user_dob == undefined || social_user_id == undefined){
         res.send({status:0,error:"missing input fields"});
         return true;
       }
    }

   if (user_name != undefined || user_name != ""){
     user_name=user_name.toLowerCase();
   }

    var input = {};
    input['full_name'] = full_name;
    input['user_name'] = user_name;
    input['device_id'] = device_id;
    input['profile_picture'] = profile_picture;
    input['profile_picture_org'] = profile_picture_org;
    input['phone_code'] = phone_code;
    input['user_phone'] = user_phone;
    input['user_dob'] = user_dob;
    input['user_email'] = user_email;
    input['is_social_login'] = is_social_login;
    input['social_user_id'] = social_user_id;
    input['social_type'] = social_type;
    input['user_about'] = user_about;

    if(user_gender != undefined || user_gender != ""){
      input['user_gender'] = user_gender;
    }

    input['user_active'] = true;
    input['push_notification'] = true;
    input['public_account'] = false;
    input['isonline'] = true;
    input['role_id'] = 1;
    input['search_distance'] = 20;
    input['user_cash'] = 0;
    input['user_point'] = 50;
    input['is_avatarcreated'] = false;
    input['reg_date'] = new Date();
    input['last_update'] = new Date();

    if(req.body.language_code!=null && req.body.language_code!=""){
      if(!Array.isArray(req.body.language_code)){
      return res.send({"status":0,message:"language_code is an array and should consist of alteast one element"})
      }
      input['language_code'] = req.body.language_code;
    }
    else
    {
      input['language_code']=['ENG'];
    }
    var where = {};

    if(phone_code && user_phone){
      where['phone_code'] = phone_code;
      where['user_phone'] = user_phone;
    }
    if(user_phone && !phone_code){
      res.send({status:0,error: "Missing phone_code"});
    }

    if(user_email){
        where['user_email'] = user_email;
    }

    if(social_user_id){
      where['social_user_id'] = social_user_id;
    }

    if(profile_picture){
      input['profile_picture'] = profile_picture;
    }


    function makeid(length) {
     var result           = '';
     var characters       = 'abcdefghijklmnopqrstuvwxyz0123456789';
     var charactersLength = characters.length;
     for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
     }
     return result;
    }

    function returnInformation(whereStatement){
      db.get().collection("aug_users").find(whereStatement).limit(1).skip(0).toArray(function(err, docs) {
       assert.equal(err, null);
         docs[0]['status'] = 1;
         return res.send(docs[0]);
      });
    }

    db.get().collection("aug_users").find(where).limit(1).skip(0).toArray(function(err, docs) {
      assert.equal(err, null);
      if(docs.length > 0){
        docs[0]['status'] = 2;
        res.send(docs[0]);
      }else{
        //check user name
        let finduser = {
                     "user_name" : user_name
          }
        db.get().collection("aug_users").find(finduser).limit(1).skip(0).toArray(function(err, docs) {
          assert.equal(err, null);

          //username found
          if(docs.length > 0){
                res.send({status:3,error:"User name already exists"});

          }else {
        //create new user
            const whereGuestToUser = { "device_id": input['device_id'], is_guest: true }
            db.get().collection("aug_users").find(whereGuestToUser).toArray(function(err, docs) {
              if(docs.length === 0){
                //whereNewUser

                db.get().collection("aug_users").insert(input,function(err, docsInserted) {
                 assert.equal(err, null);
                  if(docsInserted){
                      let findw = {
                         _id : ObjectId(docsInserted["ops"][0]["_id"])
                       }

                    var tada_user_id=docsInserted["ops"][0]["_id"];
                    var rc_input={}




                //==================rc_integration==================
                const randomPassword = makeid(8);
                const randomEmail = randomPassword + '@gmail.com';

                var myJSONObject = {
                  "username":input['user_name'],
                  "email":randomEmail, //input['user_email'] ? input['user_email'] :
                  "pass":randomPassword,
                  "name":input['user_name'], //input['full_name'] ? input['full_name'] : input['user_name'] --to avoid same user_name in full_name for chat
                  // "customFields":{
                  "tada_user_id":tada_user_id
                  // }
                }

                request({
                    //url: "https://tada.augray.com:3001/api/v1/users.register", //create
                    url: "http://13.250.36.41:3000/api/v1/users.register", //create
                    method: "POST",
                    headers: {
                        "content-type": "application/json",
                        "X-Auth-Token":"P_PWLgmxyAV25YmOXgjeZySd4TzWWop2vdv2T4_2_Ul",  //admin user for permission(tada)
                        "X-User-Id":"QKiDGDivtv8tZujc5",
                    },
                    json: true,
                    body: myJSONObject,
                }, function (error, response, body){ //res.send({dfg:response,fgG:body})
                    if(body && body.success && body.success === true){
                        rc_input['chat_email'] = myJSONObject.email;
                        rc_input['chat_password'] = myJSONObject.pass;
                        rc_input['chat_details'] = body.user;
                    }
                    else{
                    res.send({status:0,error:"Unable to register rocketchat user"});
                  }

                  let where_id = {
                     _id : ObjectId(tada_user_id)
                   }
                    db.get().collection("aug_users").findOneAndUpdate(where_id,{$set:rc_input},{upsert:false},function(err, docs) {
                     assert.equal(err, null);
                      if(docs){
                            returnInformation(findw);

                      }else{
                        res.send({status:0,error:"Unable to create new user"});
                      }

                   });
                });
              }
            })
             }else{
               //whereGuestToUser(Tada user)
              input['is_guest']=false;
              const randomPassword = makeid(8);
              const randomEmail = randomPassword + '@gmail.com';
              var myJSONObject = {
                "username":input['user_name'],
                "email":randomEmail, //input['user_email'] ? input['user_email'] :
                "pass":randomPassword,//pass
                "name":input['user_name'], //input['full_name'] ? input['full_name'] : input['user_name'] --to avoid same user_name in full_name for chat
                // "customFields":{
                "tada_user_id":input['_id']
                // }
              }
              request({
                  //url: "https://tada.augray.com:3001/api/v1/users.register", //create
                  url: "http://13.250.36.41:3000/api/v1/users.register", //create
                  method: "POST",
                  headers: {
                      "content-type": "application/json"
                  },
                  json: true,
                  body: myJSONObject,
              }, function (error, response, body){ //res.send({dfg:response,fgG:body})
                  if(body && body.success && body.success === true){
                      input['chat_email'] = myJSONObject.email;
                      input['chat_password'] = myJSONObject.pass;
                      input['chat_details'] = body.user;
                  }
                  else{
                    res.send({status:0,error:"Unable to register rocketchat user"});
                  }

                    db.get().collection("aug_users").findOneAndUpdate({ "device_id": input['device_id'] },{$set:input},{upsert:false},function(err, docs) {
                      assert.equal(err, null);
                      const updateNew = { _id : ObjectId(docs.value._id) }
                      db.get().collection("user_friends").insertOne({
                            following:ObjectId("5ec00bb1f97acc0e06f725c0"), //TadaTeam
                            follower: ObjectId(docs.value._id),
                            status: parseInt(1),
                            created_at: new Date(),
                            last_updated_at: new Date()
                          },function(err, docsInserted) {
                            assert.equal(null, err);
                            if(docsInserted!=0){

                              returnInformation(updateNew);
                            }
                            else{
                              res.send({status:0,message:"Cannot create TadaTeam chat"});
                            }
                          })
                      // returnInformation(updateNew);
                    })
              })
            }
          });
        //end create new user
        }
      });
      }
    })
};
