'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")

exports.avgetfavouritevideo = function(req, res) {
var userid = req.body.user_id; 

  if(userid == "" || userid == undefined){
    res.send({status:0,error:"missing input field"});
    return true;
  }
  
  var pagination_limit = req.body.pagination_limit || 50;
  var pagination_index = req.body.pagination_index || 1;

  var skipParams = parseInt(pagination_limit * (pagination_index - 1));
  var limitParams = parseInt(pagination_limit);  
 let videocount = (callback0) => {  
        db.get().collection("av_video").find({ favourite: { $elemMatch: { user_id: ObjectId(userid) } } }).count({},(err, docscount) => {
          assert.equal(err, null);
          callback0(null,docscount);
        });
      }
   let videodata = (callback1) => {                   
      db.get().collection("av_video").aggregate([
        { "$match" : { favourite: { $elemMatch: { user_id: ObjectId(userid) } } }},
        { "$skip" :  skipParams }, 
        { "$limit" :  limitParams},       
        { "$lookup": {
          "from": "av_video",
          "localField": "_id",
          "foreignField": "_id",
          "as": "av_videodata"
        }},  
           { "$unwind": {"path": "$av_videodata", "preserveNullAndEmptyArrays": true}}, 
        { "$project": { 
          "user_id" : 1,
          "thumb_image"   : 1,
          "video_path"   : 1,                        
          "is_public"   : 1, 
          "video_type"   : 1,   
          "is_favourite" : { $anyElementTrue: [ [ [ true ] ] ] } ,
          "last_update"   : { $dateToString: { format: "%d/%m/%Y %H:%M:%S", date: "$last_update" } }
            
       }} 
      ]).toArray((err, docs2) => {  
        assert.equal(err, null);   
        callback1(null,docs2)  
      });
          
     }  
          
      async.parallel({
      videocount: videocount,
      videodata: videodata
    },
    (err, results) => {       
        if (err) {
            res.send({
              status: 0
            })
            return
        }
        
          res.send({
          status:1,
          totalPages: Math.ceil(results.videocount / limitParams),
          currentPageIndex: pagination_index,
          totalCount: results.videocount,
          data: results.videodata  
        }) 
  }) 
}
 