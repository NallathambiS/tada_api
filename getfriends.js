'use strict';
var _ = require('lodash')
var async = require('async')
var mongo_lib = require('mongodb')
const assert = require('assert')
var db = require("../../db")
var ObjectId = mongo_lib.ObjectID

exports.getfriends = function(req, res) {
  let user_id = req.body.user_id;
  let search = req.body.search || "";

  let pagination_limit = req.body.pagination_limit || 50;
  let pagination_index = req.body.pagination_index || 1;
  let skipParams = parseInt(pagination_limit * (pagination_index - 1));
  let limitParams = parseInt(pagination_limit);
  let totalCount=0;

  if(!user_id || user_id ==""){
    let rescode = {
      status : 0,
      error  : "Missing mandatory elements user_id"
    }
    res.send(rescode);
    return false;
  }

    let where ={
      blocker : ObjectId(user_id)
    }

    db.get().collection("user_blocklist").find(where).project({blocked:1}).toArray(function(err, docs) {
      assert.equal(err, null);
      let blocked_users = [];
      for (var i = 0; i < docs.length; i++) {
        blocked_users.push(docs[i]['blocked'].toString());
      }
      let where = {
          "$or" : [{following:ObjectId(user_id)},{follower:ObjectId(user_id)}],
          status:1
      }
      let user_ids = [];
      db.get().collection("user_friends").find(where).toArray(function(err, docs2) {
        assert.equal(err, null);
        if(docs2 && docs2.length > 0){
          for (var i = 0; i < docs2.length; i++) {
            if(docs2[i]['following'] == user_id){
              user_ids.push(docs2[i]['follower'].toString());
            }else{
              user_ids.push(docs2[i]['following'].toString());
            }
          }
          let new_user_ids =  user_ids.filter(function(x) {
            return blocked_users.indexOf(x) < 0;
          });

          let new_user_objectids = [];
          for (var i = 0; i < new_user_ids.length; i++) {
            new_user_objectids.push({"_id":ObjectId(new_user_ids[i])});
          }

          let where_chat = {
            user_active:true,
            $or:new_user_objectids
          }

          db.get().collection("aug_users").find(where_chat).toArray(function(err, docs) {
            assert.equal(err, null);
            let chat_users = [];
            for (var i = 0; i < docs.length; i++) {
                if(docs[i]['chat_userId']!=""){
                  chat_users.push({"_id":ObjectId(docs[i]['_id'].toString())});
                }
            }

            let where_count = {
              user_active:true,
              $or:chat_users,

            }
            let where = {
              user_active:true,
              $or:chat_users
            }
           
            if(search!=""){ 
            where['user_name'] = { $regex: search, $options: 'i' } ;            
            where_count['user_name'] = { $regex: search, $options: 'i' } ;  
          }

            db.get().collection("aug_users").find(where_count).count(function(err, docscount) {
              assert.equal(err, null)
              totalCount= docscount
            }) 

            
            db.get().collection("aug_users").find(where).limit(limitParams).skip(skipParams).project({_id:1,user_status:1,user_name:1,user_email:1,issocial_login:1,user_gender:1,user_dob:1,profile_picture:1,chat_userId:1}).toArray(function(err, docs3) {
              assert.equal(err, null)
              res.send({
                status:1,
                totalPages: Math.ceil(totalCount / limitParams),
                currentPageIndex: pagination_index,
                totalCount: totalCount,
                data:docs3
              })
              return true
            })
          })
        } else {
          res.send({status:0,error:"no users found"});return true;
        }
      })
    })
}
