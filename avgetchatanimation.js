'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")

exports.avgetchatanimation = function(req, res) {

  var chatID = req.body.chat_type_id;
  let search = req.body.search || "";

  if(chatID == "" || chatID == undefined){
    res.send({status:0,error:"missing input fields"});
    return true;
  }

  var pagination_limit = req.body.pagination_limit || 50;
  var pagination_index = req.body.pagination_index || 1;

  var skipParams = parseInt(pagination_limit * (pagination_index - 1));
  var limitParams = parseInt(pagination_limit);

    async.waterfall([
      function(callback) {
        let where={'chat_animation_type_id': ObjectId(chatID),animation_enable:true}
        if (search!="") {
            where['animation_name'] = { $regex: search, $options: 'i' } ;            
        }
        db.get().collection("chat_animation").find(where).count({},function(err, count){
          assert.equal(err, null);
          callback(null, {
            totalCount: count
          });
        });
      },
      function(data, callback) {  
        let where = {
            "chat_animation_type_id": ObjectId(chatID), 
            "animation_enable":true
           }
        if (search!="") {
          where['animation_name'] = { $regex: search, $options: 'i' } ; 
        }
        db.get().collection("chat_animation").aggregate([
          { "$match" : where},
          { "$lookup": {
            "from": "av_asset",
            "localField": "s_plugin_asset_id",
            "foreignField": "_id",
            "as": "s_plugindata"
          }},
          { "$lookup": {
            "from": "av_asset",
            "localField": "r_plugin_asset_id",
            "foreignField": "_id",
            "as": "r_plugindata"
          }}, 
          { "$unwind": {"path": "$s_plugindata", "preserveNullAndEmptyArrays": true}}, 
          { "$unwind": {"path": "$r_plugindata", "preserveNullAndEmptyArrays": true}}, 
          { "$project": {
            _id:  1,
            "char_type_id"     : 1,
            "chat_animation_type_id"   : 1,
            "chat_mood_type_id" :1,
            "animation_name"   : 1,
            "file_path_ios"   : 1,
            "file_path_android"   : 1,
            "thumb_image"   : 1,
            "send_metadata"   : 1,
            "rec_metadata"   : 1,
            "animation_enable"   : 1,
            "s_plugin_asset_id": { "$cond": [{ "$eq": [ "$s_plugin_asset_id", ObjectId("000000000000000000000001") ] },"$$REMOVE", "$s_plugin_asset_id" ] },
            "r_plugin_asset_id": { "$cond": [{ "$eq": [ "$r_plugin_asset_id", ObjectId("000000000000000000000001") ] },"$$REMOVE", "$r_plugin_asset_id" ] },
            "s_plugin_asset"   : { "$cond": [{ "$eq": [ "$s_plugindata", null ] },"$$REMOVE", "$s_plugindata" ] },
            "r_plugin_asset"   : { "$cond": [{ "$eq": [ "$r_plugindata", null ] },"$$REMOVE", "$r_plugindata" ] },
            "country_code"   : 1,
            "point"   : 1,
            "last_update"   : { $dateToString: { format: "%d/%m/%Y %H:%M:%S", date: "$last_update" } }
          }}
        ]).toArray((err, docs2) => {
          assert.equal(err, null);   
           callback(null, {
        status: 1,
        totalPages: Math.ceil(data.totalCount / limitParams),
        currentPageIndex: pagination_index,
        totalCount: data.totalCount,
        data: docs2
      });
        });
                         
    }],
    function (err, result) {
        res.send(result)
      }); 
 
}
