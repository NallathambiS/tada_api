'use strict';
var _ = require('lodash')
var async = require('async')
var mongo = require('mongodb')
const assert = require('assert')
var db = require("../../db")
var ObjectId = mongo.ObjectID

exports.getusercontacts = function(req, res) {
    let user_id = req.body.user_id;

    if(!user_id && user_id == ""){
      res.send({status:0,error:"Missing mandatory elements user_id"});
      return true;
    }

    async.parallel([
      function(callback0) {
        let where = {
          user_id : ObjectId(user_id) 
        }
        db.get().collection("user_contacts").aggregate([
          { "$match" : where},
          { "$lookup": {
            "from": "aug_users",
            "localField": "phone_no",
            "foreignField": "user_phone",
            "as": "userObjects"
          }},
          { "$unwind": "$userObjects"},
          { "$group": {
            _id:  "$_id".valueOf(),
            "user_id"   : { "$first" : "$userObjects._id"  },
            "user_phone"   : { "$first" : "$userObjects.user_phone"  },
            "user_name"   : { "$first" : "$userObjects.user_name"  },
            "profile_picture"   : { "$first" : "$userObjects.profile_picture"  }
          }}
        ]).toArray(function(err, docs) {  
          assert.equal(err, null);
          let temparr=[]
          for (var i = 0; i < docs.length; i++) { 
            if(docs[i].user_id!=user_id){ 
              temparr.push(docs[i])
            }             
          }  
          callback0(null,temparr);
        });
      },
      function(callback1) {
        let where = {
          _id : ObjectId(user_id)
        }
        db.get().collection('aug_users').distinct("user_phone",{user_phone:{ $ne: "" }},function(err, vals) { 
          db.get().collection('user_contacts').find({user_id : ObjectId(user_id),phone_no: {$nin: vals}}).toArray(function(err, docs) { 
            assert.equal(err, null);
            let temp = [];
            for (var i = 0; i < docs.length; i++) {
              temp.push({contact_id:docs[i]['_id'],user_phone:docs[i]['phone_no'],user_name:docs[i]['contact_name']})
            }
            callback1(null,temp);
          });
        });
      }],
    function(err, results) {
      let result_data = {
        status:1,
        response:{
          tada_network:results[0],
          nontada_friends:results[1]
        }
      }

    res.send(result_data);
  })
}
