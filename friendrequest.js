'use strict'; 
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var AWS = require('aws-sdk')
var path = require('path')  
var url = require("url")
var db = require("../../db")
var Settings = require("../../lib/Settings");
const crypto = require("crypto");
var init = require("../../init");
  const gcm = require("node-gcm");
   var sender = new gcm.Sender(init.gcm_key);
  const apn = require("apn");
 var apnProvider = new apn.Provider(init.apnoptions);
  
exports.friendrequest = async function(req, res) {
  let follower = req.body.from_user_id
  let following = req.body.to_user_id
  let action = req.body.action

let followingid=following;
let followerid=follower;

  if(following === undefined || follower === undefined || action === undefined ||
    following === '' || follower === '' || action === ''){
    return res.send({"status":0,message:"Missing mandatory elements"})
  }

  if(!Array.isArray(following)){
    return res.send({"status":0,message:"to_userid is an array and should consist of alteast one element"});
  }

  if(following.length === 0){
    return res.send({"status":0,message:"to_userid array should consist of alteast one element"});
  }

  if(following.includes(follower)){
    return res.send({"status":0,message:"to_userid and from_userid cant be same"});
  }
 
    if(action == "INVITE"){  
      const awaitTask = await following.map(f => {

         db.get().collection('user_friends').find({follower:ObjectId(follower),following:ObjectId(f)}).toArray(function(err, findresult) { 
        assert.equal(err, null);
        if(findresult.length==0){ 
          db.get().collection("user_friends").insertOne({
            following:ObjectId(f),
            follower: ObjectId(follower),
            status: parseInt(0),
            created_at: new Date(),
            last_updated_at: new Date()
          },function(err, docsInserted) { 
            assert.equal(null, err);
            if(docsInserted!=0){    
            db.get().collection('aug_users').find({_id:ObjectId(f)}).toArray(function(err, result) {
            assert.equal(err, null);
            if(result && result[0] && result[0]['push_notification'] == true){  //send notification only if end user enable notification
                db.get().collection('aug_users_token').find({user_id:ObjectId(f)}).toArray(function(err, result2) {
                assert.equal(err, null);
                if(result2 && result2.length > 0 && result2[0]['token_key'] !=""){ 
                  var token_key = result2[0]['token_key'];
                  var platform = result2[0]['platform'];
                  db.get().collection('aug_users').find({_id:ObjectId(follower)}).toArray(function(err, result3) {  
                  assert.equal(err, null);
                  var friend_name = result3[0]['user_name'];
                  var profile_picture = result3[0]['profile_picture'];
                  db.get().collection('sn_notification_type').find({type_name:"friend_req_notification"}).toArray(function(err, result4) {
                    assert.equal(err, null);
                    var screen_id = result4[0]['screen_id'] || 2;
                    var msg_text = result4[0]['msg_text'] || "";
                    msg_text = msg_text.replace("{name}",friend_name)
                    var input_data = {
                      notification_type_id : result4[0]['_id'],
                      notification_type_name:"friend_req_notification",
                      notification_title:msg_text,
                      notification_message:"invited",
                      thumb_image:profile_picture,
                      seen:false,
                      isnew:true,
                      page:"chat",
                      receiver_id:ObjectId(f),
                      sender_id:ObjectId(follower),
                      last_update:new Date()
                    }
                    db.get().collection('sn_notifications').insert(input_data, function(err, result) { 
                      assert.equal(err, null);
                      
                      var payloadios = {
                        type:"friend_req_notification",
                        page:"chat",
                        sender_id:ObjectId(follower)
                      }
                      console.log(platform);
                      if(platform == "IOS"){
                        var note = new apn.Notification();
                        
                        note.alert = msg_text; 
                        note.payload = payloadios;
                        note.topic = "com.augray.avmessage";
                        note.aps.badge = 1;
                        note.aps.alert = msg_text;
                        note.aps.sound = "default";
                        // note['aps']['content-available'] = 1;
                        note.aps.category = "NEW_FRIEND_REQUEST"; 
                        apnProvider.send(note, token_key).then( (result) => {
                          console.log(result);
                          res.send({status:1,message:"Invite Sent"});  
                          // return true;
                        });
                      }else{
                        var regTokens = [token_key];

                        var datamsg = {}

                          datamsg['body'] = msg_text;
                          datamsg['type']="friend_req_notification";
                          datamsg['page']="chat";
                          datamsg['sender_id']=ObjectId(follower);
                          datamsg['sender_name']=friend_name;
                          datamsg['receiver_id']=ObjectId(f);
                          datamsg['image_url']=profile_picture; 
                        

                        var message = new gcm.Message({
                              data: datamsg 
                          });

                          sender.send(message, { registrationTokens: regTokens }, function (err, response) {
                              if (err) console.error(err);
                              else console.log(response);
                              res.send({status:1,message:"Invite Sent"});//,android_result:response
                              return true;
                          });
                      }
                    })
                  })
                })
              }  
              })                            
              } 
              else{
                res.send({status:0,message:"Invite not Sent"});
                return true;
              }
             }) 
            }
        })
    }
     else{
        res.send({status:1,message:"Invite sent already"});
        return true;
     }
     })
    }) 
  }

 
/*INVITE ends*/

    if(action == "ACCEPT"){ 
      console.log("accpeted called")
      let update_data = {
              $set : {
                status : parseInt(1),
                last_updated_at: new Date()
              }
            } 
          const awaitTask = await following.map(f => { 
            db.get().collection("user_friends").update({
              following:ObjectId(follower),
              follower: ObjectId(f)
              },update_data,function(err, docs) { 
              assert.equal(err, null);
              if(docs!=""){
                  db.get().collection('aug_users').find({_id:ObjectId(f)}).toArray(function(err, result) { 
                    assert.equal(err, null);

                    if(result && result[0] && result[0]['push_notification'] == true){
                      db.get().collection('aug_users_token').find({user_id:ObjectId(f)}).toArray(function(err, result2) { 
                        assert.equal(err, null); 
                        if(result2 && result2.length > 0 && result2[0]['token_key'] !=""){
                          var token_key = result2[0]['token_key'];
                          var platform = result2[0]['platform'];
                          db.get().collection('aug_users').find({_id:ObjectId(follower)}).toArray(function(err, result3) { 
                            assert.equal(err, null);
                            var friend_name = result3[0]['user_name'];
                            var profile_picture = result3[0]['profile_picture'];
                            db.get().collection('sn_notification_type').find({type_name:"receiver_accept_notification"}).toArray(function(err, result4) {
                              assert.equal(err, null); 
                              var screen_id = result4[0]['screen_id'] || 0;
                              var msg_text = result4[0]['msg_text'] || "";
                              msg_text = msg_text.replace("{name}",friend_name)
                              var input_data = {
                                notification_type_id : result4[0]['_id'],
                                notification_type_name:"receiver_accept_notification",
                                notification_title:msg_text,
                                notification_message:"accepted",
                                thumb_image:profile_picture,
                                seen:false,
                                isnew:true,
                                page:"chat",
                                receiver_id:ObjectId(f),
                                sender_id:ObjectId(follower),
                                last_update:new Date()
                              }
                              db.get().collection('sn_notifications').insert(input_data, function(err, result) {
                                assert.equal(err, null);  
                                db.get().collection('sn_notification_type').find({type_name:"friend_req_notification"}).toArray(function(err, result5) { 
                                assert.equal(err, null);    
                                var remove_data = {
                                  notification_type_id : ObjectId(result5[0]['_id']),    
                                  receiver_id:ObjectId(f),sender_id:ObjectId(follower)
                                }
                                 db.get().collection("sn_notifications").remove(remove_data,function(err, result6){ 
                                assert.equal(err, null);   
                                 
                                var payloadios = {
                                  type:"receiver_accept_notification",
                                  page:"chat", 
                                  sender_id:ObjectId(follower),
                                  sender_name:friend_name
                                }
                                console.log(platform);
                                if(platform == "IOS"){
                                  var note = new apn.Notification();
                                  
                                  note.payload = payloadios;
                                  note.aps.badge = 1;
                                  note.aps.sound = "default";
                                  note.aps.alert = msg_text;
                                  note.topic = "com.augray.avmessage";
                                  apnProvider.send(note, token_key).then( (result) => {
                                    console.log(result);
                                    res.send({status:1,message:"Request Accepted"}); 
                                    return true;
                                  });
                                }else{
                                  var regTokens = [token_key];
                                  var datamsg = {}
                                  datamsg['body'] = msg_text;
                                  datamsg['type']="receiver_accept_notification";
                                  datamsg['page']="chat";
                                  datamsg['sender_id']=ObjectId(follower);
                                  datamsg['sender_name']=friend_name;
                                  datamsg['receiver_id']=ObjectId(f);
                                  datamsg['image_url']=profile_picture; 
                                
                                  var message = new gcm.Message({
                                        data: datamsg 
                                    });
                                    sender.send(message, { registrationTokens: regTokens }, function (err, response) {
                                        if (err) console.error(err);
                                        else console.log(response);
                                        res.send({status:1,message:"Request Accepted"});  
                                        return true;
                                    });
                                }
                              })
                               })   
                               })    
                            })
                          })
                        }else{
                          res.send({status:1,message:"Request Accepted"});
                          return true;
                        }
                    })                 
                }else{
                  res.send({status:1,message:"Request Accepted"});
                  return true;
                }
                })  

              }
              else{
                res.send({status:0,message:"Accept failed"});
              }

            });
          }) 
    };
/*Accept ends*/

    if (action === 'DENY') {
          const awaitTask = await following.map(f => {
            db.get().collection("user_friends").remove({
              following:ObjectId(follower),
              follower: ObjectId(f)
              },function(err,docs){
              assert.equal(err, null);
              if(docs!=""){
                res.send({status:1,message:"Request Denied"});
              }
              else{
                res.send({status:0,message:"Deny failed"});
              }
            })
          })
      }
//     }
//     else{
//       res.send(status:0,message:"to_user not found")
//     }
//   })
//   }
//   else{
//     res.send(status:0,message:"from_user not found")
//   }
// })

}