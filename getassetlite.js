'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")

exports.getassetlite = function(req, res) {

  
  var pagination_limit = req.body.pagination_limit || 50;
  var pagination_index = req.body.pagination_index || 1;

  var skipParams = parseInt(pagination_limit * (pagination_index - 1));
  var limitParams = parseInt(pagination_limit);

    async.waterfall([
      function(callback) {
        db.get().collection("aug_assetlite").find().count({},function(err, count){ 
          assert.equal(err, null);
          callback(null, {
            totalCount: count
          });
        });
      },
      function(data, callback) {
            db.get().collection("aug_assetlite").find().limit(limitParams).skip(skipParams).toArray(function(err, docs) {
            assert.equal(err, null);
            callback(null, {
              status: 1,
              totalPages: Math.ceil(data.totalCount / limitParams),
              currentPageIndex: pagination_index,
              totalCount: data.totalCount,
              data: docs
            });
          });
      },
      ], function (err, result) {
      res.send(result)
      });
}
