'use strict';
var async = require('async')
var mongo = require('mongodb')
const assert = require('assert')
var ObjectId = mongo.ObjectID
var request = require('request')
var fs = require('fs')
var path = require('path')
var db = require("../../db")

exports.chatgroupget = function(req, res) {

try{

 
      var group_members = req.body.group_members;
      var groupname = req.body.groupname; 

      if(admin_user_id === undefined || admin_user_id === "" || group_members === undefined || group_members === ""
        || groupname === undefined || groupname === ""){
        res.send({status:0,error: "Missing mandatory elements"});
        return true;
      }

      if(!Array.isArray(group_members)){
        return res.send({"status":0,message:"group_members is an array and should consist of alteast one element"});
      }

      if(group_members.length === 0){
        return res.send({"status":0,message:"group_members array should consist of alteast one element"});
      }

      db.get().collection("aug_users").find({_id : ObjectId(admin_user_id)}).limit(1).skip(0).toArray(function(err, docs){
      assert.equal(err, null);

      var myJSONObject = {
        "name": groupname,
        "members": group_members,
        "customFields":{"groupicon": group_icon}
      }

      // res.send(myJSONObject);
      for(var i=0;i<group_members.length;i++){

      }
        if(docs[0] && docs[0]['chat_authToken'] && docs[0]['chat_userId']){
          request({
              url: "https://chat.augray.com/api/v1/groups.info?roomName=" + group_members[i] + "",
              method: "POST",
              headers: {
                  "content-type": "application/json",
                  "X-Auth-Token": docs[0]['chat_authToken'],
                  "X-User-Id": docs[0]['chat_userId'],
              },
              json: true,
              body: myJSONObject,
          },
           function (error, response, body){
            console.log(body)
              if(body.success === true){
                res.send({status:1,group_details:body.group})
              }else{
                res.send({status:0})
              }
          });
        }else{
          res.send({status:0})
        }

    })
    }
    catch(errorcatch){
      console.log(errorcatch);
    }
}
