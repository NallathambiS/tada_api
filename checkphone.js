'use strict';
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")


exports.checkphone = function(req, res) {

  let phone_code = req.body.phone_code
  let user_phone = req.body.user_phone
  let user_email = req.body.user_email
  let social_user_id = req.body.social_user_id

  let totfollowingcount=0;
  let totfollowercount=0;

   if(!user_phone && !user_email && !social_user_id){
    res.send({status:0,error:"missing input fields user_phone or user_email"})
      return true
   }
   if(user_phone){
    if( phone_code == "" || phone_code == undefined ){
      res.send({status:0,error:"missing input fields phone_code"})
      return true
    }
   }

   let where = {}
   if (user_phone){
   //  where['phone_code'] = phone_code
     where['user_phone'] = user_phone
   } else if(social_user_id){
     where['social_user_id'] = social_user_id
   } else if(user_email){
     where['user_email'] = user_email
   }

    db.get().collection("aug_users").find(where).limit(1).skip(0).toArray(function(err, docs) {
      assert.equal(err, null)
      if(docs.length > 0){
        let where_following = {
          follower : ObjectId(docs[0]['_id']),
          status:1
        }
        let where_follower = {
          following : ObjectId(docs[0]['_id']),
          status:1
        }

        db.get().collection("user_friends").find(where_following).count({},function(err, count1){
            assert.equal(err, null)
            totfollowingcount=count1;

        /*docs[0]['status'] = 1
        res.send(docs[0])*/
        db.get().collection("user_friends").find(where_follower).count({},function(err, count2){
            assert.equal(err, null)
            totfollowercount=count2;

        let data={}
        data =docs[0];
        data['following_count']=totfollowingcount;
        data['followers_count']=totfollowercount;
         res.send({
         'status' : 1,
         data
        })
      })
      })
      }else{
        //user not found
        res.send({status:0,error:"user not found"})
      }

    });


};
