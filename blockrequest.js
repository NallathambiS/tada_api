'use strict';
var _ = require('lodash')
var async = require('async')
var mongo_lib = require('mongodb')
const assert = require('assert')
var db = require("../../db")
var ObjectId = mongo_lib.ObjectID

exports.blockrequest = async function(req, res) {
  var blocker = req.body.from_user_id;
  var blocked = req.body.to_user_id;
  var action = req.body.action; 
  
  if(blocker === undefined || blocked === undefined ||
    blocker === '' || blocked === ''){
    return res.send({"status":0,message:"Missing mandatory elements"})
  }

  if(!Array.isArray(blocked)){
    return res.send({"status":0,message:"to_userid is an array and should consist of alteast one element"})
  }

  if(blocked.length === 0){
    return res.send({"status":0,message:"to_userid array should consist of alteast one element"})
  }

  if(blocked.includes(blocker)){
    return res.send({"status":0,message:"to_userid and from_userid cant be same"})
  }
 
    
      if(action === 'BLOCK'){
        const awaitTask = await blocked.map(b => {
          db.get().collection("user_blocklist").insertOne(
            { blocker:ObjectId(blocker), 
              blocked:ObjectId(b), 
              created_at: new Date() 
            },function(err, docsInserted) {
            assert.equal(null, err);
          })
          res.send({status:1,message:"blocked successfully"});
        })
      }else if (action === 'UNBLOCK') {
        const awaitTask = await blocked.map(b => {
          db.get().collection("user_blocklist").remove(
            { blocker:ObjectId(blocker),
              blocked:ObjectId(b) 
            },function(err,docs){
            assert.equal(null, err);
          })
        })
        res.send({status:1,message:"unblocked successfully"});
      }
}
