'use strict';
var Const = require("../../const");
var _ = require('lodash');
var async = require('async');
const assert = require('assert');
var db = require("../../db")
const {ObjectId} = require('mongodb');

exports.getchatavatar = function(req, res) {
    let chat_user_id = req.body.chat_user_id
    let chat_user_name = req.body.chat_user_name
    if((!chat_user_id || chat_user_id == "") && (!chat_user_name || chat_user_name == "")){
    res.send({
      status : 0,
      error  : "Missing mandatory element chat_user_id/chat_user_name"
    });
    return true
    }
    let whereuser={}
    if(chat_user_id!=""){
       whereuser ={chat_userId:chat_user_id}
    }
    if(chat_user_name!=""){
       whereuser ={user_name:chat_user_name}
    }
    db.get().collection("aug_users").find(whereuser).toArray(function(err, userdocs) {
      assert.equal(err, null);
      if(userdocs.length !== 0){
        var user_id= ObjectId(userdocs[0]._id)
        var where = {user_id : user_id};
    db.get().collection("av_avatar").aggregate([
      { "$match" : where},
      { "$lookup": {
        "from": "av_asset",
        "localField": "assets.asset_id",
        "foreignField": "_id",
        "as": "assets_list"
      }}
    ]).toArray(function(err, docs) {
      assert.equal(err, null)
      if(docs.length !== 0){
        res.send({
          "status" : 1,
          "response" : docs[0]
        })
      }else{
        res.send({
          "status" : 0,
          "message": "avatar not found"
        })
      }

    })
  }
  else {
    res.send({"status" : 0,
    "message": "user not found"})
  }
})
};
