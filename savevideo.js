'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")
var Settings = require("../../lib/Settings");
const crypto = require("crypto");
var init = require("../../init");
  const gcm = require("node-gcm");
   var sender = new gcm.Sender(init.gcm_key);
  const apn = require("apn");
 var apnProvider = new apn.Provider(init.apnoptions);


exports.savevideo = function(req, res) {
    var video_user_id =  req.body.user_id;
    var video_path =  req.body.video_path;     
    var is_public =  req.body.is_public;
    var thumb_image =  req.body.thumb_image;
    var videotype =  req.body.video_type;
    let checkinput={}
    let input_data={}

    if(is_public=="1"){
      is_public=true
    }
    if(is_public=="0"){
      is_public=false
    }

    checkinput['video_user_id']=input_data['video_user_id']=ObjectId(video_user_id);
    checkinput['video_path']=input_data['video_path']=video_path;
    checkinput['is_public']=input_data['is_public']=is_public;
    checkinput['thumb_image']=input_data['thumb_image']=thumb_image;  
    checkinput['video_type']=input_data['video_type']=videotype;  
    input_data['last_update']=new Date();
    input_data['favourite']=[];
    
    for(var item in checkinput) {
      if(checkinput[item] == "" && checkinput[item] == undefined){
          res.send({status:0,error: "Missing mandatory elements "+item});
          return true;
      }
    }   
      db.get().collection('av_video').insertOne(input_data,  function(err, result) { 
        assert.equal(err, null);
        if(result.insertedCount && result.insertedCount > 0){
          let video_id =  ObjectId(result["ops"][0]["_id"])
                       

        let where_videoid={video_user_id:ObjectId(video_user_id)} 
 
        db.get().collection("av_video").find(where_videoid).count(function(err, count){ 
        assert.equal(err, null);
        if(count==1){
 
          var where_friends = {
          "$or" : [{following:ObjectId(video_user_id)},{follower:ObjectId(video_user_id)}],
          status:1
          }
          let user_ids = [];
          db.get().collection("user_friends").find(where_friends).toArray(function(err, docs2) { 
            assert.equal(err, null);
            if(docs2 && docs2.length > 0){ 
              for (var i = 0; i < docs2.length; i++) {
                if(docs2[i]['following'] == video_user_id){
                  user_ids.push(docs2[i]['follower'].toString());
                }else{
                  user_ids.push(docs2[i]['following'].toString());
                }
              }
              let new_user_objectids = [];
              for (var j = 0; j < user_ids.length; j++) {
                new_user_objectids.push(user_ids[j]);
              } 
    
          for(var k = 0; k < new_user_objectids.length; k++){
          var wherenewid= new_user_objectids[k];
           db.get().collection('aug_users').find({_id:ObjectId(wherenewid)}).toArray(function(err, result) {  

                    assert.equal(err, null);
                    
                    if(result && result[0] && result[0]['push_notification'] == true){
                      db.get().collection('aug_users_token').find({user_id:ObjectId(wherenewid)}).toArray(function(err, result2) { 
                        assert.equal(err, null); 
                        if(result2 && result2.length > 0 && result2[0]['token_key'] !=""){
                          var token_key = result2[0]['token_key'];
                          var platform = result2[0]['platform'];

                          db.get().collection('aug_users').find({_id:ObjectId(video_user_id)}).toArray(function(err, result3) { 

                            assert.equal(err, null);
                            var friend_name = result3[0]['user_name'];
                            var profile_picture = result3[0]['profile_picture'];
                            db.get().collection('sn_notification_type').find({type_name:"first_post_tada_notification"}).toArray(function(err, result4) { 
                              assert.equal(err, null); 
                              var screen_id = result4[0]['screen_id'] || 2;
                              var msg_text = result4[0]['msg_text'] || "";
                              msg_text = msg_text.replace("{name}",friend_name);

                              var input_data = {
                                notification_type_id : result4[0]['_id'],
                                notification_type_name:'first_post_tada_notification',
                                notification_title:msg_text,
                                notification_message:"first post",
                                thumb_image:profile_picture,
                                video_thumb_image:thumb_image,
                                seen:false,
                                isnew:true,
                                post_id:ObjectId(video_id),
                                page:"notification thread",
                                receiver_id:ObjectId(video_user_id),
                                sender_id:ObjectId(wherenewid),
                                last_update:new Date()
                              }
                              db.get().collection('sn_notifications').insertOne(input_data, function(err, result5) {
                                assert.equal(err, null);  
                                  
                                var payloadios = {
                                  type:"first_post_tada_notification",
                                  page:"notification thread", 
                                  sender_id:ObjectId(video_user_id),
                                  post_id:video_id
                                  // thumb_image:thumb_image 
                                }
                                console.log(platform); 
                                if(platform == "IOS"){  
                                  var note = new apn.Notification();
                                  note.aps.badge = 1;
                                  note.aps.alert = msg_text;
                                  note.aps.sound = "default";
                                  note.payload = payloadios; 
                                  note.topic = "com.augray.avmessage"; 

                                  apnProvider.send(note, token_key).then( (result) => {
                                    console.log(result); 
                                     // return true;
                                  });
                                }else{  
                                  var regTokens = [token_key];
                                  var datamsg = {}
                                  datamsg['body'] = msg_text;
                                  datamsg['type']="first_post_tada_notification";
                                  datamsg['page']="notification thread";
                                  datamsg['sender_id']=ObjectId(video_user_id);
                                  datamsg['sender_name']=friend_name; 
                                  datamsg['post_id']=video_id;
                                  datamsg['image_url']=profile_picture;
                                  // datamsg['thumb_image']=thumb_image;
                                  var message = new gcm.Message({
                                        data: datamsg 
                                    });  
                                    sender.send(message, { registrationTokens: regTokens }, function (err, response) {
                                        if (err) console.error(err);
                                        else console.log(response); 
                                         // return true;
                                    });
                                }
                              })
                              })
                            })
                            }
                            })
                      }
                    })
                }//for
              }//if
            })
        }
        })

           res.send({status:1, message: "Video saved successfully"})                     
        }
        else{
          res.send({status:0, error: "Failed to save video"})
        } 
   });          
       
}
