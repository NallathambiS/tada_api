'use strict';
var Const = require("../../const");
var _ = require('lodash');
var async = require('async');
const assert = require('assert');
var db = require("../../db")
const {ObjectId} = require('mongodb');

exports.getavatar = function(req, res) {
    let user_id = req.body.user_id
    if(!user_id || user_id == ""){
    res.send({
      status : 0,
      error  : "Missing mandatory element user_id"
    });
    return true
    }

    let where = { user_id : ObjectId(user_id) }
    db.get().collection("av_avatar").aggregate([
      { "$match" : where},
      { "$lookup": {
        "from": "av_asset",
        "localField": "assets.asset_id",
        "foreignField": "_id",
        "as": "assets_list"
      }}
    ]).toArray(function(err, docs) {
      assert.equal(err, null)
      if(docs.length !== 0){
        res.send({
          "status" : 1,
          "response" : docs[0]
        })
      }else{
        res.send({
          "status" : 0,
          "message": "avatar not found"
        })
      }

    })
};
