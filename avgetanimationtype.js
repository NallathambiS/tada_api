'use strict';
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb');  

exports.avgetanimationtype = function(req, res) {

async.waterfall([
function(callback) {
db.get().collection("av_animation").distinct("animation_type_id",{animation_type_id:{ $ne: "" }},function(err, docs1) { 
  assert.equal(err, null);   
      callback(null, {
            docs:docs1
          });
    });
},
function(data, callback) { 
            db.get().collection("av_animation_type").find({_id:{$in:data.docs}}).toArray(function(err, docs) { 
            assert.equal(err, null);
            callback(null, {
              status: 1,              
              animation_type: docs
            });
          });
      },
      ], function (err, result) {
      res.send(result)
      });
}
