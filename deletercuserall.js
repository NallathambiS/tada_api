'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')
var request = require('request')

exports.deletercuserall = function(req, res) {

    let where ={
      user_name : {$nin: ["adminrc","admin","tadateam","nallathambi","tada","gobinath","augray","rocket.cat","bmanimsc"]}
    }
      let user= (callback0)=> {
        db.get().collection("aug_users").find(where).toArray((err, docs)=>{
          assert.equal(err, null);
          callback0(null, docs);
        })
      }

      async.parallel({
        data: user
      },

      (err, results) =>{ 
        if (err) {
          res.send({
            status: 0
          })
          return
         }
        if(results['data'].length !== 0){ 

          for (var i = 0; i < results['data'].length; i++) {
          var data = {
            userdata: results['data'][i]
          };

          if(data){
            var myJSONObject = {
              "username": data['userdata']['user_name']
            };

             request({
                url: "https://chat.augray.com/api/v1/users.delete",
                method: "POST",
                headers: {
                     "Content-Type":"application/json",
                      "X-Auth-Token":"_XBAlXZmzr_HzN25NTEa5O6443npSvJBIROCvHU3vBB",  //admin user for permission(tadateam)
                      "X-User-Id":"tFcM3aCuBo6dc5tWQ",
                },
                json: true,
                body: myJSONObject,
            },  (error, response, body)=>{
                console.log(body)
                if(body.success === true){
                  res.send({status:1,message:body.success})
                  // return false;
                  // console.log('success');
                }else{
                  //res.send({status:0})
                   console.log('failed');
                }
            });
          }else{
            // res.send({status:0})
            console.log('failed');
          }
        }
        // res.send({asd:results['data']})
        res.send({status:1})
        }
        else{
          res.send({status:0})
        }
      })
      
}
