'use strict';
var _ = require('lodash')
var async = require('async')
var mongo_lib = require('mongodb')
const assert = require('assert')
var db = require("../../db")
var ObjectId = mongo_lib.ObjectID

exports.pendingrequestcount = function(req, res) {
    let user_id = req.body.user_id   
    let totalCount

    if(!user_id || user_id == ""){
      let rescode = {
        status : 0,
        error  : "Missing mandatory elements user_id"
      }
      res.send(rescode);
      return false;
    }

    let where ={
      $or: [{blocker : ObjectId(user_id)},{blocked:ObjectId(user_id)}]
    }
    db.get().collection("user_blocklist").find(where).project({blocked:1,blocker:1}).toArray(function(err, docs) {
      assert.equal(err, null)
      let blocked_users = []
      for (var i = 0; i < docs.length; i++) {
        if(docs[i]['blocker'] == user_id){
          blocked_users.push(docs[i]['blocked']);
        }else{
          blocked_users.push(docs[i]['blocker']);
        }
      }

      let where = {
          //"$or" : [{following:ObjectId(user_id)},{follower:ObjectId(user_id)}],
          following:ObjectId(user_id),
          status:0
      }
      let user_ids = [];
      db.get().collection("user_friends").find(where).toArray(function(err, docs2) {
        assert.equal(err, null)
        if(docs2 && docs2.length > 0){
          for (var i = 0; i < docs2.length; i++) {
            if(docs2[i]['following'] == user_id){
              user_ids.push(docs2[i]['follower']);
            }else{
              user_ids.push(docs2[i]['following']);
            }
          }
          let where = {
            user_active:true,
            is_guest:false,
            _id:{$ne:ObjectId(user_id)},
            _id:{$nin:blocked_users},
            _id:{$in:user_ids},
          }
          
        db.get().collection("aug_users").find(where).count({},function(err, count){ 
          assert.equal(err, null);          
          res.send({
            status:1,
            totalCount: count
           })          
        })
      }
      else{
        res.send({
            status:1,
            totalCount: 0
           }) 
      }
    })
  })
}
