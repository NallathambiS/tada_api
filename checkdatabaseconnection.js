'use strict';
var init = require("../../init")
var Const = require("../../const")
var db = require("../../db")
const assert = require('assert')
const { ObjectId } = require('mongodb')

exports.checkdatabase = async (req, res) => {

  let databaseCheck = (request) => {

    let query  = {
      phone_code : request.phone_code,
      user_phone : request.user_phone
    }

    return new Promise((resolve, reject) => {
       db.get().collection("aug_users").find(query).limit(1).skip(0).toArray((err, data) => {
         assert.equal(err, null);
         err ? reject(err) : resolve(data)
       })
    })
  }


  try {

      console.log("check database connection")

      let dbCheck = await databaseCheck(req.body)

      if (dbCheck.length > 0) {
        dbCheck[0]['status'] = 1
        res.send(dbCheck[0])
        return

      } else {
        //user not found
        res.send({status:0,error:"user not found"})
        return
      }

  } catch (err) {
      res.status(500).json({
        "success": false,
        "error": err.message
      })
      return
  }
};
