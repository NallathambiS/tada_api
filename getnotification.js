'use strict';
var _ = require('lodash');
var async = require('async'); 
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')
var dateFormat = require('date-fns');

exports.getnotification = function(req, res) { 
    var input_data = {};
    var user_id =  req.body.user_id;
    var search =  req.body.search; 
    var limit =  req.body.limit; 
	
    if(!user_id || user_id==""){
      var rescode = {
        status : 0,
        error  : "Missing mandatory element userid"
      }
      res.send(rescode);
  }  
            var where = {
              receiver_id : ObjectId(user_id),
              page:{$ne:"chat_message"} 
            }
          
          db.get().collection("aug_users").find({_id:ObjectId(user_id)}).toArray(function(err, docs) {
          assert.equal(err, null)
          if(docs.length>0){
          db.get().collection("sn_notifications").aggregate([
          { "$match" : where},
            { "$lookup": {
              "from": "aug_users",
              "localField": "receiver_id",
              "foreignField": "_id",
              "as": "notificationObjects"
            }},
            { "$lookup": {
              "from": "aug_users",
              "localField": "sender_id",
              "foreignField": "_id",
              "as": "sendernotificationObjects"
            }},
            { "$lookup": {
              "from": "sn_notification_type",
              "localField": "notification_type_id",
              "foreignField": "_id",
              "as": "notificationtypeObjects"
            }},
          { "$unwind": {"path": "$sendernotificationObjects", "preserveNullAndEmptyArrays": true}}, 
          { "$unwind": {"path": "$notificationtypeObjects", "preserveNullAndEmptyArrays": true}}, 
          { "$project": {
            _id:  1,
            "notification_type_id"     : 1,
            "notification_type_name"   : 1,
            "notification_title"   : 1,
            "notification_message"   : "",
            "receiver_id"   : 1,
            "sender_id"   : 1,  
            "post_id": { "$cond": [{ "$eq": [ "$post_id", null ] },"$$REMOVE", "$post_id" ] }, 
            "thumb_image"   : 1,
            "video_thumb_image" : { "$cond": [{ "$eq": [ "$video_thumb_image", null ] },"$$REMOVE", "$video_thumb_image" ] }, 
            "seen"   : 1,
            "profile_picture"    :  "$sendernotificationObjects.profile_picture"  ,
            "sender_name"   :  "$sendernotificationObjects.user_name" ,
            "last_update"   : { $dateToString: { format: "%d/%m/%Y %H:%M:%S", date: "$last_update" } }
          }}
        ]).sort({_id:-1}).toArray((err, docs) => {
           var result_data = {
              "status" : 1,
              "data" : docs
            }
            res.send(result_data)
      });         
      }
    })
    }