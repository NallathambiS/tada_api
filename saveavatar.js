'use strict';
var Settings = require("../../lib/Settings");
var init = require("../../init");
var _ = require('lodash');
var async = require('async');
var mongo = require('mongodb');
const assert = require('assert')
var request = require('request');
var ObjectId = mongo.ObjectID;
var AWS = require('aws-sdk');
var path = require('path');
var url = require("url");
var fs = require("fs");
var db = require("../../db");

exports.saveavatar = function(req, res) {
  try{
    var gender =  req.body.gender;
    var user_id =  req.body.user_id;
    var device_id =  req.body.device_id;
    var facetexture =  req.body.face_texture;
    var orgthumbimage =  req.body.org_image;
    var thumbimage = req.body.thumb_image;
    var avatar_name = req.body.avatar_name;
    var faceshape = req.body.face_shape;
    var skin_color=  req.body.skin_color;
    var character_color=  req.body.character_color;
    var assets =  req.body.assets;
    let checkinput={}
    let update_data={}
    checkinput['gender']=gender;
    checkinput['thumb_image']=thumbimage;
    // checkinput['org_image']=orgthumbimage;
    checkinput['avatar_name']=avatar_name;
    checkinput['face_shape']=faceshape;
    checkinput['skin_color']=skin_color;
    checkinput['character_color']=character_color;
    checkinput['assets']=assets;
    for(var item in checkinput) {
      if(checkinput[item] == "" || checkinput[item] == undefined){
          res.send({status:0,error: "Missing mandatory elements "+item});
          return true;
      }
    }
    if(!user_id && !device_id){
      res.send({status:0,error:"Atleast one element required, user_id/device_id."});
      return true;
    }
    var path_profilepic = '';
    var path_profilepic_org = '';
    if(thumbimage != ""  && thumbimage != undefined){
      path_profilepic = thumbimage;
      update_data['profile_picture']  = thumbimage;
    }
    if(orgthumbimage != ""  && orgthumbimage != undefined){
      path_profilepic_org = orgthumbimage;
      update_data['profile_picture_org']  = orgthumbimage;
    }
    var tempassets = [];
    var deleteFileNames = [];
    const mapAssetID = assets.map( async a => {
             return await tempassets.push({
              asset_id: ObjectId(a.asset_id.toString()),
              name: a.name,
              color: a.color
            })
          });

    if(user_id){
      var where ={
     user_id: ObjectId(user_id)
      };
    }else if(device_id){
      var where ={
        device_id: device_id
      };
    }

    function set_avatar(findw,path_profilepic){
      db.get().collection("aug_users").find({_id:findw}).limit(1).skip(0).toArray(function(err, docs2){
          assert.equal(err, null);
          console.log("set dp process");
        if(docs2.length !== 0){
          var data = {
             from: docs2 && docs2[0]
          };
          if(data){
            var myJSONObject = {
              avatarUrl: path_profilepic,
              };
            request({
                //url: "https://tada.augray.com:3001/api/v1/users.setAvatar",
                url: "https://chat.augray.com/api/v1/users.setAvatar",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    "X-Auth-Token": data['from']['chat_authToken'],
                    "X-User-Id": data['from']['chat_userId']
                },
                json: true,
                body: myJSONObject,
            }, function (error, response, body){
                if(body.success === true){
                  res.send({
                         status: 1,
                         message: 'Saved Successfully'
                        })
                }else{
                  res.send({status:0, error: "Failed to save avatar. Check image input"})
                }
            });
          }else{
            res.send({status:0})
          }
        }
      })
    }

    db.get().collection('av_avatar').find(where).toArray(function(err, result) {
      assert.equal(err, null);
      if(result.length === 0){
                  var input_data = {
                    avatar_name     : avatar_name,
                    gender          : gender,
                    user_id         : ObjectId(user_id),
                    thumb_image     : thumbimage,
                    org_image       : orgthumbimage ? orgthumbimage : undefined,
                    face_shape      : faceshape,
                    face_texture    : facetexture ? facetexture : undefined,
                    skin_color      : skin_color,
                    character_color : character_color,
                    assets          : tempassets,
                    create_date     : new Date(),
                    last_update     : new Date()
                  };
                  db.get().collection('av_avatar').insertOne(input_data,  function(err, result) {
                    assert.equal(err, null);
                    console.log("avatar inserted");
                    if(result.insertedCount && result.insertedCount > 0){
                    let update_input_data = {
                       is_avatarcreated     : true,
                       profile_picture      : thumbimage,
                       profile_picture_org  : orgthumbimage
                    }

                      db.get().collection("aug_users").findOneAndUpdate({_id: ObjectId(user_id)}, {$set: update_input_data}, {returnOriginal:false} , function(err, result) {
                      assert.equal(err, null);
                      console.log("set dp starts");
                        if(result && result.value){
                          return set_avatar(ObjectId(user_id),path_profilepic);
                        }
                        else{
                          res.send({status:0, error: "Failed to save avatar because user_id not found"})
                        }
                    });
                }else{
                      res.status(200).send({status:0});
                      return true;
                    }
              });
      }
      //update
      else{


       db.get().collection('av_avatar').find(where).toArray(function(err, docs2) {
       assert.equal(err, null);
       var facetexture_old= docs2[0]['face_texture'];
       var org_image_old= docs2[0]['org_image'];
       var thumb_image_old= docs2[0]['thumb_image'];


    //   var s3 = new AWS.S3({
    //          accessKeyId: Settings.s3.key,
    //          secretAccessKey: Settings.s3.secret
    //        });
    //   deleteFileNames.push(path.basename(facetexture_old))
    //   deleteFileNames.push(path.basename(org_image_old))
    //   deleteFileNames.push(path.basename(thumb_image_old))

    //   console.log(deleteFileNames);
    //   const deleteSThreePath = deleteFileNames.map(async a => {
    //   var params = {
    //       Bucket: Settings.s3.bucketnew,
    //       Key: Settings.s3['path-avatar'] + a
    //   };
    //   await s3.headObject(params, function (err, metadata) {
    //     if (!err || err.code != 'NotFound') {
    //       s3.deleteObject(params, function(err, data) {
    //        if (err) console.log(a,err, err.stack); // an error occurred
    //        else     console.log(data);           // successful response
    //       });
    //     }
    //   });

    // })


      update_data = {
      avatar_name     :   avatar_name,
      gender          :   gender,
      user_id         :   ObjectId(user_id),
      assets          :   tempassets,
      face_texture    :   facetexture,
      org_image       :   orgthumbimage,
      face_shape      :   faceshape,
      skin_color      :   skin_color,
      character_color :   character_color,
      thumb_image     :   thumbimage,
      create_date     :   new Date(),
      last_update     :   new Date()
    };
    db.get().collection('av_avatar').findOneAndUpdate(where, {$set: update_data}, function(err, result) {
      assert.equal(err, null);
      console.log("update avatar starts");
      if(result && result.value){
      //set thumb image to profile pic if user updated in profile
      setTimeout(function(){
       db.get().collection('av_avatar').find(where).toArray(function(err, docs1) {
             assert.equal(err, null);
              if (docs1.user_profile_picture==1) {
                  let update_pic={
                    profile_picture    : thumbimage,
                    profile_picture_org: orgthumbimage ? orgthumbimage : undefined
                  }
                  db.get().collection('aug_users').findOneAndUpdate({_id: ObjectId(user_id)}, {$set: update_pic}, {returnOriginal:false} , function(err, result) {
                  assert.equal(err, null);

                  })
            }
      }, 300);

      })

      res.send({
        status: 1,
        message: 'Updated Successfully'
      })
}
     else{
        res.send({status: 0,
        error: 'Failed to update avatar'
      })
      }
      return true;
      })
    })
      }
    })
}
catch(catcherror){
  console.log(catcherror)
}
}
