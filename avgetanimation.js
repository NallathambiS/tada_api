'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")

exports.avgetanimation = function(req, res) {
var animID = req.body.animation_type_id;
let search = req.body.search || "";

  if(animID == "" || animID == undefined){
    res.send({status:0,error:"missing input field"});
    return true;
  }

  var pagination_limit = req.body.pagination_limit || 50;
  var pagination_index = req.body.pagination_index || 1;

  var skipParams = parseInt(pagination_limit * (pagination_index - 1));
  var limitParams = parseInt(pagination_limit); 
 
 async.waterfall([
 function(callback) {  
        let where={'animation_type_id': ObjectId(animID),animation_enable:true}
        if (search!="") {
            where['animation_name'] = { $regex: search, $options: 'i' } ;            
        }
        db.get().collection("av_animation").find(where).count({},(err, docscount) => {
          assert.equal(err, null);
          callback(null, {
            totalCount: docscount
          });
        });
      },
   function(data, callback) {  
          let where = {
          animation_type_id : ObjectId(animID),
          animation_enable:true
        }
        if (search!="") {
            where['animation_name'] = { $regex: search, $options: 'i' } ;            
        }
        db.get().collection("av_animation").aggregate([
          { "$match" : where},
          { "$lookup": {
            "from": "av_asset",
            "localField": "plugin_asset_id",
            "foreignField": "_id",
            "as": "plugindata"
          }}, 
          { "$unwind": {"path": "$plugindata", "preserveNullAndEmptyArrays": true}}, 
          { "$project": {
            _id:  1,
            "char_type_id"     : 1,
            "animation_type_id"   : 1,
            "animation_name"   : 1,
            "file_path_ios"   : 1,
            "file_path_android"   : 1,
            "thumb_image"   : 1,
            "metadata"   : 1,
            "animation_enable"   : 1,
            "plugin_asset_id": { "$cond": [{ "$eq": [ "$plugin_asset_id", null ] },"$$REMOVE", "$plugin_asset_id" ] },
            "plugin_asset"   : { "$cond": [{ "$eq": [ "$plugindata", null ] },"$$REMOVE", "$plugindata" ] },
            "country_code"   : 1,
            "point"   : 1,
            "last_update"   : { $dateToString: { format: "%d/%m/%Y %H:%M:%S", date: "$last_update" } }
          }}
        ]).toArray((err, docs2) => {
          assert.equal(err, null);   
           callback(null, {
        status: 1,
        totalPages: Math.ceil(data.totalCount / limitParams),
        currentPageIndex: pagination_index,
        totalCount: data.totalCount,
        data: docs2
      });
        });
                         
    }],
    function (err, result) {
        res.send(result)
      }); 
}
