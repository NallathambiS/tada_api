'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
var request = require('request')
const {ObjectId} = require('mongodb'); 

exports.avgetlanguage = function(req, res) {

var user_id = req.body.user_id;
var language_code=req.body.language_code;

let userlanguage = (callback0) => {
	let users_lang = []      
		if(!user_id){
	    var rescode = {
	      status : 0,
	      error  : "Missing mandatory elements user_id"
	    }
	    res.send(rescode);
	    return false;
	    } 
    var where ={
      "_id": ObjectId(user_id)
    }; 
	    let update_data={}
	    if(language_code!=null && language_code!=""){
	      if(!Array.isArray(language_code)){
	      	return res.send({"status":0,message:"language_code is an array and should consist of alteast one element"})
	      }
	      update_data['language_code']= language_code;
	      db.get().collection("aug_users").update(where, {$set: update_data},{upsert:false},function(err, docs){
	      assert.equal(err, null);
		  })
	    } 
    db.get().collection("aug_users").find(where).toArray(function(err, docs) {   
    assert.equal(err, null)          
	    if (docs[0]['language_code']!="") {
	    	users_lang=docs[0]['language_code']; 
	    }
	    else
	    { 
	    	callback0(null, docs); 
	    }
    let wherelang={
    	language_code:{$in:users_lang}
    }
    db.get().collection("av_language").find(wherelang).toArray(function(err, docs) {   
    assert.equal(err, null)         
	callback0(null, docs);  
	})
  })
}
 
let language = (callback1) => {
    db.get().collection("av_language").find().toArray((err, docs) => {
      assert.equal(err, null);
      callback1(null, docs);
    })
}
async.parallel({
    user_language: userlanguage,
    languages: language
  },
  (err, results) => { 

      if (err) {
          res.send({
            status: 0
          })
          return
      }

      res.send({
        status:1,
        user_language: results.user_language,
        languages: results.languages  
        
      })
    }
  )
}
