'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')
var request = require('request')
var Settings = require("../../lib/Settings");
const crypto = require("crypto");
var init = require("../../init");
const gcm = require("node-gcm");
var sender = new gcm.Sender(init.gcm_key);
const apn = require("apn");
var apnProvider = new apn.Provider(init.apnoptions);
var emoji = require('emoji-node')
var trimEmoji = require('trim-emoji')

exports.rocketchatnotification = function(req, res) {  
  try{
    var rm_id =  req.body.room_id;
    var from_chatuser_id =  req.body.from_user_id;
    var messagetype =  req.body.messagetype || "text";
    var user_message=req.body.user_message;
    var file_local_path=req.body.file_path;

    let user_names={}
    if(!from_chatuser_id || from_chatuser_id == "" || !rm_id || rm_id == ""){
      res.send({status:0,error:"Elements not found"});
      return true;
    }
 
     db.get().collection('aug_users').find({chat_userId:from_chatuser_id}).toArray(function(err, resulfromuser) {
      assert.equal(err, null);            
      if(resulfromuser.length>0){
      var from_aug_user_id=resulfromuser[0]['_id'];
      var friend_name = resulfromuser[0]['user_name'];
      var profile_picture = resulfromuser[0]['profile_picture'];
      var chat_authToken = resulfromuser[0]['chat_authToken'];
      var chat_userId = resulfromuser[0]['chat_userId'];
  
      request({ 
            url: "https://chat.augray.com/api/v1/rooms.info?roomId="+rm_id+"",
            method: "GET",
            headers: {
                "X-Auth-Token": chat_authToken,
                "X-User-Id": chat_userId,
                "content-type": "application/json"
            },
            json: true,
        }, function (error, response, body){   
            if(body && body.success ===true){ 
                 user_names = body.room && body.room.usernames;
                 // return true;
            }
            else{
            res.send({status:0,error:"Unable to get room users"});
          }
        
          
      let to_users={}
      to_users=user_names
      to_users = to_users.filter(e => e !== friend_name); 
   
      for (var i = 0; i < to_users.length ; i++) {
        
      db.get().collection('aug_users').find({user_name:to_users[i]}).toArray(function(err, resulttouser) { //to_users[i]
      assert.equal(err, null);
      if(resulttouser.length>0){
      let to_aug_user_id=resulttouser[0]['_id']


      if(resulttouser && resulttouser[0] && resulttouser[0]['push_notification'] == true){  //send notification only if end user enable notification
          db.get().collection('aug_users_token').find({user_id:ObjectId(to_aug_user_id)}).toArray(function(err, result2) {
          assert.equal(err, null);
          if(result2 && result2.length > 0 && result2[0]['token_key'] !=""){ 
            var token_key = result2[0]['token_key'];
            var platform = result2[0]['platform'];


            let emojisymbols="";
            let gettype="";
            let notif_message="";
            let page="";
            if(messagetype=="text"){
              gettype="sent_chat_message";
              notif_message="text sent";
              page="chat_message";

            }else if(messagetype=="video"){
              gettype="sent_chat_video";
              notif_message="video sent";
              page="chat_video";
              user_message="Video";  
              emojisymbols=trimEmoji(':🎥:') 
            }else if(messagetype=="image"){
              gettype="sent_chat_image";
              notif_message="image sent";
              page="chat_image";
              user_message="Photo";
              emojisymbols = emoji.get('camera');
            }
            else if(messagetype=="location"){
              gettype="sent_chat_location";
              notif_message="location sent";
              page="chat_location";
              user_message="Location";
              emojisymbols = trimEmoji(':📍:');
            }
            else if(messagetype=="contact"){
              gettype="sent_chat_contact";
              notif_message="contact sent";
              page="chat_contact";
              // user_message="Contact"; 
            }
            else if(messagetype=="chatanimation"){
              gettype="sent_chat_animation";
              notif_message="chatanimation sent";
              page="chat_animation";
              user_message="Animation";
              emojisymbols = trimEmoji(':💬:');
            } 
             else if(messagetype=="audio"){
              gettype="sent_chat_audio";
              notif_message="audio sent";
              page="chat_audio";
              user_message="Audio";
              emojisymbols = trimEmoji(':🔉:');
            }  
            db.get().collection('sn_notification_type').find({type_name:gettype}).toArray(function(err, result4) { 
              assert.equal(err, null);
              var screen_id = result4[0]['screen_id'];
              var screen_name = result4[0]['screen_name'];
              var msg_text = result4[0]['msg_text'] || "";
              msg_text = msg_text.replace("{name}",friend_name)
              
              var input_data = {
                notification_type_id : result4[0]['_id'],
                notification_type_name:gettype,
                notification_title:msg_text,
                notification_message:notif_message,
                thumb_image:profile_picture,
                seen:true,
                isnew:false,
                page:"chat_message",
                receiver_id:ObjectId(to_aug_user_id),
                sender_id:ObjectId(from_aug_user_id),
                last_update:new Date()
              }
              db.get().collection('sn_notifications').insert(input_data, function(err, result) { 
                assert.equal(err, null); 

                if(messagetype=='image' || messagetype=='video' || messagetype=="audio"){
                  var file_path= "https://chat.augray.com" + file_local_path + "?rc_uid=" + chat_userId + "&rc_token=" + chat_authToken
                  var payloadios = { 
                  type:gettype,
                  page:"chat_message",
                  sender_name : friend_name,
                  file_path : file_path  
                  }
                }
                else{
                  var payloadios = { 
                  type:gettype,
                  page:"chat_message",
                  sender_name : friend_name 
                  }
                }
                  
                console.log(platform);
                if(platform == "IOS"){
                  var note = new apn.Notification();
                  note.payload = payloadios; 
                  note.aps.badge = 1;
                  note.aps.sound = "default"; 
                  
                  if(messagetype=='image' || messagetype=='video' || messagetype=="audio"){
                    note['aps']['mutable-content'] = 1;
                    note.aps.category = "ROCKET_CHAT_IMAGE_VIDEO";
                  }else{
                    note.aps.category = "ROCKET_CHAT_MESSAGE";
                  }
                  note.aps.alert = {title: friend_name,
                                    body: emojisymbols + ' ' + user_message};
                  note.topic = "com.augray.avmessage"; 
                  apnProvider.send(note, token_key).then( (result) => {
                    console.log(result);
                    // res.send({status:1,message:"rocketchat notification sent"}); //,ios_result:result
                    // return true;
                  });
                }else{
                  var regTokens = [token_key];

                      var datamsg = {} 
                      datamsg['body'] = emojisymbols + ' ' + user_message;//username
                      datamsg['type']=gettype;
                      datamsg['page']="chat_message"; 
                      datamsg['sender_id']=from_aug_user_id;
                      datamsg['sender_name']=friend_name; 
                      datamsg['image_url']=profile_picture;
                      datamsg['room_id']=rm_id; 
                      datamsg['title']=friend_name; 
                      datamsg['user_message']=user_message; 

                      if(messagetype=="image" || messagetype=="video" || messagetype=="audio"){
                        var file_path= "https://chat.augray.com" + file_local_path + "?rc_uid=" + chat_userId + "&rc_token=" + chat_authToken
                        datamsg['file_path']=file_path; 
                      }
                    
                      
                  var message = new gcm.Message({
                        data: datamsg 
                    }); 

                    sender.send(message, { registrationTokens: regTokens }, function (err, response) { 
                        if (err) console.error(err);
                        else console.log(response);
                         // res.send({status:1,message:"rocketchat notification sent"});//,android_result:response
                        // return true;
                    });
                }
              })
            })
        }  
        })
        }//push_notification
        else
        {
          res.send({status:0,message:"notification not allowed by user"})
        }
        }  
        else{
          res.send({status:0,message:"to user not found"})
        } 
  })//resulttouser
 }//for
res.send({status:1,message:"rocketchat notification sent"});
return true;
}) //request
}else{
  res.send({status:0,message:"from user not found"})
}
})//resulfromuser
}
catch(errrr){
  res.send({sdds:errrr})
}
}
 
 


