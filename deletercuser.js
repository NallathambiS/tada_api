'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')
var request = require('request')

exports.deletercuser = function(req, res) {

  var user_id = req.body.user_id;

  if(user_id === undefined || user_id === ""){
    res.send({status:0,error: "Missing mandatory elements"});
    return true;
  }

      let user= (callback0)=> {
        db.get().collection("aug_users").find({_id : ObjectId(user_id)}).limit(1).skip(0).toArray((err, docs)=>{
          assert.equal(err, null);
          callback0(null, docs);
        })
      }

      async.parallel({
        data: user
      },

      (err, results) =>{
        if (err) {
          res.send({
            status: 0
          })
          return
         }
        if(results.length !== 0){
          var data = {
            userdata: results['data'][0],
          };

          if(data){
            var myJSONObject = {
              "username": data['userdata']['user_name']
            };

            request({
                url: "https://chat.augray.com/api/v1/users.delete",
                method: "POST",
                headers: {
                     "Content-Type":"application/json",
                      "X-Auth-Token":"_XBAlXZmzr_HzN25NTEa5O6443npSvJBIROCvHU3vBB",  //admin user for permission(tadateam)
                      "X-User-Id":"tFcM3aCuBo6dc5tWQ",
                },
                json: true,
                body: myJSONObject,
            },  (error, response, body)=>{
              console.log(body)
                if(body.success === true){
                  res.send({status:1,message:body.success})
                }else{
                  res.send({status:0})
                }
            });
          }else{
            res.send({status:0})
          }
        }
      })
}
