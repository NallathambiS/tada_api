'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var AWS = require('aws-sdk')
var path = require('path')  
var url = require("url")
var db = require("../../db")
var Settings = require("../../lib/Settings");
var request = require('request')

exports.deleteuser = function(req, res) {
    var user_id =req.body.user_id;
 
    if(user_id == undefined || user_id == ""){
      res.send({status:0,error:"missing mandatory element user_id"})
      return true
    }
    async.parallel([
      function(callback0) {
        db.get().collection('av_avatar').remove({user_id: ObjectId(user_id)}, function(err, docs) {
        assert.equal(err, null)
        if(docs['result']['ok']> 0){
        callback0(null, docs)
        }else{
        callback0(null, [])
        }
        })
      },
      function(callback1) {
         db.get().collection('av_video').find({video_user_id:ObjectId(user_id)}).toArray(function(err, awspathresult) {
        assert.equal(err, null);
        var videocount=awspathresult.length;
        if(awspathresult.length!=0){

        for(var i=0;i<awspathresult.length;i++){
          let video_id = awspathresult[i]['_id']
          let video_url = awspathresult[i]['video_path']
          let thumb_url = awspathresult[i]['thumb_image'] 
   
          db.get().collection('av_video').remove({_id:ObjectId(video_id),video_user_id:ObjectId(user_id)},  function(err, result) {  
          assert.equal(err, null);
          if(result['result']['ok']> 0){

            var delimg = path.basename(thumb_url);
            var deletevid = path.basename(video_url);
             
            var s3 = new AWS.S3({
              accessKeyId: Settings.s3.key,
              secretAccessKey: Settings.s3.secret
            });
            if(delimg != ""){
              var paramsimg = {
              Bucket: Settings.s3.bucketnew,
              Key: Settings.s3['path-avatar']+delimg
            }; 
            s3.headObject(paramsimg, function (err, metadata) {
                  if (!err || err.code != 'NotFound') {
                    s3.deleteObject(paramsimg, function(err, data) {
                      if (err) console.log(err, err.stack); // an error occurred
                      else     console.log(data);           // successful response
                    });
                  }
                });
          }
          if(deletevid != ""){
               var paramsvid = {
              Bucket: Settings.s3.bucketnew,
              Value: Settings.s3['path-post']+deletevid
            }; 
             
            s3.headObject(paramsvid, function (err, metadata) {
              if (!err || err.code != 'NotFound') {
                s3.deleteObject(paramsvid, function(err, data) {
                  if (err) console.log(err, err.stack); // an error occurred
                  else     console.log(data);           // successful response
                  return true;
                });
              }
            });
          }

          else{
            callback1(null, {status:0,error: "Failed to delete video"})
          } 
          
        }  
        else{       
          callback1(null, {status:0,error: "Failed to find video"})
        } 
        callback1(null, result)
         })     
      }
      }else{
        callback1(null, [])
      }
      })
      },
      function(callback2) {
         db.get().collection("av_video").update({},{ $pull: {favourite: ObjectId(user_id)}},function(err, docs) { 
         assert.equal(err, null)
         if(docs['result']['ok']> 0){ 
          callback2(null, docs)
          }else{
          callback2(null, [])
          }
         })
      },
      function(callback3) {
        let where = {
          "$or" : [{sender_id:ObjectId(user_id)},{receiver_id:ObjectId(user_id)}]
      }
         db.get().collection("sn_notifications").deleteMany(where,function(err, docs) {
         assert.equal(err, null)
         if(docs['result']['ok']> 0){
          callback3(null, docs)
          }else{
          callback3(null, [])
          }
         })
      },
      function(callback4) {
        let where = {
          "$or" : [{blocker:ObjectId(user_id)},{blocked:ObjectId(user_id)}]
      }
         db.get().collection("user_blocklist").deleteMany(where,function(err, docs) {
         assert.equal(err, null)
         if(docs['result']['ok']> 0){
          callback4(null, docs)
          }else{
          callback4(null, [])
          }
         })
      },
      function(callback5) {
        let where = {
          "$or" : [{following:ObjectId(user_id)},{follower:ObjectId(user_id)}]
      }
         db.get().collection("user_friends").deleteMany(where,function(err, docs) {
           assert.equal(err, null)
           if(docs['result']['ok']> 0){
            callback5(null, docs)
            }else{
            callback5(null, [])
            }
         })
      },
      function(callback6){
        db.get().collection("aug_users").find({_id : ObjectId(user_id)}).toArray(function(err, docs){ 
          assert.equal(err, null);
          if(docs.length != 0){ 
          var data = {
            user_name: docs[0]['user_name']
          };

            var myJSONObject = {
              "username": data['user_name']
            };
            request({
                url: "https://chat.augray.com/api/v1/users.delete",
                method: "POST",
                headers: {
                     "Content-Type":"application/json",
                      "X-Auth-Token":"_XBAlXZmzr_HzN25NTEa5O6443npSvJBIROCvHU3vBB",//docs[0]['chat_authToken'],  //admin user for permission(tadateam)
                      "X-User-Id":"tFcM3aCuBo6dc5tWQ",//docs[0]['chat_userId'],
                },
                json: true,
                body: myJSONObject,
            },  (error, response, body)=>{
                if(body.success === true){
                  callback6(null, {status:1,"message":"rocketchat user delete success"})
                  }else{
                  callback6(null, {status:0,"message":"rocketchat user not found"})
                  }
                });
        }
        else{
          res.send({status:0,"error":"tada user not found"})
        }
      })
      },
      function(callback7) {
         db.get().collection("aug_users_token").deleteMany({user_id: ObjectId(user_id)},function(err, docs) {
           assert.equal(err, null)
           if(docs['result']['ok']> 0){
            callback7(null, docs)
            }else{
            callback7(null, [])
            }
         })
      },
      function(callback8) {
         db.get().collection("aug_users_track").deleteMany({user_id: ObjectId(user_id)},function(err, docs) {
           assert.equal(err, null)
           if(docs['result']['ok']> 0){
            callback8(null, docs)
            }else{
            callback8(null, [])
            }
         })
      },
      function(callback9) {
        db.get().collection('aug_users').remove({_id: ObjectId(user_id)}, function(err, docs) {
          assert.equal(err, null)
          if(docs['deletedCount']> 0){
            callback9(null, docs)
            }else{
            callback9(null, {status:0,"error":"user not found"})
            }
        })
      }
      ],
      function(err, results) {
        if(results[9]['deletedCount']> 0){
          var result_data = {
            status : 1,
            message : "logout success"
          }
        }else{
          var result_data = {
            status : 0,
            error : "logout failed"
          }
        }
        res.send(result_data)
    })

};