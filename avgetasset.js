'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")

exports.avgetasset = function(req, res) {

  var assetID = req.body.asset_type_id;
  var gender = req.body.gender;
  let search = req.body.search || "";

  if(assetID == "" || gender == "" || assetID == undefined || gender == undefined){
    res.send({status:0,error:"missing input fields"});
    return true;
  }

  var pagination_limit = req.body.pagination_limit || 50;
  var pagination_index = req.body.pagination_index || 1;

  var skipParams = parseInt(pagination_limit * (pagination_index - 1));
  var limitParams = parseInt(pagination_limit);

    async.waterfall([
      function(callback) {
        let where={}
        where['asset_type_id']= ObjectId(assetID)
        where['gender']=gender
        where['asset_enable']=true
        if (search!="") {
            where['asset_name'] = { $regex: search, $options: 'i' } ;            
        }
        db.get().collection("av_asset").find(where).count({},function(err, count){
          assert.equal(err, null);
          callback(null, {
            totalCount: count
          });
        });
      },
      function(data, callback) {
        let where={}
        where['asset_type_id']= ObjectId(assetID)
        where['gender']=gender
        where['asset_enable']=true
        if (search!="") {
            where['asset_name'] = { $regex: search, $options: 'i' } ;            
        }
            db.get().collection("av_asset").find(where).limit(limitParams).skip(skipParams).toArray(function(err, docs) {
            assert.equal(err, null);
            callback(null, {
              status: 1,
              totalPages: Math.ceil(data.totalCount / limitParams),
              currentPageIndex: pagination_index,
              totalCount: data.totalCount,
              data: docs
            });
          });
      },
      ], function (err, result) {
        res.send(result)
      });


}
