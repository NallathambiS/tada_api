'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")

exports.avgetchatanimationdata = function(req, res) {

  var chatanimID = req.body.chat_animation_id;
  // var gender = req.body.gender;

  if(chatanimID == "" || chatanimID == undefined){
    res.send({status:0,error:"missing input fields"});
    return true;
  }
       

        let where = {
              "_id": ObjectId(chatanimID) 
             }
        db.get().collection("chat_animation").aggregate([
          { "$match" : where},
          { "$lookup": {
            "from": "av_asset",
            "localField": "s_plugin_asset_id",
            "foreignField": "_id",
            "as": "s_plugindata"
          }},
          { "$lookup": {
            "from": "av_asset",
            "localField": "r_plugin_asset_id",
            "foreignField": "_id",
            "as": "r_plugindata"
          }}, 
          { "$unwind": {"path": "$s_plugindata", "preserveNullAndEmptyArrays": true}}, 
          { "$unwind": {"path": "$r_plugindata", "preserveNullAndEmptyArrays": true}}, 
          { "$project": {
            _id:  1,
            "chat_animation_type_id"     : 1,
            "chat_mood_type_id"   : 1,
            "animation_name"   : 1,
            "file_path_ios"   : 1,
            "file_path_android"   : 1,
            "thumb_image"   : 1,
            "send_metadata"   : 1,
            "rec_metadata"   : 1,
            "animation_enable"   : 1,
            "s_plugin_asset_id": { "$cond": [{ "$eq": [ "$s_plugin_asset_id", ObjectId("000000000000000000000001") ] },"$$REMOVE", "$s_plugin_asset_id" ] },
            "r_plugin_asset_id": { "$cond": [{ "$eq": [ "$r_plugin_asset_id", ObjectId("000000000000000000000001") ] },"$$REMOVE", "$r_plugin_asset_id" ] },
            "s_plugin_asset"   : { "$cond": [{ "$eq": [ "$s_plugindata", null ] },"$$REMOVE", "$s_plugindata" ] },
            "r_plugin_asset"   : { "$cond": [{ "$eq": [ "$r_plugindata", null ] },"$$REMOVE", "$r_plugindata" ] },
            "country_code"   : 1,
            "point"   : 1,
            "last_update"   : { $dateToString: { format: "%d/%m/%Y %H:%M:%S", date: "$last_update" } }
          }}
        ]).toArray(function(err, docs) {
        assert.equal(err, null);
        if(docs.length!=0){
        var resultdata={
          status: 1,
          data: docs[0]
        };
        res.send(resultdata)
      }
      else {
        res.send({status:0,data:[]})
      }
    });
      }
