'use strict'; 
var _ = require('lodash');
var async = require('async'); 
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')


exports.readnotificationall = function(req, res) { 
    var user_id =  req.body.user_id; 

    if(user_id === "" || user_id == undefined){
      res.send({status:0,error:"missing mandatory element deviceid"});
      return true;
    }
 
      var input_data = {
        seen : true,
        isnew: false
      }
      var where = {
         receiver_id : ObjectId(user_id) 
      } 
 
        
     
    db.get().collection('sn_notifications').updateMany(where, { $set: input_data }, function(err, result) { 
      assert.equal(err, null); 

      if(result.result['nModified']!=0){
        res.send({status:1})
      }else{
        res.send({status:0, error: "Update failed"})
      }

    }) 
};
