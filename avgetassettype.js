'use strict';
var async = require('async')
const assert = require('assert')
var db = require("../../db")

exports.avgetassettype = function(req, res) {

    async.parallel({
      type:function(callback0) {
        db.get().collection("av_asset_type").find({}).toArray(function(err, docs) {
          assert.equal(err, null);
          callback0(null,docs);
        });
      },
      color:function(callback1) {
        db.get().collection("av_asset_color").find({}).toArray(function(err, docs) {
          assert.equal(err, null);
          callback1(null,docs);
        });
      },
    }, function (err, results) {

      res.send({
        status: 1,
        asset_type: results.type,
        asset_color: {
          charcolorlist: results.color[0].charcolorlist.split(','),
          assetcolorlist: results.color[0].assetcolorlist.split(','),
          haircolorlist: results.color[0].haircolorlist.split(',')
        }
      });

      })
}
