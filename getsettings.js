'use strict';
var init = require("../../init");
var Settings = require("../../lib/Settings");
var _ = require('lodash');
var async = require('async');
var mongo_lib = require('mongodb');
const MongoClient = mongo_lib.MongoClient;
const assert = require('assert');
const mongo_url = init.mongo_url;
var ObjectId = mongo_lib.ObjectID;
var Const = require("../../const");


exports.getsettings = function(req, res) {
  var $user_id = req.body.user_id;

  if(!$user_id){
    var rescode = {
      status : 0,
      error  : "Missing mandatory elements user_id"
    }
    res.send(rescode);
    return false;
  }



    var where ={
      "_id": ObjectId($user_id)
    };
    db.get().collection("aug_users").aggregate([
      { "$match" : where},
      { "$lookup": {
        "from": "av_language",
        "localField": "language_code",
        "foreignField": "language_code",
        "as": "langObjects"
      }},
      { "$unwind": "$langObjects"},
      { "$group": {
        _id: "$_id".valueOf(),
        "user_id"   : { "$first" : "$_id".valueOf()  },
        "search_distance"   : { "$first" : "$search_distance"  },
        "role_id"   : { "$first" : "$role_id"  },
        "user_name"   : { "$first" : "$user_name"  },
        "user_email"   : { "$first" : "$user_email"  },
        "user_phone"   : { "$first" : "$user_phone"  },
        "is_social_login"   : { "$first" : "$is_social_login"  },
        "phone_code"   : { "$first" : "$phone_code"  },
        "user_gender"   : { "$first" : "$user_gender"  },
        "user_dob"   : { "$first" : "$user_dob"  },
        "profile_picture"   : { "$first" : "$profile_picture"  },        
        "language_id"   : { "$first" : "$langObjects._id"  },
        "language_code"   : { "$first" : "$langObjects.language_code"  },
        "language_name"   : { "$first" : "$langObjects.language_name"  }
      }}
    ]).toArray(function(err, docs) {
      assert.equal(err, null); 
      if(docs.length > 0){
        docs[0]['status'] = "1"
      }
      res.send((docs.length > 0) ? docs[0] : { "status":0,error:"User not found" });
    });

};
