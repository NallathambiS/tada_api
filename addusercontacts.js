'use strict';
var _ = require('lodash')
var async = require('async')
var mongo = require('mongodb')
const assert = require('assert')
var ObjectId = mongo.ObjectID
var vCard = require( 'vcf' )
var vcardparser = require( 'vcardparser' )
var request = require('request')
var db = require("../../db")

exports.addusercontacts = function(req, res) {
  var vcf_path = req.body.vcf_path ;
  var country_code = req.body.country_code ;
  var user_id = req.body.user_id ;
  if(!vcf_path || vcf_path == ""){
    res.send({status:0,message:"missing mandatory element vcf_path"});
    return true;
  }

  if(!country_code || country_code == ""){
    res.send({status:0,message:"missing mandatory element country_code"});
    return true;
  }

  if(!user_id || user_id == ""){
    res.send({status:0,message:"missing mandatory element user_id"});
    return true;
  }
  var own_phone=""
  db.get().collection("aug_users").find({_id : ObjectId(user_id)}).toArray(function(err,userresult){ //res.send({ddgfG:phone_number})
  if(userresult.length>0){
     own_phone=userresult[0]['user_phone']
  

  var contact_data = [];
  request.get(vcf_path, function (error, response, body) {
      if (!error && response.statusCode == 200) {
          var data = body;
          var cards = vCard.parse( data );
              
          if(cards.length <= 0){ 
            res.send({status:0,message:"no contacts available"});
            return true;
          }else{ 
            for (var i = 0; i < cards.length; i++) {
              cards[i] = cards[i].toJSON();



              if(cards[i][1] && cards[i][1][2] && cards[i][1][2][0] == "fn"){ 
                var FirstName = cards[i][1][2][3];
              }else{
                if(cards[i][1] && cards[i][1][3] && cards[i][1][3][0] == "fn"){ 
                  var FirstName = cards[i][1][3][3];
                }
                else if(cards[i][1] && cards[i][1][1] && cards[i][1][1][0] == "fn"){
                  var FirstName = cards[i][1][1][3];
                }

                else{
                var FirstName = "";
                }
              }
              var phone_number=""
              if(cards[i][1] && cards[i][1][4] && cards[i][1][4][0] == "tel"){
                 phone_number = cards[i][1][4][3];
              }
              else if(cards[i][1] && cards[i][1][5] && cards[i][1][5][0] == "tel"){
                 phone_number = cards[i][1][5][3];
              }
              else if(cards[i][1] && cards[i][1][6] && cards[i][1][6][0] == "tel"){
                   phone_number = cards[i][1][6][3];
                }
              else if(cards[i][1] && cards[i][1][3] && cards[i][1][3][0] == "tel"){ //res.send("asa")
                   phone_number = cards[i][1][3][3];
                }

                else{
                 phone_number = "";
                }
      
              phone_number = phone_number.replace(/[^0-9]/g, "");

              if(phone_number.length > 10 && phone_number.length <=12){
                phone_number = phone_number.substring(2);
              }
              if(phone_number.indexOf("1800") != -1 || phone_number.length <= 6 || phone_number == "" || phone_number.length > 12){
                continue;
              }
              
              var temp={}
              if(phone_number != own_phone){
               temp = {
                contact_name:FirstName,
                phone_no:phone_number,
                country_code:country_code,
                user_id:ObjectId(user_id)
              }
              

              if(!JSON.stringify(contact_data).includes(JSON.stringify(temp))){
              contact_data.push(temp);
              }
            }
            }
              if(user_id){
                var where = {
                  user_id : ObjectId(user_id)
                }
              }else{
                var where = {}
              }
              db.get().collection("user_contacts").find(where).toArray(function(err,results){
                if(results && results.length > 0){
                  db.get().collection("user_contacts").remove({user_id:ObjectId(user_id)},function(err,result){
                    db.get().collection("user_contacts").insertMany(contact_data,function(err,insert_result){
                      // res.send({status:1,message:"successfully added"});
                   // return showdata();
                   async.parallel([
                          function(callback0) {
                          let where = {
                            user_id : ObjectId(user_id)
                          }
                          db.get().collection("user_contacts").aggregate([
                            { "$match" : where},
                            { "$lookup": {
                              "from": "aug_users",
                              "localField": "phone_no",
                              "foreignField": "user_phone",
                              "as": "userObjects"
                            }},
                            { "$unwind": "$userObjects"},
                            { "$group": {
                              _id:  "$_id".valueOf(),
                              "user_id"   : { "$first" : "$userObjects._id"  },
                              "user_phone"   : { "$first" : "$userObjects.user_phone"  },
                              "user_name"   : { "$first" : "$userObjects.user_name"  },
                              "profile_picture"   : { "$first" : "$userObjects.profile_picture"  }
                            }}
                          ]).toArray(function(err, docs) {
                            assert.equal(err, null);
                            let temparr=[]
                            for (var i = 0; i < docs.length; i++) { 
                              if(docs[i].user_id!=user_id){ 
                                temparr.push(docs[i])
                              }             
                            }  
                            callback0(null,temparr);
                          });
                        },
                        function(callback1) {
                          let where = {
                            _id : ObjectId(user_id)
                          }
                          db.get().collection('aug_users').distinct("user_phone",{user_phone:{ $ne: "" },_id:{$ne:ObjectId(user_id)}},function(err, vals) {
                            db.get().collection('user_contacts').find({user_id : ObjectId(user_id),phone_no: {$nin: vals}}).toArray(function(err, docs) {
                              assert.equal(err, null);
                              let temp = [];
                              for (var i = 0; i < docs.length; i++) {
                                temp.push({contact_id:docs[i]['_id'],user_phone:docs[i]['phone_no'],user_name:docs[i]['contact_name']})
                              }
                              callback1(null,temp);
                            });
                          });
                        }],
                      function(err, results) {
                        var result_data = {
                          status:1,
                          response:{
                            tada_network:results[0],
                            nontada_friends:results[1]
                          }
                        }

                      res.send(result_data);
                    });
                    });
                  });
                }else{
                  db.get().collection("user_contacts").insertMany(contact_data,function(err,insert_result){
                    console.log(insert_result);
                    // res.send({status:1,message:"successfully added"});
                // return showdata();
                async.parallel([
          function(callback0) {
            let where = {
              user_id : ObjectId(user_id)
            }
            db.get().collection("user_contacts").aggregate([
              { "$match" : where},
              { "$lookup": {
                "from": "aug_users",
                "localField": "phone_no",
                "foreignField": "user_phone",
                "as": "userObjects"
              }},
              { "$unwind": "$userObjects"},
              { "$group": {
                _id:  "$_id".valueOf(),
                "user_id"   : { "$first" : "$userObjects._id"  },
                "user_phone"   : { "$first" : "$userObjects.user_phone"  },
                "user_name"   : { "$first" : "$userObjects.user_name"  },
                "profile_picture"   : { "$first" : "$userObjects.profile_picture"  }
              }}
            ]).toArray(function(err, docs) {
              assert.equal(err, null);
              callback0(null,docs);
            });
          },
          function(callback1) {
            let where = {
              _id : ObjectId(user_id)
            }
            db.get().collection('aug_users').distinct("user_phone",{user_phone:{ $ne: "" }},function(err, vals) {
              db.get().collection('user_contacts').find({user_id : ObjectId(user_id),phone_no: {$nin: vals}}).toArray(function(err, docs) {
                assert.equal(err, null);
                let temp = [];
                for (var i = 0; i < docs.length; i++) {
                  temp.push({contact_id:docs[i]['_id'],user_phone:docs[i]['phone_no'],user_name:docs[i]['contact_name']})
                }
                callback1(null,temp);
              });
            });
          }],
        function(err, results) {
          var result_data = {
            status:1,
            response:{
              tada_network:results[0],
              nontada_friends:results[1]
            }
          }
          res.send(result_data);
        });
                  });
                }


              })
          }
          return true;
      }
  });
return true;
}
})
};
