'use strict';
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')

exports.setpublicvideo = function(req, res) {
  var video_id =  req.body.video_id
  var ispublic=req.body.is_public
    let where = {_id:ObjectId(video_id)}
    db.get().collection("av_video").update(where,{$set:{is_public:ispublic}},{upsert:false},function(err, docs) {  
          assert.equal(err, null);
          if (docs.result['nModified']!=0) {
            res.send({status:1,is_public:ispublic})
          }
          else {
            res.send({status:0,message:"is_public not changed"});
          }
    })
}
