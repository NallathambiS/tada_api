'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const { ObjectId } = require('mongodb')

exports.getuserfollowlist = function(req, res) {
  let from_user_id = req.body.from_user_id;
  let to_user_id = req.body.to_user_id;
  let type = req.body.type;
  let searchelem = req.body.search || "";

  let where;
  let where_status;
  let where_followinglist;
  let totalFollowCount;

  let pagination_limit = req.body.pagination_limit || 50;
  let pagination_index = req.body.pagination_index || 1;

  let skipParams = parseInt(pagination_limit * (pagination_index - 1));
  let limitParams = parseInt(pagination_limit);

  if(!to_user_id || to_user_id =="" || !from_user_id || from_user_id =="" || !type || type ==""){
    return res.send({
      status : 0,
      message : "Missing mandatory elements from/to_user_id"
    });
  }
  function skip(c){
      return this.filter((x,i) => {
        if(i>(c-1)){return true}
      })
  }
  function limits(c){
      return this.filter((x,i)=>{
        if(i<=(c-1)){return true}
      })
  }

    async.parallel([
      function(callback0) {
        if(type === "following"){
          where = {
        follower : ObjectId(to_user_id),
        status:1
      }
       where_followinglist = {
       /* following : ObjectId(to_user_id),
        status:1*/
      }
      where_status = {
        follower : ObjectId(from_user_id),
      }

         db.get().collection("user_friends").find(where_status).toArray(function(err, docs) {
        assert.equal(err, null);
        callback0(null, docs);
      });

       }else if(type === "follower"){
         where = {
           following : ObjectId(to_user_id),
         }
         where_followinglist = {
           follower : ObjectId(to_user_id),
           status:1
         }
         where_status = {
           following : ObjectId(from_user_id),
         }

         db.get().collection("user_friends").find(where_status).toArray(function(err, docs) {
           assert.equal(err, null);
           callback0(null, docs);
         });

       }
       else {
         return res.send({
           status : 0,
           error  : "invalid type"
         });
       }
      },
      function(callback1) {

          db.get().collection("user_friends").aggregate([
      { "$match" : where},
      // { "$skip": skipParams },
      // { "$limit": limitParams },
      { "$lookup": {
       "from": "aug_users",
       "localField": type,
       "foreignField": "_id",
       "as": "follow_list"
      }},

      { "$unwind": "$follow_list"},
      { "$project": {
           "_id": 0,
           "follow_list._id": 1,
           "follow_list.user_name": 1,
           "follow_list.user_gender": 1,
           "follow_list.user_dob": 1,
           "follow_list.profile_picture": 1,
           "follow_list.thumb_image": {$arrayElemAt: ['$follow_list.thumb_image.file_path', 0] },
           "follow_list.status": "$status"
         }
       }
     ]).toArray(function(err, docs) {
         assert.equal(err, null);
         var arrayFollowers = [];
         if(docs.length !== 0){
           docs.map(a=> {
             arrayFollowers.push(a['follow_list']);
           })
          callback1(null, arrayFollowers);
         }else{
          callback1(null, docs);
         }
     })
      },
      function(callback2) {

          db.get().collection("user_friends").aggregate([
      { "$match" : where_followinglist},
       // { "$skip": skipParams },
       // { "$limit": limitParams },
      { "$lookup": {
       "from": "aug_users",
       "localField": type==="follower"? "following" : "follower",
       "foreignField": "_id",
       "as": "follow_list"
      }},

      { "$unwind": "$follow_list"},
      { "$project": {
           "_id": 0,
           "follow_list._id": 1,
           "follow_list.user_name": 1,
           "follow_list.user_gender": 1,
           "follow_list.user_dob": 1,
           "follow_list.profile_picture": 1,
           "follow_list.thumb_image": {$arrayElemAt: ['$follow_list.thumb_image.file_path', 0] },
           "follow_list.status": "$status"
         }
       }
     ]).toArray(function(err, docs) {
         assert.equal(err, null);
         var arrayFollowers = [];
         if(docs.length !== 0){
           docs.map(a=> {
             arrayFollowers.push(a['follow_list']);
           })

          callback2(null, arrayFollowers);
         }else{
           callback2(null, docs);
         }
     })


      },
      ],
      function(err, results) {
            // res.send(results)
          var temparr=[];
        if(type==="follower"){



          var arrftemp1=[];
          var arrftemp2=[];

          for(var c=0;c<results[1].length;c++)
            {
                  arrftemp1.push({
                      "_id":results[1][c]._id,
                      "user_name":results[1][c].user_name,
                      "user_gender":results[1][c].user_gender,
                      "user_dob":results[1][c].user_dob,
                      "profile_picture":results[1][c].profile_picture,
                      "thumb_image":results[1][c].thumb_image
                    });
            }
            for(var d=0;d<results[2].length;d++)
            {
                  arrftemp2.push({
                      "_id":results[2][d]._id,
                      "user_name":results[2][d].user_name,
                      "user_gender":results[2][d].user_gender,
                      "user_dob":results[2][d].user_dob,
                      "profile_picture":results[2][d].profile_picture,
                      "thumb_image":results[2][d].thumb_image
                    });
            }




       for(var i=0;i<arrftemp2.length;i++)
            {
               if(!JSON.stringify(arrftemp1).includes(JSON.stringify(arrftemp2[i]))){
                  temparr.push({
                      "_id":arrftemp2[i]._id,
                      "user_name":arrftemp2[i].user_name,
                      "user_gender":arrftemp2[i].user_gender,
                      "user_dob":arrftemp2[i].user_dob,
                      "profile_picture":arrftemp2[i].profile_picture,
                      "thumb_image":arrftemp2[i].thumb_image,
                      "status" :  2 //getfollowers(results[2][i]._id) //(JSON.stringify(result[1][i]['_id'])!=JSON.stringify(result[0][j]['_id'])) ? (JSON.stringify(result[0][j]['status'])) */
                    });
                  }
                }

        for(var j = 0 ;j<results[1].length; j++) {
            temparr.push({
              "_id":results[1][j]._id,
              "user_name":results[1][j].user_name,
              "user_gender":results[1][j].user_gender,
              "user_dob":results[1][j].user_dob,
              "profile_picture":results[1][j].profile_picture,
              "thumb_image":results[1][j].thumb_image,
              "status": getfollowers(results[1][j]._id)
            });
        }
      }
         if(type==="following"){
         for(var k = 0 ;k<results[1].length; k++) {
            temparr.push({
              "_id":results[1][k]._id,
              "user_name":results[1][k].user_name,
              "user_gender":results[1][k].user_gender,
              "user_dob":results[1][k].user_dob,
              "profile_picture":results[1][k].profile_picture,
              "thumb_image":results[1][k].thumb_image,
              "status": getfollowers(results[1][k]._id)
            });
        }
      }



      var result_data={};
      if(searchelem != ""){
          for (var i = temparr.length - 1; i >= 0; i--) {
            if(temparr[i].user_name=searchelem){
             res.send({
                     status: 1,
                     totalPages: Math.ceil(temparr.length / limitParams),
                     currentPageIndex: pagination_index,
                     totalCount: temparr.length,
                     data: [temparr[i]]
                     })
            }
          }
        }
//res.send("FSss")
        Array.prototype.skip=skip;
        Array.prototype.limits=limits;
        res.send({
          status: 1,
          totalPages: Math.ceil(temparr.length / limitParams),
          currentPageIndex: pagination_index,
          totalCount: temparr.length,
          data: temparr.skip(skipParams).limits(limitParams)//.slice(skipParams,limitParams)
        });
        function getfollowers(_id){

          var def_status=2;
          /*if(JSON.stringify(_id)===JSON.stringify(from_user_id)){    //if dont want to show button for visiting user
            return def_status=3;
          }*/
          if(type==="follower"){
          for(var j = 0 ;j<results[0].length; j++) {
          if(JSON.stringify(_id)===(JSON.stringify(results[0][j].follower))){
            return def_status=results[0][j].status===1 || 0 ? results[0][j].status: 2 || results[0][j].status===0 ? 0: 2;
          }
          if(JSON.stringify(_id)===JSON.stringify(from_user_id)){    //if dont want to show button for visiting user
            return def_status=3;
          }
      }
      }
      if (type==="following") {
        for(var j = 0 ;j<results[0].length; j++) {
          if(JSON.stringify(_id)===(JSON.stringify(results[0][j].following))){
            return def_status=results[0][j].status===1 ? results[0][j].status: 2 || results[0][j].status===0 ? 0: 2;
          }
          if(JSON.stringify(_id)===JSON.stringify(from_user_id)){    //if dont want to show button for visiting user
            return def_status=3;
          }
      }
      }
      return def_status;
    }

       //res.send(result_data);

      })

};
