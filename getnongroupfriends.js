'use strict';
var _ = require('lodash')
var async = require('async')
var mongo_lib = require('mongodb')
const assert = require('assert')
var ObjectId = mongo_lib.ObjectID
var db = require("../../db")

exports.getnongroupfriends = function(req, res) {
    var user_id = req.body.user_id;
    var room_id=req.body.room_id;
    var search = req.body.search || "";

    var pagination_limit = req.body.pagination_limit || 50;
    var pagination_index = req.body.pagination_index || 1;
    var skipParams = parseInt(pagination_limit * (pagination_index - 1));
    var limitParams = parseInt(pagination_limit);

    if(!user_id || user_id =="" || !room_id || room_id ==""){
      var rescode = {
        status : 0,
        error  : "Missing mandatory elements user_id or room_id"
      }
      res.send(rescode);
      return false;
    }

    let where ={
      blocker : ObjectId(user_id)
    }

    db.get().collection("user_blocklist").find(where).toArray(function(err, docs) {
      assert.equal(err, null);
      var blocked_users = [];
      if(docs.length>0){
      for (var i = 0; i < docs.length; i++) {
        blocked_users.push(docs[i]['blocked'].toString());
      }
      }
       var where = {
          "$or" : [{following:ObjectId(user_id)},{follower:ObjectId(user_id)}],
          status:1
      }

      var user_ids = [];
      db.get().collection("user_friends").find(where).toArray(function(err, docs2) {
        assert.equal(err, null);
        if(docs2 && docs2.length > 0){
          for (var i = 0; i < docs2.length; i++) {
            if(docs2[i]['following'] == user_id){
              user_ids.push(docs2[i]['follower'].toString());
            }else{
              user_ids.push(docs2[i]['following'].toString());
            }
          }
          var new_user_ids =  user_ids.filter(function(x) {
            return blocked_users.indexOf(x) < 0;
          });

          var new_user_objectids = [];
          for (var i = 0; i < new_user_ids.length; i++) {
            new_user_objectids.push({"_id":ObjectId(new_user_ids[i])});
          }

              let where_chat = {
              user_active:true,
              $or:new_user_objectids
            }

              db.get().collection("aug_users").find(where_chat).toArray(function(err, docs) {
                assert.equal(err, null);
                var chat_friends = [];
                if(docs.length>0){
                for (var i = 0; i < docs.length; i++) {
                  if(docs[i]['chat_userId']!=""){
                    chat_friends.push(ObjectId(docs[i]['_id'].toString()));
                  }
                }
              }

                let where_room = {
                _id:room_id
              }
                db.getChat().collection("rocketchat_room").find(where_room).toArray(function(err, group_results) {
                assert.equal(err, null);
                var group_users = [];
                if(group_results.length>0){
                for (var i = 0; i < group_results[0].usernames.length; i++) {
                  if(group_results[0].usernames[i]!=""){
                    group_users.push(group_results[0].usernames[i].toString());
                  }
                }
              }

                let where_group_users = {
                user_name:{$in:group_users},
              }


                db.get().collection("aug_users").find(where_group_users).toArray(function(err, group_ids) {
                assert.equal(err, null);
                var group_user_ids = [];
                for (var i = 0; i < group_ids.length; i++) {
                    group_user_ids.push(ObjectId(group_ids[i]['_id'].toString()));
                }

          var temparr=[];
          for(var i=0;i<chat_friends.length;i++)
            {
               if(!JSON.stringify(group_user_ids).includes(JSON.stringify(chat_friends[i]))){
                  temparr.push(
                       chat_friends[i],
                       );
                  }
                }

        let where = {
            // _id:{$in:chat_friends},
            _id:{$in:temparr}
          }
          if(search != ""){
            where['user_name'] = { $regex: search, $options: 'i' } ;
          }
          db.get().collection("aug_users").find(where).limit(limitParams).project({_id:1,user_status:1,user_name:1,user_email:1,issocial_login:1,user_gender:1,user_dob:1,profile_picture:1,chat_userId:1}).toArray(function(err, docs3) {
            assert.equal(err, null);
            res.send({status:1,
                      totalPages: Math.ceil(docs3.length / limitParams),
                      currentPageIndex: pagination_index,
                      totalCount: docs3.length,
                      data:docs3});return true;
          })
          })
        })
        })
        }else{
          res.send({status:0,error:"no users found"});return true;
        }
      })

    });

};
