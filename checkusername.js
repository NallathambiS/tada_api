'use strict';
var _ = require('lodash');
const assert = require('assert');
var db = require("../../db")

exports.checkusername = function(req, res) {

    let user_name = req.body.user_name;

    if (user_name == "" ||  user_name == undefined ){
       res.send({status:0,error:"missing input fields"});
       return true;
    }

    if(user_name != undefined || user_name != ""){
      user_name=user_name.toLowerCase().trim();
    }

    let where = {};
    where['user_name'] = user_name;

    db.get().collection("aug_users").find(where).limit(1).skip(0).toArray(function(err, docs) {
      assert.equal(err, null);
      if (docs.length > 0){
        res.send({status:1,info:"user name exists"});
      } else{
        //user not found
        res.send({status:0,error:"user not found"});
      }

    });

};
