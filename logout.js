'use strict';
var _ = require('lodash')
var async = require('async')
var mongo = require('mongodb')
const assert = require('assert')
var db = require("../../db")
var ObjectId = mongo.ObjectID


exports.logout = function(req, res) {

    let user_id     =  req.body.user_id

    if(user_id == undefined || user_id == ""){
      res.send({status:0,error:"missing mandatory element user_id"})
      return true
    }

    async.parallel([
      function(callback) {
        db.get().collection('aug_users').updateOne({_id: ObjectId(user_id)}, {$set: { isonline : false}}, function(err, result) {
          assert.equal(err, null)
          callback(null, result)
        })
      },
      function(callback) {
         db.get().collection("aug_users_token").deleteMany({user_id: ObjectId(user_id)},function(err, docs) {
           assert.equal(err, null)
           callback(null, docs)
         })
      }],
      function(err, results) {
        if(results[0] || results[1]){
          var result_data = {
            status : 1
          }
        }else{
          var result_data = {
            status : 0,
            error : "logout failed"
          }
        }
        res.send(result_data)
    })

};
