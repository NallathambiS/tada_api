'use strict';
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')

exports.setpublic = function(req, res) {
  var user_id =  req.body.user_id;
  var public_account=req.body.public_account
    let where = {_id:ObjectId(user_id)}
    db.get().collection("aug_users").updateOne(where,{$set:{public_account:public_account}},{upsert:false},function(err, docs) {  
          assert.equal(err, null);
          if (docs.result['nModified']!=0) {
            res.send({status:1,public_account:public_account})
          }
          else {
            res.send({status:0,message:"public_account not changed"});
          }
    })
}
