'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')
var Settings = require("../../lib/Settings");
const crypto = require("crypto");
var init = require("../../init");

const gcm = require("node-gcm");
var sender = new gcm.Sender(init.gcm_key);
const apn = require("apn");
var apnProvider = new apn.Provider(init.apnoptions);


exports.favouritevideo = function(req, res) { 
	  var video_id =  req.body.video_id;
    var user_id =  req.body.user_id;
    var fav =  req.body.favourite;

    if(!user_id || user_id == "" || !fav || fav == ""){
      res.send({status:0,error:"Elements not found"});
      return true;
    }
    
    let where={_id:ObjectId(video_id)}
    let input_data={} 
     if(fav==='favourite'){ 
      input_data={$addToSet: {favourite:{user_id:ObjectId(user_id)}}}
      db.get().collection('av_video').updateOne(where,input_data, function(err, result) {
      assert.equal(err, null);

      if(result){   
        db.get().collection('av_video').find({_id:ObjectId(video_id)}).toArray(function(err, resultvid) {
            assert.equal(err, null);
            let vid_user_id=resultvid[0]['video_user_id'];
            let video_thumb_image=resultvid[0]['thumb_image'];

            db.get().collection('aug_users').find({_id:ObjectId(vid_user_id)}).toArray(function(err, resultuser) {
            assert.equal(err, null);
            if(resultuser && resultuser[0] && resultuser[0]['push_notification'] == true){  //send notification only if end user enable notification
                db.get().collection('aug_users_token').find({user_id:ObjectId(vid_user_id)}).toArray(function(err, result2) {
                assert.equal(err, null);
                if(result2 && result2.length > 0 && result2[0]['token_key'] !=""){ 
                  var token_key = result2[0]['token_key'];
                  var platform = result2[0]['platform'];
                  db.get().collection('aug_users').find({_id:ObjectId(user_id)}).toArray(function(err, result3) {  
                  assert.equal(err, null);
                  var friend_name = result3[0]['user_name'];
                  var profile_picture = result3[0]['profile_picture'];
                  db.get().collection('sn_notification_type').find({type_name:"post_like_notification"}).toArray(function(err, result4) { 
                    assert.equal(err, null);
                    var screen_id = result4[0]['screen_id'];
                    var screen_name = result4[0]['screen_name'];
                    var msg_text = result4[0]['msg_text'] || "";
                    msg_text = msg_text.replace("{name}",friend_name)
                    var input_data = {
                      notification_type_id : result4[0]['_id'],
                      notification_type_name : "post_like_notification",
                      notification_title:msg_text,
                      notification_message:"invited",
                      thumb_image:profile_picture,
                      video_thumb_image:video_thumb_image,
                      seen:false,
                      isnew:true,
                      post_id:ObjectId(video_id),
                      page:"single feed",
                      receiver_id:ObjectId(vid_user_id),
                      sender_id:ObjectId(user_id),
                      last_update:new Date()
                    }
                    if(vid_user_id!=user_id){
                    db.get().collection('sn_notifications').insert(input_data, function(err, result) { 
                      assert.equal(err, null);
                      var payloadios = { 
                      type:"post_like_notification",
                      page:"single feed", 
                      post_id:video_id
                      // thumb_image:video_thumb_image 
                      }
                      
                      console.log(platform);
                      if(platform == "IOS"){
                        var note = new apn.Notification();
                        note.aps.badge = 1;
                        note.aps.alert = msg_text;
                        note.aps.sound = "default";
                        note.payload = payloadios; 
                        note.topic = "com.augray.avmessage"; 
                        apnProvider.send(note, token_key).then( (result) => {
                          console.log(result);
                          res.send({status:1,message:"Favourite Sent"}); //,ios_result:result
                          return true;
                        });
                      }else{
                        var regTokens = [token_key];
                        var datamsg = {}
                                  datamsg['body'] = msg_text;
                                  datamsg['type']="post_like_notification";
                                  datamsg['page']="single feed";
                                  datamsg['post_id']=video_id;
                                  datamsg['sender_id']=user_id;
                                  datamsg['sender_name']=friend_name; 
                                  datamsg['image_url']=profile_picture; 
                                  // datamsg['thumb_image']=video_thumb_image; 
                        var message = new gcm.Message({
                              data: datamsg 
                          }); 

                          sender.send(message, { registrationTokens: regTokens }, function (err, response) {
                              if (err) console.error(err);
                              else console.log(response);
                              res.send({status:1,message:"Favourite Sent"});//,android_result:response
                              return true;
                          });
                      }
                    
                    })
                  }
                  else
                    {
                      res.send({status:1,message:"Favourite cannot sent to own user"});//,android_result:response
                      // return true;
                    }
                  })
                })
              }  
                })
              }  
            })
})
}




       // res.send({status:1,message:"favourite added and sent"});
      return true;
    }) 
	}
	else if(fav==='unfavourite'){
      input_data={ $pull: { favourite:{user_id:ObjectId(user_id) }}}
		  db.get().collection('av_video').update(where,input_data, function(err, result) {
      assert.equal(err, null);

      res.send({status:1,message:"favourite removed"});
      return true;
    })
	}
   else{
    res.send({status:0, message:"favourite type not found"});
  } 
}
