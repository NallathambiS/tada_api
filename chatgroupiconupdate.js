'use strict';
var async = require('async')
var mongo = require('mongodb')
const assert = require('assert')
var ObjectId = mongo.ObjectID
var request = require('request')
var fs = require('fs')
var path = require('path')
var db = require("../../db")

exports.chatgroupiconupdate = function(req, res) {

try{
      var admin_user_id = req.body.admin_user_id; 
      var room_id = req.body.room_id;
      var group_icon=req.body.group_icon;

      if(admin_user_id === undefined || admin_user_id === "" || room_id === undefined || room_id === ""
        || group_icon === undefined || group_icon === ""){
        res.send({status:0,error: "Missing mandatory elements"});
        return true;
      }

      db.get().collection("aug_users").find({_id : ObjectId(admin_user_id)}).limit(1).skip(0).toArray(function(err, docs){
      assert.equal(err, null);

      var myJSONObject = {
        "roomId": room_id,
        "topic": group_icon
      }
      let username= docs[0]['user_name']
        if(docs[0] && docs[0]['chat_authToken'] && docs[0]['chat_userId']){
          request({
              url: "https://chat.augray.com/api/v1/groups.setTopic",
              method: "POST",
              headers: {
                  "content-type": "application/json",
                  "X-Auth-Token": docs[0]['chat_authToken'],
                  "X-User-Id": docs[0]['chat_userId'],
              },
              json: true,
              body: myJSONObject,
          },
           function (error, response, body){  
            console.log(body)
              if(body.success === true){

                var myJSONCustomfield = {
                "roomId": room_id,
                "customFields":{"groupicon": group_icon}
              }

                request({
                  url: "https://chat.augray.com/api/v1/groups.setCustomFields",
                  method: "POST",
                  headers: {
                      "content-type": "application/json",
                      "X-Auth-Token": docs[0]['chat_authToken'],
                      "X-User-Id": docs[0]['chat_userId'],
                  },
                  json: true,
                  body: myJSONCustomfield,
                  },
                   function (error, response, body){  
                    console.log(body)
                      if(body.success === true){
                        let group_details=body.group
                        request({
                        url: "http://13.250.36.41:3000/api/v1/groups.messages?roomId=" + room_id +"",
                        method: "GET",
                        headers: {
                            "content-type": "application/json",
                            "X-Auth-Token": docs[0]['chat_authToken'],
                            "X-User-Id": docs[0]['chat_userId'],
                        },
                        json: true 
                        },
                         function (error, response, body){  
                          console.log(body)
                            if(body.success === true){

                              let all_messages=[]
                              let topic_messages=[]
                              all_messages=body.messages 
                              for (var i = 0 ; i<all_messages.length ; i++) {
                                if(all_messages[i]['t']=="room_changed_topic"){
                                  topic_messages.push(all_messages[i]) //anyway topmost will come first   
                                  break                                
                                }
                              }
                              let message_id=topic_messages[0]['_id']
                              var myJSONmessagedelete = {
                              "roomId": room_id,
                              "msgId":message_id 
                              }
                              request({
                                url: "https://chat.augray.com/api/v1/chat.delete",
                                method: "POST",
                                headers: {
                                    "content-type": "application/json",
                                    "X-Auth-Token": docs[0]['chat_authToken'],
                                    "X-User-Id": docs[0]['chat_userId'],
                                },
                                json: true,
                                body:myJSONmessagedelete
                                },
                                 function (error, response, body){  
                                console.log(body)
                                if(body.success === true){
                                  var myJSONmessagepost = {
                                  "roomId": room_id,
                                  "text":"Information: Group Icon updated by " + username + "" 
                                  }
                                  request({
                                    url: "https://chat.augray.com/api/v1/chat.postMessage",
                                    method: "POST",
                                    headers: {
                                        "content-type": "application/json",
                                        "X-Auth-Token": docs[0]['chat_authToken'],
                                        "X-User-Id": docs[0]['chat_userId'],
                                    },
                                    json: true,
                                    body:myJSONmessagepost
                                    },
                                     function (error, response, body){  
                                    console.log(body)
                                    if(body.success === true){
                                      res.send({status:1,group_details:group_details}) 
                                    }
                                    else
                                    {
                                      res.send({status:0, message:"chat.update error"}) //cannot update message
                                    }
                                  })
                                  // res.send({status:1,group_details:group_details}) 
                                }
                                else
                                {
                                  res.send({status:0, message:"chat.update error"}) //cannot update message
                                }
                              })
                            }
                            else
                            {
                              res.send({status:0, message:"groups.messages error"}) //cannot update message
                            }
                          })
                        // res.send({status:1,group_details:body.group}) 
                      }else{
                        res.send({status:0, msg:"setCustomFields"})
                      }
                  });
                // res.send({status:1,groupicon:body.topic})
              }else{
                res.send({status:0})
              }
          });
        }else{
          res.send({status:0})
        }

    })
    }
    catch(errorcatch){
      console.log(errorcatch);
    }
}
