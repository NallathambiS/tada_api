'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var ObjectId = require('mongodb').ObjectID
var request = require('request')
var db = require("../../db")

exports.regdevice = function(req, res) {
  try{
    var input_data = {};
    var isGuest = false;

    input_data['deviceid'] = req.body.deviceid ;
    input_data['platform'] = req.body.platform ;
    input_data['token_key'] = req.body.token_key ;
    let update_data={}
    for (var item in input_data) {
        if(input_data[item] == "" || input_data[item] == undefined){
            res.send({status:0,error: "Missing mandatory elements "+item});
            return true;
        }
      }


    if(input_data['deviceid'] === "" || input_data['deviceid'] === undefined){
      res.send({status:0,error: "Missing mandatory element deviceid for guest user"});
    }
    if(req.body.language_code!=null && req.body.language_code!=""){
      if(!Array.isArray(req.body.language_code)){
      return res.send({"status":0,message:"language_code is an array and should consist of alteast one element"})
      }
      input_data['language_code'] = update_data['language_code']= req.body.language_code;
    }

    input_data['userid'] = req.body.userid ;
    input_data['country'] = req.body.country ||"";
    input_data['country_code'] = req.body.country_code ||"";
    input_data['city'] = req.body.city ||"";
    input_data['geo_lat'] = (req.body.geo_lat == "") ? 0 : req.body.geo_lat;
    input_data['geo_long'] = (req.body.geo_long == "") ? 0 : req.body.geo_long;

    if(input_data['userid'] === undefined || input_data['userid'] === ""){
      isGuest = true;
    }

    var where;
    if(isGuest === false){
      where = {
         user_id : ObjectId(input_data['userid'])
         // device_id: input_data['deviceid']
      }
    }else{
      where = {
        device_id: input_data['deviceid']
      }
    }


        async.parallel([
          function(callback) {
             update_data = {
              $set : {
                user_id: input_data['userid'] ? ObjectId(input_data['userid']) : null,
                is_guest: isGuest,
                platform : input_data['platform'],
                country : input_data['country'],
                country_code : input_data['country_code'],
                city : input_data['city'],
                token_key : input_data['token_key'],
                device_id : input_data['deviceid'],
                last_update : new Date(),
                geo : {
                  "type":"Point",
                  "coordinates":[parseFloat(input_data['geo_long']),parseFloat(input_data['geo_lat'])]
                }
              }
            }
            db.get().collection("aug_users_token").update(where,update_data,{upsert:true},function(err, docs) {
              assert.equal(err, null);
              callback(null, docs);
            });
          },
          function(callback) {
            var whereID;
            if(!isGuest){
              whereID = {
                _id : ObjectId(input_data['userid'])
              }
            }else{
              whereID={
                device_id:input_data['deviceid']
              }
            }
            //==================rc_integration==================
            if(!isGuest){
            db.get().collection("aug_users").find({_id : ObjectId(input_data['userid'])}).limit(1).skip(0).toArray(async function(err, docs){
              assert.equal(err, null);
              if(docs.length !== 0){
                var myJSONObject = {
                  "user": docs[0]['user_name'],
                  "password": docs[0]['chat_password'],
                }
                await request({
                    url: "https://chat.augray.com/api/v1/login",
                    method: "POST",
                    headers: {
                        "content-type": "application/json"
                    },
                    json: true,
                    body: myJSONObject,
                }, function (error, response, body){
                     update_data = {
                      $set : {
                         device_id : input_data['deviceid'],
                         country_code:input_data['country_code'],//added
                         is_guest: isGuest,
                         user_active : true,
                         role_id : 1,
                         isonline : true,
                         search_distance : 20,
                         reg_date: new Date(),
                         last_update : new Date(),
                      }
                    }
                      if(body && body.status && body.status === 'success'){
                        update_data['$set']['chat_authToken'] = body.data && body.data.authToken;
                        update_data['$set']['chat_userId'] = body.data && body.data.userId;
                      }
                      db.get().collection("aug_users").findOneAndUpdate(whereID,update_data,{upsert:isGuest},function(err, docs){
                        assert.equal(err, null);
                       

              //==========chat thread create with tadateam==========





         let from_user= (callback0)=> {
        db.get().collection("aug_users").find({_id : ObjectId(input_data['userid'])}).limit(1).skip(0).toArray((err, docs)=>{
          assert.equal(err, null);
          callback0(null, docs);
        })
      }
      let to_user= (callback1)=> {
        db.get().collection("aug_users").find({_id : ObjectId("5ec00bb1f97acc0e06f725c0")}).limit(1).skip(0).toArray((err, docs)=>{  //ObjectId("5e01eaf22d11434ed480ac30")
          assert.equal(err, null);
          callback1(null, docs);
        })
      }

      async.parallel({
        fromdata: from_user,
        todata: to_user
      },

      (err, results) =>{
        if (err) {
          res.send({
            status: 0
          })
          return
         }
        if(results.length !== 0){
          var data = {
            from: results['fromdata'][0], //results[0] && results[s0][0],
            to: results['todata'][0] //results[1] && results[1][0],
          };

          if(data){
            var myJSONObject = {
              "username": data['to']['user_name']
            };

            request({
                //url: "https://tada.augray.com:3001/api/v1/im.create",
                url: "https://chat.augray.com/api/v1/im.create",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    "X-Auth-Token": data['from']['chat_authToken'],
                    "X-User-Id": data['from']['chat_userId'],
                },
                json: true,
                body: myJSONObject,
            },  (error, response, body)=>{
              console.log(body)
                if(body.success === true){
                  return false;// res.send({status:1,room_details:body.room})
                }else{
                  res.send({status:0})
                }
            });
          }else{
            res.send({status:0})
          }
        }
      })

 callback(null, docs);
                      });
                });
              }

            });
            //==================rc_integration==================
          }else{
              update_data = {
                      $set : {
                         device_id : input_data['deviceid'],
                         country_code:input_data['country_code'],//added
                         is_guest: isGuest,
                         user_active : true,
                         role_id : 1,
                         isonline : true,
                         search_distance : 20,
                         language_code : ['ENG'],
                         reg_date: new Date(),
                         last_update : new Date(),
                      }
                    }
                     db.get().collection("aug_users").findOneAndUpdate(whereID,update_data,{upsert:isGuest},function(err, docs){
                        assert.equal(err, null);
                        callback(null, docs);
                      });
          }
            //==================rc_integration==================
           },
           function(callback) {
             let data = {
               is_guest: isGuest,
               user_id : input_data['userid'] ? ObjectId(input_data['userid']) : null,
               platform : input_data['platform'],
               country : input_data['country'],
               country_code : input_data['country_code'],
               city : input_data['city'],
               last_update : new Date(),
               geo : {
                 "type":"Point",
                 "coordinates":[parseFloat(input_data['geo_lat']),parseFloat(input_data['geo_long'])]
               }
             }
             db.get().collection("aug_users_track").insert(data,function(err, docs) {
               assert.equal(err, null);
               callback(null, docs);
             });
           }
          ],
          function(err, results) {
            var result_data={};
            if(results.length > 0){

              result_data = {
                status : 1,
                guestID: isGuest === false ? undefined :
                            (results[1].value && results[1].value._id ? results[1].value && results[1].value._id :
                              results[1].lastErrorObject && results[1].lastErrorObject.upserted),

                chat_email: isGuest === false ? results[1].value.chat_email : undefined,
                chat_password: isGuest === false ? results[1].value.chat_password : undefined,
                isGuest: isGuest
              }

           // }
          }else{
               result_data = {
                status : 0,
                error : "Registration failed"
              }
            }
            res.send(result_data);
        });
}
catch(errorcatch){
  console.log(errorcatch);
}
};
