'use strict';
var _ = require('lodash')
var async = require('async')
var mongo_lib = require('mongodb')
const assert = require('assert')
var db = require("../../db")
var ObjectId = mongo_lib.ObjectID

exports.getblockusers = function(req, res) {
    let user_id = req.body.user_id
    let search = req.body.search || ""
    let pagination_limit = req.body.pagination_limit || 50
    let pagination_index = req.body.pagination_index || 1
    let skipParams = parseInt(pagination_limit * (pagination_index - 1))
    let limitParams = parseInt(pagination_limit)
    let totalCount=0
    let temparr2=[];

    if(!user_id || user_id == ""){
      let rescode = {
        status : 0,
        error  : "Missing mandatory elements user_id"
      }
      res.send(rescode);
      return false;
    }
    let where ={
     blocker : ObjectId(user_id)
    }
      db.get().collection("user_blocklist").find(where).toArray(function(err, docs) { 
        assert.equal(err, null)
        let blocked_users = []
        if (docs.length>0) {
          for (var i = 0; i < docs.length; i++) {
            if (docs[i]['blocker'] == user_id){
              blocked_users.push(ObjectId(docs[i]['blocked']));
            }
          }
        }
                 
        let where = { 
          _id:{$in:blocked_users}
        }

        if(search == ""){
        db.get().collection("aug_users").find(where).count({},function(err, totcountdocs) {  
          assert.equal(err, null);
          if (totcountdocs>0) {
            totalCount=totcountdocs;
          }
         })
        }
        if(search != ""){
          where['user_name'] = { $regex: search, $options: 'i' } ;

           db.get().collection("aug_users").find(where).count({},function(err, totcountdocs) {
           assert.equal(err, null);
           if (totcountdocs>0) {
            totalCount=totcountdocs;
          }
         })
        }               
        db.get().collection("aug_users").find(where).limit(limitParams).skip(skipParams).project({_id:1,user_name:1,user_email:1,user_gender:1,user_dob:1,profile_picture:1}).toArray(function(err, docs3) {
        assert.equal(err, null);      
        if(docs3.length>0)  { 
          for (var i = docs3.length - 1; i >= 0; i--) {
            temparr2.push({
                        "_id":docs3[i]._id,
                        "user_name":docs3[i].user_name,
                        "user_gender":docs3[i].user_gender,
                        "user_dob":docs3[i].user_dob,
                        "profile_picture":docs3[i].profile_picture 
                    }); 
          }                      
              res.send({
                status: 1,
                totalPages: Math.ceil(totalCount / limitParams),
                currentPageIndex: pagination_index,
                totalCount: totalCount,
                data: temparr2
              });
            }
          else{
            res.send({status:0,error:"no users found"});return true;
          } 
      }) 
  })
}
