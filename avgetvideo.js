'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")

exports.avgetvideo = function(req, res) {
var userid = req.body.user_id;
var videotype=req.body.video_type;

  if(userid == "" || userid == undefined || videotype == "" || videotype == undefined){
    res.send({status:0,error:"missing input field"});
    return true;
  }

  var pagination_limit = req.body.pagination_limit || 50;
  var pagination_index = req.body.pagination_index || 1;

  var skipParams = parseInt(pagination_limit * (pagination_index - 1));
  var limitParams = parseInt(pagination_limit); 
 
 let videocount = (callback0) => {  
        db.get().collection("av_video").find({'video_user_id': ObjectId(userid),'video_type': videotype}).count({},(err, docscount) => {
          assert.equal(err, null);
          callback0(null,docscount);
        });
      }
   let videodata = (callback1) => {                   
      db.get().collection("av_video").aggregate([
        { "$match"      : {'video_user_id': ObjectId(userid),'video_type': videotype}},
        { "$skip"       :  skipParams }, 
        { "$limit"      :  limitParams}, 
        { "$sort"       :  { _id: -1 }},
        { "$lookup"     : {
          "from"        : "av_video",
          "localField"  : "_id",
          "foreignField": "_id",
          "as"          : "av_videodata"
        }},  
        { "$unwind"     : {"path": "$av_videodata", "preserveNullAndEmptyArrays": true}}, 
        { "$project"    : { 
          "video_user_id": 1,
          "thumb_image" : 1,
          "video_path"  : 1,                        
          "is_public"   : 1, 
          "video_type"  : 1, 
          "last_update" : { $dateToString: { format: "%d/%m/%Y %H:%M:%S", date: "$last_update" } },
          "is_fav_count":  {
            "$filter"   : {
               "input"  : "$av_videodata.favourite",
               "as"     : "item",
               "cond"   : { "$eq": [ "$$item.user_id", ObjectId(userid) ] }
            }
         }  
       }}  
        ,
        { "$project"    : {
          _id           :  1, 
          "video_user_id": 1,
          "thumb_image" : 1,
          "video_path"  : 1,                        
          "is_public"   : 1, 
          "video_type"  : 1, 
          "last_update" : "$last_update",
          "is_favourite":{   
              "$switch" : {
             "branches" : [
                { "case": {$gt:[{"$size":"$is_fav_count"},0]}, then: true },
                { "case": {$eq:[{"$size":"$is_fav_count"},0]}, then: false },
                { "case": { $eq: ["$is_fav_count", null]}, then :false },
                { "case": { $eq: ["$is_fav_count", undefined]}, then :false } ,
                { "case": { $eq: ["$is_fav_count", ""]}, then :false }
             ],
             "default": false
              }
          }
         } 
        }
      ]).toArray((err, docs2) => {  
        assert.equal(err, null);   
        callback1(null,docs2)  
      });
          
     }  
          
      async.parallel({
      videocount: videocount,
      videodata: videodata
    },
    (err, results) => { 

        if (err) {
            res.send({
              status: 0
            })
            return
        }
          res.send({
          status:1,
          totalPages: Math.ceil(results.videocount / limitParams),
          currentPageIndex: pagination_index,
          totalCount: results.videocount,
          data: results.videodata  
        }) 
  }) 
}
