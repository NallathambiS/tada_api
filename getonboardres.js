'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')

exports.getonboardres = function(req, res) {

    let gender = req.body.gender
    let gender_array = ['M','F']

    if(!gender){
      let randomGender = Math.floor(Math.random()*gender_array.length)
      gender = gender_array[randomGender]
    }

    let where = { gender }
    where.asset_name = { $regex: `onb_${gender}`, $options: 'i' }

    let animation_type = {
      animation_type_name: "Onboarding"
    }

    let asset = (callback0) => {
      db.get().collection("av_asset").aggregate([
        {
          "$match" : where
        },
        {
          $lookup: {
            "from": "av_asset_type",
            "localField": "asset_type_id",
            "foreignField": "_id",
            "as": "ob_asset"
          }
        },
        {
          $unwind: "$ob_asset"
        },
        {
          $project: {
            asset_type_name: "$ob_asset.asset_type_name",
            asset_type_id: 1,
            char_type_id: 1,
            gender: 1,
            user_id: 1,
            asset_name: 1,
            thumb_image: 1,
            file_path_ios: 1,
            file_path_android: 1,
            metadata: 1,
            asset_enable: 1,
            country_code: 1,
            point: 1,
            last_update: 1
          }
        }
      ]).toArray((err, docs) => {
        assert.equal(err, null);
        callback0(null, docs);
      })
    }

    let animation = (callback1) => {
      db.get().collection("av_animation_type").aggregate([
        {
          "$match" : animation_type
        },
        {
          $lookup: {
            "from": "av_animation",
            "localField": "_id",
            "foreignField": "animation_type_id",
            "as": "ob_animations"
          }
        },
        {
          $unwind: "$ob_animations"
        },
        {
          $lookup: {
            "from": "av_animation_type",
            "localField": "ob_animations.animation_type_id",
            "foreignField": "_id",
            "as": "av_animation_type_collection"
          }
        },
        {
          $unwind: "$av_animation_type_collection"
        },
        { "$lookup": {
            "from": "av_asset",
            "localField": "ob_animations.plugin_asset_id",
            "foreignField": "_id",
            "as": "plugindata"
          }}, 
      { "$unwind": {"path": "$plugindata", "preserveNullAndEmptyArrays": true}}, 
        {
          $project: {
            "_id": "$ob_animations._id",
            "user_id": "$ob_animations.user_id",
            "char_type_id": "$ob_animations.char_type_id",
            "animation_type_id": "$ob_animations.animation_type_id",
            "animation_type_name": "$av_animation_type_collection.animation_type_name",
            "animation_name": "$ob_animations.animation_name",
            "file_path_ios": "$ob_animations.file_path_ios",
            "file_path_android": "$ob_animations.file_path_android",
            "thumb_image": "$ob_animations.thumb_image",
            "metadata": "$ob_animations.metadata",
            "plugin_asset_id": { "$cond": [{ "$eq": [ "$plugin_asset_id", null ] },"$$REMOVE", "$plugin_asset_id" ] },
            "plugin_asset"   : { "$cond": [{ "$eq": [ "$plugindata", null ] },"$$REMOVE", "$plugindata" ] },
            "animation_enable": "$ob_animations.animation_enable",
            "country_code": "$ob_animations.country_code",
            "point": "$ob_animations.point",
            "last_update": "$ob_animations.last_update"
          }
        }

      ]).toArray((err, docs) => {
        assert.equal(err, null);
        callback1(null, docs);
      })
    }

    async.parallel({
      onboard_asset: asset,
      onboard_animation: animation
    },
    (err, results) => {

        if (err) {
            res.send({
              status: 0
            })
            return
        }

        res.send({
          status:1,
          data: [
            {
              c_type: "asset",
              count: _.size(results.onboard_asset),
              gender: gender,
              resource_list: results.onboard_asset
            },
            {
              c_type: "animation",
              count: _.size(results.onboard_animation),
              resource_list: results.onboard_animation
            }
          ]
        })
      }
    )
}

// exports.getonboardres = function(req, res) {
//
//     let gender = req.body.gender
//     let gender_array = ['M','F']
//
//     if(!gender){
//       let randomGender = Math.floor(Math.random()*gender_array.length)
//       gender = gender_array[randomGender]
//     }
//
//     let where = { gender }
//
//     async.parallel({
//       onboard: function(callback0) {
//         db.get().collection("init_onboard_resource").aggregate([
//           { "$match" : where},
//           { "$lookup": {
//             "from": "av_animation",
//             "localField": "resource_list.animation_id",
//             "foreignField": "_id",
//             "as": "ob_animations"
//           }
//           },
//           { "$lookup": {
//               "from": "av_asset",
//               "localField": "resource_list.asset_id",
//               "foreignField": "_id",
//               "as": "ob_assets"
//             }
//
//           },
//
//           {"$project" : {
//              "_id" : 0,
//              "created_date" : 0,
//              "last_updated" : 0,
//              "resource_list" : 0,
//              "ob_animations.char_type_id" : 0,
//              "ob_animations.animation_type_id" : 0,
//              "ob_animations.animation_enable" : 0,
//              "ob_animations.user_id" : 0,
//              "ob_animations.c_type" : 0,
//              "ob_animations.gender" : 0,
//              "ob_animations.ob_animations" : 0,
//              "ob_assets.asset_type_id" : 0,
//              "ob_assets.user_id" : 0,
//              "ob_assets.asset_enable" : 0,
//              "ob_assets.c_type" : 0,
//              "ob_assets.gender" : 0,
//              "ob_assets.ob_assets" : 0,
//              "ob_assets.asset_enable" : 0,
//              "ob_assets.char_type_id" : 0
//           }}
//         ]).toArray(function(err, docs) {
//           assert.equal(err, null);
//           callback0(null, docs);
//         });
//
//       },
//       defassets_m: function(callback1) {
//        db.get().collection("preset_asset").aggregate([
//           { "$match" : {gender: 'M'}},
//           { $sample: { size: 1 } },
//           { "$lookup": {
//             "from": "av_asset",
//             "localField": "asset_list",
//             "foreignField": "_id",
//             "as": "assets"
//           }},
//           {
//             "$project" : {
//               "_id": 0,
//               "category_name": 0,
//               "gender": 0,
//               "preset_name": 0,
//               "asset_list": 0,
//             }
//           }
//         ]).toArray(function(err, docs) {
//           assert.equal(err, null);
//           callback1(null, docs);
//         });
//       },
//       defassets_f: function(callback1) {
//         db.get().collection("preset_asset").aggregate([
//               { "$match" : {gender: 'F'}},
//               { $sample: { size: 1 } },
//               { "$lookup": {
//                 "from": "av_asset",
//                 "localField": "asset_list",
//                 "foreignField": "_id",
//                 "as": "assets"
//               }},
//               {
//                 "$project" : {
//                   "_id": 0,
//                   "category_name": 0,
//                   "gender": 0,
//                   "preset_name": 0,
//                   "asset_list": 0,
//                 }
//               }
//             ]).toArray(function(err, docs) {
//               assert.equal(err, null);
//               callback1(null, docs);
//             });
//       }
//     },
//       function(err, results) {
//         res.send({
//           status:1,
//           data: results.onboard,
//           defassets_m: results.defassets_m[0].assets,
//           defassets_f: results.defassets_f[0].assets
//         });
//       }
//     )
// }
