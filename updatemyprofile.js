'use strict';
var Settings = require("../../lib/Settings")
var _ = require('lodash')
var async = require('async')
var mongo = require('mongodb')
const assert = require('assert')
var ObjectId = mongo.ObjectID
var AWS = require('aws-sdk')
var path = require('path')
var request = require('request')
var db = require("../../db")


exports.updatemyprofile = function(req, res) {
try{
    let input_data = {}
    let update_data = {}

    input_data['user_id']   = req.body.user_id


     for (var item in input_data) {
      if(input_data[item] == "" || input_data[item] == undefined){
          res.send({status:0,error: "Missing mandatory elements "+item});
          return true;
      }
    }

    if(req.body.user_dob!=null && req.body.user_dob!=""){
      input_data['user_dob']  = update_data['user_dob']   =  req.body.user_dob
    }
    if(req.body.user_gender!=null && req.body.user_gender!=""){
      input_data['user_gender']  = update_data['user_gender']   =  req.body.user_gender
    }
    if(req.body.country_code!=null && req.body.country_code!=""){
      input_data['country_code']   = update_data['country_code']  =  req.body.country_code;
    }

    if(!ObjectId.isValid(input_data['user_id'])){
      res.send({status:0,error: "user_id is not mongo object ID"});
      return true;
    }

    if((req.body.user_email==null || req.body.user_email=="") && (req.body.user_phone==null || req.body.user_phone=="")){
       res.send({status:0,error: "email/phone required"});
      return true;
    }

    if(req.body.user_phone!=null && req.body.user_phone!=""  && req.body.phone_code!=null && req.body.phone_code!=""){
       input_data['phone_code'] = update_data['phone_code']= req.body.phone_code;
       input_data['user_phone'] = update_data['user_phone']= req.body.user_phone;
    }
    if(req.body.user_phone && !req.body.phone_code){
      res.send({status:0,error: "Missing phone_code"});
    }
    if(req.body.user_email!=null && req.body.user_email!=""){
       input_data['user_email'] = update_data['user_email']= req.body.user_email;
    }

    if(req.body.language_code!=null && req.body.language_code!=""){
      if(!Array.isArray(req.body.language_code)){
      return res.send({"status":0,message:"language_code is an array and should consist of alteast one element"})
      }
      input_data['language_code'] = update_data['language_code']= req.body.language_code;
    }

    var path_profilepic = '';
    var path_profilepic_org = '';

    if(req.body.profile_picture != ""  && req.body.profile_picture != undefined){
      input_data['profile_picture']  = update_data['profile_picture']   =  req.body.profile_picture;
        path_profilepic = req.body.profile_picture;
    }

    if(req.body.profile_picture_org != ""  && req.body.profile_picture_org != undefined){
      input_data['profile_picture_org']  = update_data['profile_picture_org']   =  req.body.profile_picture_org;
        path_profilepic_org = req.body.profile_picture_org;
    }

    if(req.body.user_about != ""  && req.body.user_about != undefined){
      input_data['user_about']  = update_data['user_about']   =  req.body.user_about;
    }


     if(req.body.user_email!=null && req.body.user_email!=""){
       input_data['user_email'] = update_data['user_email']= req.body.user_email;
    }
    if(req.body.is_social_login!=null && req.body.is_social_login!="" && req.body.is_social_login===true){
        //if(phone!=null && phone!=""){
         if(req.body.social_type!=null && req.body.social_type!="" && req.body.social_user_id!=null && req.body.social_user_id!=""){
         input_data['is_social_login'] = update_data['is_social_login']=  req.body.is_social_login;
         input_data['social_type']    = update_data['social_type'] =  req.body.social_type;
         input_data['social_user_id']  = update_data['social_user_id']  =  req.body.social_user_id;
       }else{
           res.send({status:0,error: "Missing mandatory elements social_type/social_user_id bcz socail_login is entered"});
          return true;
       }
     }
      if(req.body.is_social_login!=null && req.body.is_social_login!="" && req.body.is_social_login===false){
         input_data['is_social_login'] = update_data['is_social_login']=  req.body.is_social_login;
         update_data['social_type'] = update_data['social_user_id'] =  "";
      }

      /* var where_username = {
       // user_name: input_data['user_name'],
        _id: {$ne: ObjectId(input_data['user_id'])}
    }

     db.get().collection('aug_users').find(where_username).toArray(function(err, docs) {
      assert.equal(err, null);
      if(docs.length > 0){
        res.send({status:0,message:"user_name already exists"})
      }*/



    var where = {
        _id: ObjectId(input_data['user_id'])
    }
    var old_path_profilepic = '';
    var old_path_profilepic_org = '';
    db.get().collection('aug_users').find(where).toArray(function(err, result) {
      assert.equal(err, null);
      if(result.length > 0){
        old_path_profilepic = result[0]['profile_picture'];
        old_path_profilepic_org = result[0]['profile_picture_org'];

        if(path_profilepic != "" && path_profilepic != undefined && path_profilepic != null){
          update_data['profile_picture'] = path_profilepic;


          if(old_path_profilepic != "" && old_path_profilepic != undefined && old_path_profilepic != null){
          var s3 = new AWS.S3({
             accessKeyId: Settings.s3.key,
             secretAccessKey: Settings.s3.secret
           });
          var mynewBucket = Settings.s3.bucketnew;

           var del_filename = path.basename(old_path_profilepic);

          var params = {
              Bucket: mynewBucket,
              Key:  Settings.s3['path-avatar'] + del_filename
          };
          s3.headObject(params, function (err, metadata) {
            if (!err || err.code != 'NotFound') {
              s3.deleteObject(params, function(err, data) {
               if (err) console.log(err, err.stack); // an error occurred
               else     console.log(data);           // successful response
              });
            }
          });

           db.get().collection("aug_users").find({_id : ObjectId(input_data['user_id'])}).limit(1).skip(0).toArray(function(err, docs){
          assert.equal(err, null);

        if(docs.length !== 0 && docs[0]['chat_authToken'].length!="" && docs['chat_userId']!=""){
          var data = {
             from: docs && docs[0]
          };

          if(data){
            var myJSONObject = {
              avatarUrl: path_profilepic
              // username: username
              };
            request({
                url: "https://chat.augray.com/api/v1/users.setAvatar",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    "X-Auth-Token": data['from']['chat_authToken'],
                    "X-User-Id": data['from']['chat_userId'],
                },
                json: true,
                  body: myJSONObject,
            }, function (error, response, body){
              console.log(body)
                if(body.success === true){
                  return false; // res.send({status:1}) //,room_details:body.room
                }else{
                  res.send({status:0,message:"Upload Failed in rocketchat"})
                }
            });
          }else{
            res.send({status:0,message:"Check rocketchat authToken"})
          }
        }
      })

           if (path_profilepic!="" && path_profilepic_org != undefined) {
            db.get().collection('av_avatar').findOneAndUpdate({user_id: ObjectId(input_data['user_id'])}, {$set: {user_profile_picture:parseInt(1)}},{returnOriginal:false} , function(err, result) {
            assert.equal(err, null);
           })
          }


        }
      }

        if(path_profilepic_org != "" && path_profilepic_org != undefined && path_profilepic_org != null){
          update_data['profile_picture_org'] = path_profilepic_org;


          if(old_path_profilepic_org != "" && old_path_profilepic_org != undefined && old_path_profilepic_org != null){
          var s3 = new AWS.S3({
             accessKeyId: Settings.s3.key,
             secretAccessKey: Settings.s3.secret
           });
          var mynewBucket = Settings.s3.bucketnew;

           var del_filename = path.basename(old_path_profilepic_org);

          var params = {
              Bucket: mynewBucket,
              Key:  Settings.s3['path-avatar'] + del_filename
          };
          s3.headObject(params, function (err, metadata) {
            if (!err || err.code != 'NotFound') {
              s3.deleteObject(params, function(err, data) {
               if (err) console.log(err, err.stack); // an error occurred
               else     console.log(data);           // successful response
              });
            }
          });
        }
      }



    db.get().collection('aug_users').findOneAndUpdate({_id: ObjectId(input_data['user_id'])}, {$set: update_data},{returnOriginal:false} , function(err, result) {
      assert.equal(err, null);

      if(result && result.value){
        var result_data = {};
        //result_data['status'] = 1;
        result_data['user_id'] = result.value['_id'];
        result_data['user_name'] = result.value['user_name'];
        result_data['user_email'] = result.value['user_email'];
        result_data['user_phone'] = result.value['user_phone'];
        result_data['is_social_login'] = result.value['is_social_login'];
        result_data['language_code'] = result.value['language_code'];
        result_data['phone_code'] = result.value['phone_code'];
        // result_data['country_code'] = result.value['country_code'];
        result_data['user_gender'] = result.value['user_gender'];
        result_data['user_dob'] = result.value['user_dob'];
        result_data['profile_picture'] = result.value['profile_picture'];
        // result_data['profile_picture_org'] = result.value['profile_picture_org'];
        result_data['user_about'] = result.value['user_about'];
        res.send({
          status: 1,
          data: result_data
        })
      }else{
            res.send({status:0, error: "Update failed"})
          }

        })

      }else{
        res.send({status:0,error:"User not found"})
      }

   // })

});
}
catch(errorcath){
  console.log(errorcath)
}
};
