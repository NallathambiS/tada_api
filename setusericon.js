'use strict';
var init = require("../../init");
var Const = require("../../const");
var async = require('async');
var Settings = require("../../lib/Settings");
var mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const assert = require('assert');
const mongo_url = init.mongo_url;
var ObjectId = mongo.ObjectID;
var request = require('request');

exports.setusericon = function(req, res) {

  var from_user_id = req.body.from_user_id;
  // var username = req.body.username;
  var url_link = req.body.url_link;

  if(from_user_id === undefined || from_user_id === "" || url_link === undefined || url_link === ""){
    res.send({status:0,error: "Missing mandatory elements"});
    return true;
  }

  
        db.get().collection("aug_users").find({_id : ObjectId(from_user_id)}).limit(1).skip(0).toArray(function(err, docs){
          assert.equal(err, null);
        if(docs.length !== 0){
          var data = {
             from: docs && docs[0]
          };
          if(data){
            var myJSONObject = {
              avatarUrl: url_link,
              // username: username
              };
              // res.send(myJSONObject)
            request({
                //url: "https://tada.augray.com:3001/api/v1/users.setAvatar",
                url: "https://chat.augray.com/api/v1/users.setAvatar",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    "X-Auth-Token": data['from']['chat_authToken'],
                    "X-User-Id": data['from']['chat_userId'],
                },
                json: true,
                  body: myJSONObject,
            }, function (error, response, body){
              console.log(body)
                if(body.success === true){
                  res.send({status:1}) //,room_details:body.room
                }else{
                  res.send({status:0})
                }
            });
          }else{
            res.send({status:0})
          }
        }
      })
  

}
