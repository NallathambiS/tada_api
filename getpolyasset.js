'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")
var request = require('request')

exports.getpolyasset = function(req, res) {
let content_type=req.body.content_type
let pagination_limit=req.body.pagination_limit || 2
let pagination_index = req.body.pagination_index || 1;
let key=req.body.api_key
let pageToken=req.body.pagetoken

  let skipParams = parseInt(pagination_limit * (pagination_index - 1));
  let limitParams = parseInt(pagination_limit);
  let totalCount=0; 
  // https://poly.googleapis.com/v1/assets?keywords=duck&pageSize=5&key=AIzaSyDsxW6B1qB91pr6fXCuVX5g-GpSzuO3pD8

  function skip(c) {
      return this.filter((x,i) => {
        if(i>(c-1)){return true}
      })
  }
  function limits(c) {
      return this.filter((x,i) => {
        if(i<=(c-1)){return true}
      })
  }
  Array.prototype.skip=skip;
  Array.prototype.limits=limits;
  request({
      url: "https://poly.googleapis.com/v1/assets?keywords="+content_type+"&pageSize="+100+"&key="+key+"",
      method: "GET"
      },
       function (error, response, body){
        let data=JSON.parse(response.body)
        // totalCount=data['totalSize']
        totalCount=data['assets'].length;
        let temparr=[]
        for(var i=0;i<data['assets'].length;i++){
          temparr.push(data['assets'][i])
        }
          if(response.statusCode === 200){ 
            res.send({status:1,//nextpagetoken:data['nextPageToken'],
              totalPages:Math.ceil(totalCount / limitParams),
              currentPageIndex: pagination_index,
              totalCount:totalCount,
              pageCount:pagination_limit,data:temparr.skip(skipParams).limits(limitParams)})
          }else{
            res.send({status:0,error:response.body.message})
          }
})

}
//https:developers.google.com/apis-explorer/?hl=en_US#p/poly/v1/poly.assets.list?keywords=duck&pageSize=5&_h=1&