'use strict';
var Const = require("../../const")
var _ = require('lodash')
var mongo_lib = require('mongodb')
const assert = require('assert')
var db = require("../../db")
var ObjectId = mongo_lib.ObjectID
const sgMail = require('@sendgrid/mail')

exports.reportgroup = function(req, res) {
      sgMail.setApiKey(Const.sendgrid_api)
      let user_id = req.body.user_id
      let room_id = req.body.room_id
      let report_message = req.body.report_message
      let report_type_name=""
      let user_name=""

      if(!user_id || !room_id || room_id == '' || user_id == '' || !report_message || report_message == ''){
        res.send({error:"Missing mandatory elements room_id/user_id/report_message",status:0});
        return true;
      }

       var where = {};
       where['_id'] = ObjectId(user_id);
       db.get().collection("aug_users").find(where).toArray(function(err, docs) {
            assert.equal(err, null);
            if(docs.length>0){
              user_name=docs[0].user_name;
            }
       })
      var where = {};
      where['_id'] = room_id;
      db.getChat().collection("rocketchat_room").find(where).toArray(function(err, docs) {
          assert.equal(err, null);
          if(docs.length>0) {
            let msgTxt =  '<html><p>Hi,</p><p>'+user_name+' reporting this Group ' + docs[0].name + '.</p><p>Reason: </br>'+report_message+'</p></html>';
            const msg = {
              to: 'nallathambis@augray.com',
              from: {
                email: 'tada@augray.com',
                name: 'TaDa Admin'
              },
              subject: 'Report Group',
              html: msgTxt,
            };

          sgMail.send(msg,function(success,message) {
            if (!success) {
              res.send({status:1,message:message});
            }
          })
         }
    else{
      res.send({status:0,message:"Failed to send"});
    }
    })
  }


//groupid info
