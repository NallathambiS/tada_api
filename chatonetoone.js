'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')
var request = require('request')

exports.chatonetoone = function(req, res) {

  var from_user_id = req.body.from_user_id;
  var to_user_id = req.body.to_user_id;

  if(from_user_id === undefined || from_user_id === "" || to_user_id === undefined || to_user_id === ""){
    res.send({status:0,error: "Missing mandatory elements"});
    return true;
  }
// res.send("jjfhjkfhvjkhfjh")
      let from_user= (callback0)=> {
        db.get().collection("aug_users").find({_id : ObjectId(from_user_id)}).limit(1).skip(0).toArray((err, docs)=>{
          assert.equal(err, null);
          callback0(null, docs);
        })
      }
      let to_user= (callback1)=> {
        db.get().collection("aug_users").find({_id : ObjectId(to_user_id)}).limit(1).skip(0).toArray((err, docs)=>{
          assert.equal(err, null);
          callback1(null, docs);
        })
      }

      async.parallel({
        fromdata: from_user,
        todata: to_user
      },

      (err, results) =>{
        if (err) {
          res.send({
            status: 0
          })
          return
         }
        if(results.length !== 0){
          var data = {
            from: results['fromdata'][0], //results[0] && results[0][0],
            to: results['todata'][0] //results[1] && results[1][0],
          };

          if(data){
            var myJSONObject = {
              "username": data['to']['user_name']
            };

            request({
                url: "https://chat.augray.com/api/v1/im.create",
                method: "POST",
                headers: {
                    "content-type": "application/json",
                    "X-Auth-Token": data['from']['chat_authToken'],
                    "X-User-Id": data['from']['chat_userId'],
                },
                json: true,
                body: myJSONObject,
            },  (error, response, body)=>{ //res.send({asd:response,awd:body})
              console.log(body)
                if(body.success === true){
                  // res.send({status:1,room_details:body.room;
                var fulldata={}

                var resdata=body.room;  
                  // res.send({status:1,room_details:resdata.room_details});
                  // var newvar=resdata.room_details;
                  
                var t=resdata['t'];
                var rid=resdata['rid'];
                var usernames=resdata['usernames'];
                var _updatedAt=new Date();
 
                var newdata={}
                newdata['t']=t;
                newdata['rid']=rid;
                newdata['_id']=rid;
                newdata['usernames']=usernames;
                 newdata['_updatedAt']=_updatedAt;
                res.send({status:1,room_details:newdata});

                }else{
                  res.send({status:0})
                }
            });
          }else{
            res.send({status:0})
          }
        }
      })
}
