'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var AWS = require('aws-sdk')
var path = require('path')  
var url = require("url")
var db = require("../../db")
var Settings = require("../../lib/Settings");

exports.deletevideo = function(req, res) {
    var videoid =req.body.video_id;
    var video_user_id =  req.body.user_id;
    var videopath =  req.body.video_path;  
    var thumbimage =  req.body.thumb_image; 
    let checkinput={} 

    checkinput['_id']=ObjectId(videoid); 
    checkinput['video_user_id']=video_user_id;
    // checkinput['video_path']=videopath; 
    // checkinput['thumb_image']=thumbimage;   
    for(var item in checkinput) {
      if(checkinput[item] == "" && checkinput[item] == undefined){
          res.send({status:0,error: "Missing mandatory elements "+item});
          return true;
      }
    }   
  
    db.get().collection('av_video').find({_id:ObjectId(videoid),video_user_id:ObjectId(video_user_id)}).toArray(function(err, awspathresult) {
        assert.equal(err, null);
        if(awspathresult.length!=0){
        let video_url = awspathresult[0]['video_path']
        let thumb_url = awspathresult[0]['thumb_image'] 

   
          db.get().collection('av_video').remove({_id:ObjectId(videoid),video_user_id:ObjectId(video_user_id)},  function(err, result) {  
          assert.equal(err, null);
          if(result['result']['ok']> 0){

            var delimg = path.basename(thumb_url);
            var deletevid = path.basename(video_url);
             
            var s3 = new AWS.S3({
              accessKeyId: Settings.s3.key,
              secretAccessKey: Settings.s3.secret
            });

              
         
            if(delimg != ""){
              var paramsimg = {
              Bucket: Settings.s3.bucketnew,
              Key: Settings.s3['path-avatar']+delimg
            }; 
            s3.headObject(paramsimg, function (err, metadata) {
                  if (!err || err.code != 'NotFound') {
                    s3.deleteObject(paramsimg, function(err, data) {
                      if (err) console.log(err, err.stack); // an error occurred
                      else     console.log(data);           // successful response
                    });
                  }
                });
          }
          if(deletevid != ""){
            // var urls = s3->getObjectUrl('my-bucket', 'my-key');
            // res.send({asd:urls})
               var paramsvid = {
              Bucket: Settings.s3.bucketnew,
              Value: Settings.s3['path-post']+deletevid
            }; 
             /*s3.headObject(paramsvid, function (error, metadata) {
              if(error){
                res.send({status:0, error: "Failed to find videosss in aws"})
              }
              else{
                s3.deleteObject(paramsvid, function(err, data) {
                  if(err){
                    res.send({status:0, error: "Failed to delete video in aws"}) 
                  }else{
                     res.send({status:1, message: "Video deleted successfully"})         
                  }
                  
                });
              } 
          }); */ 
            s3.headObject(paramsvid, function (err, metadata) {
              if (!err || err.code != 'NotFound') {
                s3.deleteObject(paramsvid, function(err, data) {
                  if (err) console.log(err, err.stack); // an error occurred
                  else     console.log(data);           // successful response
                  res.send({status:1, message: "Video deleted successfully"})  
                });
              }
            });
          }

           // res.send({status:1, message: "Video deleted successfully"})                     
        //  }
          else{
            res.send({status:0, error: "Failed to delete video"})
          } 
        
      }  
      else{       
        res.send({status:0, error: "Failed to find video"})         
      } 
   })     
}
})  
  }