'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
const { ObjectId } = require('mongodb')

exports.getfollowlist = function(req, res) {
  let user_id = req.body.user_id;
  let type = req.body.type;
  let searchelem = req.body.search || "";
  let where;
  let where_followinglist;
  let totalFollowCount;
  let totalFriendsCount;
  let totalBlockedCount;
  let totalFollowupCount;

  let pagination_limit = req.body.pagination_limit || 50;
  let pagination_index = req.body.pagination_index || 1;

  let skipParams = parseInt(pagination_limit * (pagination_index - 1));
  let limitParams = parseInt(pagination_limit);

  if (!user_id || user_id =="" || !type || type =="") {
    return res.send({
      status : 0,
      message : "Missing mandatory elements user_id"
    });
  }
  function skip(c) {
      return this.filter((x,i) => {
        if(i>(c-1)){return true}
      })
  }
  function limits(c) {
      return this.filter((x,i) => {
        if(i<=(c-1)){return true}
      })
  }

      if (type === "follower") {
         where = {
           following : ObjectId(user_id),
           status:1 //added
         }
        /* where_followinglist = {
           follower : ObjectId(user_id),
           status:1
         }*/

        async.parallel([
           function(callback0) {
              db.get().collection("user_friends").aggregate([
               { "$match" : where},
               { "$lookup": {
                "from": "aug_users",
                "localField": "follower" ,
                "foreignField": "_id",
                "as": "follow_list"
               }},
               { "$unwind": "$follow_list"},
               { "$project": {
                   "_id": 0,
                   "follow_list._id": 1,
                   "follow_list.user_name": 1,
                   "follow_list.user_gender": 1,
                   "follow_list.user_dob": 1,
                   "follow_list.profile_picture": 1,
                   "follow_list.thumb_image": {$arrayElemAt: ['$follow_list.thumb_image.file_path', 0] },
                    "follow_list.status": "$status",
                 }
               },
             ]).toArray(function(err, docs) {  
                 assert.equal(err, null);
                 var arrayFollowers = [];
                 if(docs.length !== 0){
                   docs.map(a=> {
                     arrayFollowers.push(a['follow_list']);
                   })
                   callback0(null, arrayFollowers);
                 }else{
                   console.log("debugging") // testing
                   res.send({
                     status:0
                   });
                 }
              })
       }],
          function(err, result) {
            var arrfollowersts=[];
            var arrftemp0=[];
            var arrftemp1=[];

            for(var c=0;c<result[0].length;c++)
              {
                    arrfollowersts.push({
                        "_id":result[0][c]._id,
                        "user_name":result[0][c].user_name,
                        "user_gender":result[0][c].user_gender,
                        "user_dob":result[0][c].user_dob,
                        "profile_picture":result[0][c].profile_picture,
                        "thumb_image":result[0][c].thumb_image,
                        "status": result[0][c].status
                      });
              }

            if(arrfollowersts  !== 0){
            Array.prototype.skip=skip;
            Array.prototype.limits=limits;

            if(searchelem != ""){
            for (var i = arrfollowersts.length - 1; i >= 0; i--) {
            if(arrfollowersts[i].user_name===searchelem){
            res.send({
                       status: 1,
                       totalPages: Math.ceil(arrfollowersts.length / limitParams),
                       currentPageIndex: pagination_index,
                       totalCount: arrfollowersts.length,
                       data: [arrfollowersts[i]]
                       })
            }
            }
            }
            res.send({
               status: 1,
               totalPages: Math.ceil((arrfollowersts.length) / limitParams),
               currentPageIndex: pagination_index,
               totalCount: arrfollowersts.length,
               data: arrfollowersts.skip(skipParams).limits(limitParams)
             });
           }else{
             res.send({
               status:0,
               currentPageIndex: 0,
               totalCount: 0,
               data: []
             });
           }
            })
    }
    else if (type=="following"){
       where = {
           follower : ObjectId(user_id),
           status:1
       }

      db.get().collection("user_friends").find(where).count(function(err, docs) {
        assert.equal(err, null);
        totalFollowCount = docs;
      });
      db.get().collection("user_friends").aggregate([
        {"$match" : where},
        { "$lookup": {
         "from": "aug_users",
         "localField": type ,
         "foreignField": "_id",
         "as": "follow_list"
        }},
        { "$unwind": "$follow_list"},
        { "$project": {
             "_id": 0,
             "follow_list._id": 1,
             "follow_list.user_name": 1,
             "follow_list.user_gender": 1,
             "follow_list.user_dob": 1,
             "follow_list.profile_picture": 1,
             "follow_list.thumb_image": {$arrayElemAt: ['$follow_list.thumb_image.file_path', 0] },
             "follow_list.status": "$status",
           }
         },
         { $skip: skipParams },
         { $limit: limitParams }

       ]).toArray(function(err, docs) {
           assert.equal(err, null);
           var arrayFollowers = [];
           docs.map(a=> {
              arrayFollowers.push(a['follow_list']);
           })
           if(arrayFollowers  !== 0){
           if(searchelem != "") {
             for (var i = arrayFollowers.length - 1; i >= 0; i--) {
               if (arrayFollowers[i].user_name===searchelem) {
                  res.send({
                         status: 1,
                         totalPages: Math.ceil(arrayFollowers.length / limitParams),
                         currentPageIndex: pagination_index,
                         totalCount: arrayFollowers.length,
                         data: [arrayFollowers[i]]
                  })
               }
             }
           }

           res.send({
             status: 1,
             totalPages: Math.ceil(totalFollowCount / limitParams),
             currentPageIndex: pagination_index,
             totalCount: totalFollowCount,
             data: arrayFollowers
           });
         }
         else{
          res.send({
             status: 0,
             totalPages: 0,
             currentPageIndex: 0,
             totalCount: 0,
             data: []
           });
         }
       })
   }

   else if (type=="blocked") {
        where = {}
        where['blocker'] = ObjectId(user_id)
        if(search != ""){
            where['user_name'] = {$regex: search, $options: 'i'};//{ $regex: search, $options: 'i' } ;
        }
        db.get().collection("user_blocklist").find(where).count(function(err, docs) {
          assert.equal(err, null);
          totalBlockedCount = docs;
        })
        db.get().collection("user_blocklist").find(where).toArray(function(err, docs4) {
            assert.equal(err, null);
            var user_ids = [];
            if (docs4 && docs4.length > 0) {
              for (var i = 0; i < docs4.length; i++) {
                if(docs4[i]['blocker'] == user_id){
                  user_ids.push(docs4[i]['blocked'].toString());
                }
              }
              var new_user_objectids = [];
              for (var i = 0; i < user_ids.length; i++) {
                new_user_objectids.push(ObjectId(user_ids[i]));
              }

              let where_blocked ={_id: {$in:new_user_objectids}}

              if(search != ""){
                where_blocked['user_name'] = {$regex: search, $options: 'i'};//{ $regex: search, $options: 'i' } ;
              }

              db.get().collection("aug_users").find(where_blocked).limit(limitParams).skip(skipParams).project({_id:1,user_name:1,user_email:1,issocial_login:1,user_gender:1,user_dob:1,profile_picture:1}).toArray(function(err, docs5) {
                assert.equal(err, null);
                res.send({
                  status: 1,
                  totalPages: Math.ceil(totalBlockedCount / limitParams),
                  currentPageIndex: pagination_index,
                  totalCount: totalBlockedCount,
                  data: docs5
                })
                return true
              })


            } else {
              res.send({status:0,error:"no users found"})
              return true
            }
          })
   }
   else {
        return res.send({status : 0,error  : "invalid type"})
   }
}
