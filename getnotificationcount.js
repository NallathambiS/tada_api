'use strict'; 
var _ = require('lodash');
var async = require('async'); 
const assert = require('assert')
var db = require("../../db")
const {ObjectId} = require('mongodb')


exports.getnotificationcount = function(req, res) { 
    var user_id =  req.body.user_id; 
    if(user_id === "" || user_id == undefined){
      res.send({status:0,error:"missing mandatory element deviceid"});
      return true;
    } 
    var where = {
      receiver_id : ObjectId(user_id) ,
      seen:false
    }     
    db.get().collection('sn_notifications').find(where).count(function(err, result) { 
    assert.equal(err, null); 
    if(result!=""){
      res.send({status:1,unread_count:result})
    }else{
      res.send({status:0, error: "notifications not found"})
    }

  }) 
};
