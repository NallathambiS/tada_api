'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")
var mongo_lib = require('mongodb')
var request = require('request')
var ObjectId = mongo_lib.ObjectID


exports.getuserprofile = function(req, res) {
try{
  let from_user_id = req.body.from_user_id;
  let to_user_id = req.body.to_user_id;

  if (from_user_id === "" || from_user_id === undefined){
     res.send({status:0,error:"missing input field, from_user_id is mandatory."});
     return true;
  }

  if (to_user_id === "" || to_user_id === undefined){
     res.send({status:0,error:"missing input field, to_user_id is mandatory."});
     return true;
  }

      db.get().collection("aug_users")
        .find({_id:ObjectId(from_user_id)})
        .project({ _id: 1, thumb_image: 1, user_name: 1, user_gender: 1,chat_authToken:1, chat_userId:1})
        .toArray(function(err, user_docs) {
        assert.equal(err, null);
        if(user_docs.length !== 0){
          async.parallel([
            function(callback0) { 
              //groups
            let usergrpnames=[]
            request({
              url: "https://chat.augray.com/api/v1/groups.listAll?username="+ user_docs[0]['user_name'] +"",
              method: "GET",
              headers: {
                  "content-type": "application/json",
                  "X-Auth-Token": user_docs[0]['chat_authToken'],
                  "X-User-Id": user_docs[0]['chat_userId'],
              },
              json: true,
            },
            function (error, response, body){ 
              if(body.success === true){    
              for(var i=0;i<body.groups.length;i++){        
                 usergrpnames.push(body.groups[i].name)
              } 
              }
             }); 
              callback0(null, usergrpnames);
                // db.getChat().collection("rocketchat_room")
                //   .find({t:'p',usernames:user_docs[0]['user_name']})
                //     .project({ _id: 1, name: 1, usernames: 1 })
                //       .toArray(function(err, docs) { 
                //         assert.equal(err, null);
                //         callback0(null, docs);
                //       })
            },
            function(callback1) {
              //user_following
              let user_following_where = {
                follower : ObjectId(to_user_id),
                status : 1,
              }
              db.get().collection("user_friends").find(user_following_where).count(function(err1, docs) {
                assert.equal(err1, null);
                callback1(null, docs);
              });
            },
            function(callback2) {
              //user_followers
              let user_followers_where = {
                following : ObjectId(to_user_id),
                status : 1
              }
              db.get().collection("user_friends").find(user_followers_where).count(function(err2, docs) { 
                assert.equal(err2, null);
                callback2(null, docs);
              });
            },
            function(callback3) {
              //user_data
              db.get().collection("aug_users")
                .find({_id:ObjectId(to_user_id)})
                .project({ _id: 1, profile_picture: 1, user_name: 1, user_gender: 1,user_about: 1 })
                .toArray(function(err, docs) {
                assert.equal(err, null);
                callback3(null, docs);
              })
            },
            function(callback4) {
              //user_data
              db.get().collection("user_friends")
                .find({following : ObjectId(to_user_id), follower: ObjectId(from_user_id)})
                .toArray(function(err, docs) {  
                assert.equal(err, null);
                callback4(null, docs);
              })
            },
            function(callback5) {
              //user_block_data
              db.get().collection("user_blocklist")
                .find({blocker : ObjectId(from_user_id),blocked : ObjectId(to_user_id)})
                .toArray(function(err, docs) {  
                assert.equal(err, null);
                callback5(null, docs);
              })
            }
          ],
          function(err, results) {  
            results[3][0]['groups'] = results && results[0];
            results[3][0]['following_count'] = results && results[1];
            results[3][0]['followers_count'] = results && results[2];
            results[3][0]['status'] = results && results[4] && results[4][0] && results[4][0]['status'] !== undefined ? results[4][0]['status'] : 2;
            results[3][0]['thumb_image'] = results[3][0]['profile_picture'];
            results[3][0]['user_about'] = results[3][0]['user_about'] ? results[3][0]['user_about'] : "";
            results[3][0]['blocked'] = results[5][0] && results[5][0]['blocked'] !== undefined ? 1 : 0;
            /*results[3][0]['follow_status'] = (
              results[3][0]['follow_status'] === 0 ? results[3][0]['follow_status']= 0 :
                ( results[3][0]['follow_status'] === 1 ? 1 : 2 )
              )*/
            res.send({
              status: 1,
              data: results[3] ? results[3][0] : {}
            });
          })
        }
      })
}
catch(exception){
  res.send({status:0, error: exception})
}
};
