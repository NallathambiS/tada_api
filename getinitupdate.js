'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
var db = require("../../db")

exports.getinitupdate = function(req, res) {
  let platform = req.body.platform

  if (!platform) {
      let rescode = {
        status : 0,
        error  : "Missing mandatory elements platform"
      }
      res.send(rescode)
  }

  let where = {}
  where.asset_name = { $regex: `onb_`, $options: 'i' }

  let animation_type = {
    animation_type_name: "Onboarding"
  }

  let asset = (callback0) => {
    db.get().collection("av_asset").aggregate([
      {
        "$match" : where
      },
      {
        $lookup: {
          "from": "av_asset_type",
          "localField": "asset_type_id",
          "foreignField": "_id",
          "as": "ob_asset"
        }
      },
      {
        $unwind: "$ob_asset"
      },
      {
        $project: {
          asset_type_name: "$ob_asset.asset_type_name",
          asset_type_id: 1,
          char_type_id: 1,
          gender: 1,
          user_id: 1,
          asset_name: 1,
          thumb_image: 1,
          file_path_ios: 1,
          file_path_android: 1,
          metadata: 1,
          asset_enable: 1,
          country_code: 1,
          point: 1,
          last_update: 1
        }
      }
    ]).toArray((err, docs) => {
      assert.equal(err, null);
      callback0(null, docs);
    })
  }

  let animation = (callback1) => {
    db.get().collection("av_animation_type").aggregate([
      {
        "$match" : animation_type
      },
      {
        $lookup: {
          "from": "av_animation",
          "localField": "_id",
          "foreignField": "animation_type_id",
          "as": "ob_animations"
        }
      },
      {
        $unwind: "$ob_animations"
      },
      {
        $lookup: {
          "from": "av_animation_type",
          "localField": "ob_animations.animation_type_id",
          "foreignField": "_id",
          "as": "av_animation_type_collection"
        }
      },
      {
        $unwind: "$av_animation_type_collection"
      },
      { "$lookup": {
            "from": "av_asset",
            "localField": "ob_animations.plugin_asset_id",
            "foreignField": "_id",
            "as": "plugindata"
          }}, 
      { "$unwind": {"path": "$plugindata", "preserveNullAndEmptyArrays": true}}, 
      {
        $project: {
          "_id": "$ob_animations._id",
          "user_id": "$ob_animations.user_id",
          "char_type_id": "$ob_animations.char_type_id",
          "animation_type_id": "$ob_animations.animation_type_id",
          "animation_type_name": "$av_animation_type_collection.animation_type_name",
          "animation_name": "$ob_animations.animation_name",
          "file_path_ios": "$ob_animations.file_path_ios",
          "file_path_android": "$ob_animations.file_path_android",
          "thumb_image": "$ob_animations.thumb_image",
          "metadata": "$ob_animations.metadata",
          "plugin_asset_id": { "$cond": [{ "$eq": [ "$plugin_asset_id", null ] },"$$REMOVE", "$plugin_asset_id" ] },
          "plugin_asset"   : { "$cond": [{ "$eq": [ "$plugindata", null ] },"$$REMOVE", "$plugindata" ] },
          "animation_enable": "$ob_animations.animation_enable",
          "country_code": "$ob_animations.country_code",
          "point": "$ob_animations.point",
          "last_update": "$ob_animations.last_update"
        }
      }

    ]).toArray((err, docs) => {
      assert.equal(err, null);
      callback1(null, docs);
    })
  }

  let where_def = {
    asset_name: "Default"
  }

  let animation_name_def = {
    animation_name: "Default"
  }

  let default_asset = (callback0) => {
    db.get().collection("av_asset").aggregate([
      {
        "$match" : where_def
      },
      {
        $lookup: {
          "from": "av_asset_type",
          "localField": "asset_type_id",
          "foreignField": "_id",
          "as": "ob_asset"
        }
      },
      {
        $unwind: "$ob_asset"
      },
      {
        $project: {
          asset_type_name: "$ob_asset.asset_type_name",
          asset_type_id: 1,
          char_type_id: 1,
          gender: 1,
          user_id: 1,
          asset_name: 1,
          thumb_image: 1,
          file_path_ios: 1,
          file_path_android: 1,
          metadata: 1,
          asset_enable: 1,
          country_code: 1,
          point: 1,
          last_update: 1
        }
      }
    ]).toArray((err, docs) => {
      assert.equal(err, null);
      callback0(null, docs);
    })
  }

  let default_animation = (callback1) => {
    db.get().collection("av_animation").aggregate([
      {
        "$match" : animation_name_def
      },
      {
        $lookup: {
          "from": "av_animation_type",
          "localField": "animation_type_id",
          "foreignField": "_id",
          "as": "default_animations"
        }
      },
      {
        $unwind: "$default_animations"
      },
      {
        $project: {
          "user_id": 1,
          "char_type_id": 1,
          "animation_type_id": 1,
          "animation_type_name": "$default_animations.animation_type_name",
          "animation_name": 1,
          "file_path_ios": 1,
          "file_path_android": 1,
          "thumb_image": 1,
          "metadata": 1,
          "animation_enable": 1,
          "country_code": 1,
          "point": 1,
          "last_update": 1
        }
      }

    ]).toArray((err, docs) => {
      assert.equal(err, null);
      callback1(null, docs);
    })
  }

  let init_update = (callback2) => {
      let where = {
          platform : platform
      };
      db.get().collection("init_update").find(where).toArray(function(err, docs) {
        assert.equal(err, null)

        if(docs.length > 0){
          docs['update_id'] = docs['_id']
          docs['app_version'] = docs['app_version']
          docs['is_force_update'] = docs['is_force_update']
        }
        callback2(null, docs)
      })
  }

  let media = (callback3) => {
      callback3(null, {
        images: [
          {
            _id: 'Bilal1',
            image_path: 'https://www.pentaxforums.com/content/uploads/files/1/1456/PKS22063.jpg',
            last_update: "6761337409436123137"
          },
          {
            _id: 'Bilal2',
            image_path: 'https://www.pentaxforums.com/content/uploads/files/1/1456/IMGP0027.jpg',
            last_update: "6761337409436123137"
          },
          {
            _id: 'Bilal3',
            image_path: 'https://www.pentaxforums.com/content/uploads/files/1/1456/IMGP0321.JPG',
            last_update: "6761337409436123137"
          }
        ],
        videos: [
          {
            _id: 'Vijay kumar',
            video_path: 'vijay.mp4',
            last_update: "6761337409436123137"
          },
          {
            _id: 'Vijay kumar',
            video_path: 'vijay.mp4',
            last_update: "6761337409436123137"
          },
          {
            _id: 'Vijay kumar',
            video_path: 'vijay.mp4',
            last_update: "6761337409436123137"
          }
        ]
      })
  }

  async.parallel({
    onboard_asset: asset,
    onboard_animation: animation,
    default_asset: default_asset,
    default_animation: default_animation,
    init_update: init_update,
    media: media
    },
    (err, results) => {

      let result_data = {
        status : 1,
        response : {
          download : {
            onboard: {
              asset_male: _.filter(results.onboard_asset, { gender: 'M' }),
              asset_female: _.filter(results.onboard_asset, { gender: 'F' }),
              animation: results.onboard_animation
            },
            default: {
              asset_male: _.filter(results.default_asset, { gender: 'M' }),
              asset_female: _.filter(results.default_asset, { gender: 'F' }),
              animation: results.default_animation
            }
          },
          app_update : results.init_update,
          media : results.media
        }
      }
      res.send(result_data)
  })


};
