'use strict';
var _ = require('lodash')
var async = require('async')
const assert = require('assert')
const {ObjectId} = require('mongodb')
var db = require("../../db")
var Settings = require("../../lib/Settings");

exports.savefeedback = function(req, res) {
  var user_id =req.body.user_id;
  var user_feedback =req.body.user_feedback;
  var user_suggestion =req.body.user_suggestion || "";
    db.get().collection('aug_users').find({_id: ObjectId(user_id)}).toArray(function(err, docs) { 
    assert.equal(err, null)
    let user_data={}
       user_data['user_name']=docs[0]['user_name'];
       user_data['user_email']=docs[0]['user_email'] || "";
       user_data['user_phone']=docs[0]['user_phone'] || "";
       user_data['phone_code']=docs[0]['phone_code'] || "";
       user_data['user_feedback']=user_feedback;
       user_data['user_suggestion']=user_suggestion;

    db.get().collection("user_feedback").insertOne(user_data,function(err, docs) {
    assert.equal(err, null);
      if(docs['insertedCount']> 0){
        res.send({status:1,message:"feedback stored"})
      }
      else {
        res.send({status:0,error:"feedback not stored"});
      }
    })
  })
}
